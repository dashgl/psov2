/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/

"use strict";

const AssetEnemies = {

	"Boss1" : async function() {

		let ark = await NinjaFile.API.load("br01mdl.bin");
		let mbd = NinjaFile.API.prs(ark);

	},

	"Rappy" : async function () {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_lappy.bml"]);
		let tex = NinjaTexture.API.parse(bml["re3_b_lappy_base.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re3_b_lappy_base.nj"]);
		modelLoader.addAnimation(bml["attack_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["damage_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["die_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["run_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["tumble_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["walk_re3_b_base.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Al Rappy" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_lappy.bml"]);
		let tex = NinjaTexture.API.parse(gsl["re3_b_lappy_base_ao.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re3_b_lappy_base.nj"]);
		modelLoader.addAnimation(bml["attack_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["damage_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["die_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["run_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["tumble_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["walk_re3_b_base.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Booma" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re8_b_beast.bml"]);
		let tex = NinjaTexture.API.parse(bml["re8_b_beast_wola_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_b_beast_wola_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);


	},

	"GoBooma" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re8_b_beast.bml"]);
		let tex = NinjaTexture.API.parse(bml["re8_b_srdbeast_wola_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_b_srdbeast_wola_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"GigaBooma" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re8_b_beast.bml"]);
		let tex = NinjaTexture.API.parse(bml["re8_b_rdbeast_wola_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_b_rdbeast_wola_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Savage Wolf" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm5_wolf.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm5_s_kem_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm5_s_kem_body.nj"]);
		modelLoader.addAnimation(bml["dams_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["deadr_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["eat_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hunt_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okil_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okir_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["run_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["runb_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["sleep_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["stdup_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["wait_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm5_s_kem_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Barbarous Wolf" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm5_wolf.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm5_s_kem_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm5_s_keml_body.nj"]);
		modelLoader.addAnimation(bml["dams_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["deadr_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["eat_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hunt_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okil_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okir_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["run_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["runb_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["sleep_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["stdup_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["wait_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm5_s_kem_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Monest" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm3_fly.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm3_fly_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm3_s_nest.nj"]);
		modelLoader.addAnimation(bml["dam_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dead_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["down_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwndam_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwnexit_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwnwait_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["exit_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["land_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["trance_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["wait_bm3_s_nest.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Mothmant" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm3_fly.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm3_fly_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm3_fly_body.nj"]);
		modelLoader.addAnimation(bml["atack_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["fly_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["move_bm3_fly_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hildebaby" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm2_moja.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm2c_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2c_s_moj_body.nj"]);
	
		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hildebear" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm2_moja.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm2f_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2f_s_moj_body.nj"]);

		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hildeblue" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm2_moja.bml"]);

		let tex = NinjaTexture.API.parse(bml["bm2w_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2w_s_moj_body.nj"]);

		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dragon" : async function() {

		let ark = await NinjaFile.API.load("bm_boss1_dragon.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["boss1_s_nb_dragon.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["boss1_s_nb_dragon.nj"]);

		modelLoader.addAnimation(bml["daml_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["dams_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["dead_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["down_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["fire_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["fly_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["flyshot_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frin_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frloop_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frout_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["kiri_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["land_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["lift_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nkdown_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nkup_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nobi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["stand_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tatk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tobidasi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tukomi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["walk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wgwalk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wing_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wngclose_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wngopn_boss1_s_nb_dragon.njm"]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Evil Shark" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm1_shark.bml"]);

		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1_s_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Pal Shark" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm1_shark.bml"]);

		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1f_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1f_s_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Guil Shark" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_bm1_shark.bml"]);

		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1tl_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1tl_s_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Pan Arms" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm7_s_paa_body.bml"], true);
			
		let tex = NinjaTexture.API.parse(bml['bm7_s_paa_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm7_s_paa_body.nj']);

		modelLoader.addAnimation(bml['beamdwn_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['beamup_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['btamed_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['btameu_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['cry_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['deada_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['walkl_bm7_s_paa_body.njm']); 
		modelLoader.addAnimation(bml['walkr_bm7_s_paa_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hidoom" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm7_s_paa_body.bml"]);

		let tex = NinjaTexture.API.parse(bml['bm7_s_pal_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm7_s_pal_body.nj']);

		modelLoader.addAnimation(bml['atack_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['bunri_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['deada_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['gattai_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['heal_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['run_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm7_s_pal_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm7_s_pal_body.njm'])

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);


	},

	"Migium" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm7_s_paa_body.bml"]);

		let tex = NinjaTexture.API.parse(bml['bm7_s_par_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm7_s_par_body.nj']);

		modelLoader.addAnimation(bml['atack_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['bunri_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['deada_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['gattai_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['run_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm7_s_par_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm7_s_par_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},


	"Mini Grass Assasin": async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_cgrass.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['re1c_b_cgrass_base.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re1c_b_cgrass_base.nj']);
		
		modelLoader.addAnimation(bml['re1c_b_cgrass_base.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Grass Assasin": async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_grass.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re1_b_grass_base.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re1_b_grass_base.nj']);
		
		modelLoader.addAnimation(bml['damege_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['die_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['lattack_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['mad_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['rattack_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['spit_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['wait_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['walk_re1_b_base.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Nano Dragoon" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_nanodrago.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['bm6_s_drc_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm6_s_drc_body.nj']);
		
		modelLoader.addAnimation(bml['beam_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['damfly_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['damgrd_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['deadg_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['deads_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['fly_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['joy_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['land_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['lasfly_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['lift_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm6_s_drc_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm6_s_drc_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Poison Lily" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re2_flower.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re2_b_flower_root.pvm']);
		tex.pop();
		tex.pop();

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re2_b_flower_root.nj']);

		modelLoader.addAnimation(bml['attack_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['damege_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['die_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['laugh_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waitc_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waito_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['wake_re2_b_root.njm']);		

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Nar Lily" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re2_flower.bml"]);

		console.log(gsl);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re2_b_flower_root.pvm']);
		tex.shift();
		tex.shift();

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re2_b_flower_root.nj']);

		modelLoader.addAnimation(bml['attack_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['damege_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['die_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['laugh_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waitc_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waito_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['wake_re2_b_root.njm']);		

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Pofuilly Slime (Blue)" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm4_ps_ma_body.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['bm4_ps_ma_body.pvm']);
		let modelLoader = new NinjaModel(this + "_body", tex);
		modelLoader.parse(bml['bm4_ps_ma_body.nj']);
		modelLoader.addAnimation(bml['atack_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['kie_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['nodam_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['wait2_bm4_ps_ma_body.njm']); 
		let body = modelLoader.getModel();
		
		tex = NinjaTexture.API.parse(bml['bm4_ps_ma_tail.pvm']);
		modelLoader = new NinjaModel(this + "_tail", tex);
		modelLoader.parse(bml['bm4_ps_ma_tail.nj']);
		modelLoader.addAnimation(bml['tlatk_bm4_ps_ma_tail.njm']); 
		let tail = modelLoader.getModel();
		
		tex = NinjaTexture.API.parse(bml['bm4_ps_mb_body.pvm']);
		modelLoader = new NinjaModel(this + "_bubble", tex);
		modelLoader.parse(bml['bm4_ps_mbr_body.nj']);
		modelLoader.addAnimation(bml['apear_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['kie_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['move_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm4_ps_mb_body.njm']); 
		let bubble = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, body, [tail, bubble], tex);

	},
	
	"Pouilly Slime (Red)" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm4_ps_ma_body.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['bm4_ps_mar_body.pvm']);
		let modelLoader = new NinjaModel(this + "_body", tex);
		modelLoader.parse(bml['bm4_ps_mar_body.nj']);

		modelLoader.addAnimation(bml['atack_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['kie_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['nodam_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm4_ps_ma_body.njm']); 
		modelLoader.addAnimation(bml['wait2_bm4_ps_ma_body.njm']); 
		let body = modelLoader.getModel();
		
		tex = NinjaTexture.API.parse(bml['bm4_ps_mar_tail.pvm']);
		modelLoader = new NinjaModel(this + "_tail", tex);
		modelLoader.parse(bml['bm4_ps_mar_tail.nj']);
		modelLoader.addAnimation(bml['tlatk_bm4_ps_ma_tail.njm']); 
		let tail = modelLoader.getModel();
		
		tex = NinjaTexture.API.parse(bml['bm4_ps_mbr_body.pvm']);
		modelLoader = new NinjaModel(this + "_bubble", tex);
		modelLoader.parse(bml['bm4_ps_mbr_body.nj']);
		modelLoader.addAnimation(bml['apear_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['kie_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['move_bm4_ps_mb_body.njm']); 
		modelLoader.addAnimation(bml['wait_bm4_ps_mb_body.njm']); 
		let bubble = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, body, [tail, bubble], tex);
		

	},

	"De Rol Le" : async function() {
		
		let ark = await NinjaFile.API.load("bm_boss2_de_rol_le.bml");
		console.log(ark);
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf("b_body.njm") !== -1 && key.indexOf("b_body.njm") === -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				continue;
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				continue;
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['boss2_b_derorure_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['boss2_b_derorure_body.nj']);
		
		modelLoader.addAnimation(bml['beam02_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beamwait_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_a_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_b_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_c_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['bite_lloop_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['bite_rloop_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['die_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['enter_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['fd02_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['fjump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['forward_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['lrjump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['l_bite_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['rljump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['r_bite_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['scatter_boss2_b_body.njm']);
		let mdl = modelLoader.getModel();

		let keys = [
			'boss2_b_derorure_fin_b.nj',
			'boss2_b_derorure_fin_a.nj',
			'boss2_b_derorure_sting.nj',
			'boss2_b_derorure_tentacle.nj',
			'boss2_b_helm_break.nj',
			'boss2_b_shell_break.nj'
		];
	
		let meshList = [];
		keys.forEach(key => {

            const fragmentName = key.split('.').shift();
			let loader = new NinjaModel(fragmentName, tex);
			loader.parse(bml[key]);
			meshList.push(loader.getModel());

		});

		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dubchic" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_dubchik.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['me2_y_me2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dubchic Damaged" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_dubchik.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['me2_y_me2_2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2_2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Garanz" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_gyaranzo.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me4_y_me4.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me4_y_me4.nj']);
		
		modelLoader.addAnimation(bml['attack_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['damage01_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['damage02_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['deth_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['wait_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk01_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk02_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk03_me4_y_me4.njm']);
		let mdl = modelLoader.getModel();

		let meshList = [];
		let keys = [
			{
				pvm : "me4_y_hahen01.pvm",
				nj : "me4_y_hahen01.nj"
			},
			{
				pvm : "me4_y_hahen02.pvm",
				nj : "me4_y_hahen02.nj"
			},
			{
				pvm : "me4_y_hahen03.pvm",
				nj : "me4_y_hahen03.nj"
			},
			{
				pvm : "me4_y_mine.pvm",
				nj : "me4_y_mine.nj"
			},
			{
				pvm : "me4_y_missile.pvm",
				nj : "me4_y_missile.nj"
			}
		];

		let i = 0;
		keys.forEach(key => {
	
			let t = NinjaTexture.API.parse(bml[key.pvm]);
			let loader = new NinjaModel(this + "_" + i, t);
			i++;
			loader.parse(bml[key.nj]);
			meshList.push(loader.getModel());

		});

		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Canadine" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_me1_mb.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me1_y_mb.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me1n_y_mb.nj']);

		modelLoader.addAnimation(bml['change02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['change01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait02_me1_y_mb.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Canane" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_me1_mb.bml"]);

		console.log(gsl);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me1_y_mb.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me1_y_mb.nj']);

		modelLoader.addAnimation(bml['change02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['change01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait02_me1_y_mb.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Sinow Beat" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_me3_shinowa.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me3_y_me3.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me3_y_me3.nj']);

		modelLoader.addAnimation(bml['apper_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['backstep_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['damage_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['death_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['f_attack_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['sword_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['transform_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['t_wait_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['wait_me3_y_me3.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Sinow Gold" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_me3_shinowa.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me3_y_me3.pvm']);
		
		tex = [
			tex[3],
			tex[4],
			tex[5],
			tex[0],
			tex[1],
			tex[2]
		];

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me3_y_me3.nj']);

		modelLoader.addAnimation(bml['apper_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['backstep_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['damage_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['death_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['f_attack_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['sword_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['transform_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['t_wait_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['wait_me3_y_me3.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Vol Opt" : async function() {
		
		let file = await NinjaFile.API.load("bm_boss3_volopt.bml");
		let bml = NinjaFile.API.bml(file);

		console.log(bml);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['me5p02_y_all.pvm']);	
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me5p02_y_all.nj']);

		modelLoader.addAnimation(bml['b_attack_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['damage_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['death_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['f_attack_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['l_attack_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['r_attack_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['start_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['wait_me5p02_y_all.njm']);
		let mdl = modelLoader.getModel();

		const meshList = [];
		const keys = [
			/*
			{
				nj : 'me5p02_y_broken01.nj',
				pvm : 'me5p02_y_broken01.pvm',
				njm : [
					'b_attack_me5p02_y_all.njm',
					'damage_me5p02_y_all.njm',
					'death_me5p02_y_all.njm',
					'f_attack_me5p02_y_all.njm',
					'l_attack_me5p02_y_all.njm',
					'r_attack_me5p02_y_all.njm',
					'start_me5p02_y_all.njm',
					'wait_me5p02_y_all.njm'
				]
			},
			{
				nj : 'me5_y_all.nj',
				pvm : 'me5_y_all.pvm',
				njm : [
					'wait_me5_y_all.njm',
					'damage_me5_y_all.njm'
				]
			},
			{
				nj : 'me5p01_y_all.nj',
				pvm : 'me5p01_y_all.pvm',
				njm : [
					'wait_me5p01_y_all.njm',
					'start_me5p01_y_all.njm',
					'damage_me5p01_y_all.njm',
					'attack_me5p01_y_all.njm'
				]
			},
			*/
			{
				nj : 'fe_obj_hira_kage.nj',
				pvm : 'fe_obj_hira_kage.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_aka.nj',
				pvm : 'fe_obj_vo_mo_dai_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_ao.nj',
				pvm : 'fe_obj_vo_mo_dai_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_hakai.nj',
				pvm : 'fe_obj_vo_mo_dai_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_aka.nj',
				pvm : 'fe_obj_vo_mo_sho01_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_ao.nj',
				pvm : 'fe_obj_vo_mo_sho01_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho01_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_aka.nj',
				pvm : 'fe_obj_vo_mo_sho02_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_ao.nj',
				pvm : 'fe_obj_vo_mo_sho02_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho02_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_aka.nj',
				pvm : 'fe_obj_vo_mo_sho03_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_ao.nj',
				pvm : 'fe_obj_vo_mo_sho03_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho03_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_futa_moto.nj',
				pvm : 'fe_obj_vo_futa_moto.pvm'
			},
			{
				nj : 'fe_obj_vo_tenjo_hahen01.nj',
				pvm : 'fe_obj_vo_tenjo_hahen01.pvm'
			},
			{
				nj : 'fe_obj_vo_tenjo_hahen02.nj',
				pvm : 'fe_obj_vo_tenjo_hahen02.pvm'
			},
			{
				nj : 'fs_obj_hiraishin_a.nj',
				pvm : 'fs_obj_hiraishin_a.pvm',
				/*
				njm : [
					'fs_obj_hiraishin_a.njm',
					'fs_obj_hiraishin_b.njm',
					'fs_obj_hiraishin_c.njm'
				]
				*/
			},
			{
				nj : 'me5p02_y_cage.nj',
				pvm : 'me5p02_y_cage.pvm'
			},
			{
				nj : 'me5p02_y_missile.nj',
				pvm : 'me5p02_y_missile.pvm'
			},
			{
				nj : 'me5p02_y_pillar.nj',
				pvm : 'me5p02_y_pillar.pvm'
			}
		];

		let i = 0;
		keys.forEach(key => {
			
			let t = NinjaTexture.API.parse(bml[key.pvm]);
			let loader = new NinjaModel(this + "_" + i, t);
			loader.parse(bml[key.nj]);
			i++;

			console.log("reaing animations");

			let njm = key.njm || [];
			for(let i = 0; i < njm.length; i++) {
				modelLoader.addAnimation(bml[key.njm[i]]);
			}

			meshList.push(loader.getModel());

		});
		
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Bulclaw (Open)" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_balclaw.bml"]);
	
		for(let key in bml) {
			if(key.indexOf("bal_body.njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml["re6_b_bal_body.pvm"]);	
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re6_b_bal_body.nj"]);
		
		modelLoader.addAnimation(bml['balattack_re6_b_bal_body.njm']);
		modelLoader.addAnimation(bml['balcomb_re6_b_bal_body.njm']);
		modelLoader.addAnimation(bml['baldamage_re6_b_bal_body.njm']);
		modelLoader.addAnimation(bml['baldie_re6_b_bal_body.njm']);
		modelLoader.addAnimation(bml['balshout_re6_b_bal_body.njm']);
		modelLoader.addAnimation(bml['balwait_re6_b_bal_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Bulclaw (Closed)" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_balclaw.bml"]);
	
		for(let key in bml) {
			if(key.indexOf("bcbody.njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml["re6_b_bcbody.pvm"]);	
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re6_b_bcbody.nj"]);

		modelLoader.addAnimation(bml['bcattack_re6_b_bcbody.njm']);
		modelLoader.addAnimation(bml['bcdamage_re6_b_bcbody.njm']);
		modelLoader.addAnimation(bml['bcdie_re6_b_bcbody.njm']);
		modelLoader.addAnimation(bml['bcwait_re6_b_bcbody.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Claw" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_balclaw.bml"]);
	
		for(let key in bml) {
			if(key.indexOf("claw_body.njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml["re6_b_claw_body.pvm"]);	
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re6_b_claw_body.nj"]);

		modelLoader.addAnimation(bml['clattack_re6_b_claw_body.njm']);
		modelLoader.addAnimation(bml['cldamage_re6_b_claw_body.njm']);
		modelLoader.addAnimation(bml['cldie_re6_b_claw_body.njm']);
		modelLoader.addAnimation(bml['cllturn_re6_b_claw_body.njm']);
		modelLoader.addAnimation(bml['clrturn_re6_b_claw_body.njm']);
		modelLoader.addAnimation(bml['clwait_re6_b_claw_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Delsaber" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_df1_saver.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml["df1_s_kil_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["df1_s_kil_body.nj"]);
		
		modelLoader.addAnimation(bml['aseri_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['atack_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['damage_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['deadb_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['dead_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['defdam_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['defence_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['jump_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['nail_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_kil_body.njm']);
		modelLoader.addAnimation(bml['walk_df1_s_kil_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dimenian" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_df3_dimedian.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_s_wala_body.nj']);
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"La Dimenian" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_df3_dimedian.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_ssl_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_ssl_wala_body.nj']);	
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"So Dimenian" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_df3_dimedian.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_sl_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_sl_wala_body.nj']);
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Chaos Sorcerer" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re4_sorcerer.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re4_b_sorcer_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re4_b_sorcer_body.nj']);
				
		modelLoader.addAnimation(bml['attack2_re4_b_body.njm']);
		modelLoader.addAnimation(bml['attack3_re4_b_body.njm']);
		modelLoader.addAnimation(bml['attack1_re4_b_body.njm']);
		modelLoader.addAnimation(bml['cure_re4_b_body.njm']);
		modelLoader.addAnimation(bml['damage_re4_b_body.njm']);
		modelLoader.addAnimation(bml['die_re4_b_body.njm']);
		modelLoader.addAnimation(bml['enter_re4_b_body.njm']);
		modelLoader.addAnimation(bml['wait_re4_b_body.njm']);

		let loader = new NinjaModel(this + "_spell0", tex);
		loader.parse(bml['re4_b_bit.nj']);
		let spell = loader.getModel();
		
		let meshList = [];
		for(let i = 0; i < 4; i++) {
			let geo = spell.geometry.clone();
			let mat = spell.material[0].clone();
			mat.map = tex[3 + i];
			meshList.push(new THREE.Mesh(geo, [mat]));
		}
		
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Belra" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_re7_berura.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re7_b_bell_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re7_b_bell_body.nj']);

		modelLoader.addAnimation(bml['attack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['damege_re7_b_body.njm']);
		modelLoader.addAnimation(bml['die_re7_b_body.njm']);
		modelLoader.addAnimation(bml['lattack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['memai_re7_b_body.njm']);
		modelLoader.addAnimation(bml['rattack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['wait_re7_b_body.njm']);
		modelLoader.addAnimation(bml['walk_re7_b_body.njm']);		
		
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dark Gunner" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient03.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_darkgunner.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re5_b_gunner_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re5_b_gunner_body.nj']);

		modelLoader.addAnimation(bml['attack_re5_b_body.njm']);
		modelLoader.addAnimation(bml['await_re5_b_body.njm']);
		modelLoader.addAnimation(bml['damage2_re5_b_body.njm']);
		modelLoader.addAnimation(bml['damage_re5_b_body.njm']);
		modelLoader.addAnimation(bml['die_re5_b_body.njm']);
		modelLoader.addAnimation(bml['drop_re5_b_body.njm']);
		modelLoader.addAnimation(bml['duckdame_re5_b_body.njm']);
		modelLoader.addAnimation(bml['duckdie_re5_b_body.njm']);
		modelLoader.addAnimation(bml['duckroop_re5_b_body.njm']);
		modelLoader.addAnimation(bml['duckwake_re5_b_body.njm']);
		modelLoader.addAnimation(bml['duck_re5_b_body.njm']);
		modelLoader.addAnimation(bml['move_re5_b_body.njm']);
		modelLoader.addAnimation(bml['pullback_re5_b_body.njm']);
		modelLoader.addAnimation(bml['wait_re5_b_body.njm']);
		let mdl = modelLoader.getModel();

		let meshList = [];
		let keys = [
			{
				name : "_green",
				pvm : "re5_b_cly_cy_line.pvm",
				nj : "re5_b_cly_cy_line.nj"
			},
			{
				name : "_red",
				pvm : "re5_b_rcly_cy_line.pvm",
				nj : "re5_b_rcly_cy_line.nj"
			},
			{
				name : "_wave",
				pvm : "shockwave.pvm",
				nj : "shockwave.nj"
			}
		];

		keys.forEach(key => {
	
			let t = NinjaTexture.API.parse(bml[key.pvm]);
			let loader = new NinjaModel(this + key.name, t);
			loader.parse(bml[key.nj]);
			meshList.push(loader.getModel());

		});

		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Chaos Bringer" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_ancient03.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_df2_bringer.bml"]);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['bm8_s_kb_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm8_s_kb_body.nj']);

		modelLoader.addAnimation(bml['beam_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['cold_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['damage_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['dead_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['hoe_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['kamae_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['kiri_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['run_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['tpkyu_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['wait_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['walk_bm8_s_kb_body.njm']);

		let mdl = modelLoader.getModel();

		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"DarkFalz Form 1 Body" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df1_s_body") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_body.nj'])	
		
		modelLoader.addAnimation(bml['beaml_df1_s_body.njm']);
		modelLoader.addAnimation(bml['beamr_df1_s_body.njm']);
		modelLoader.addAnimation(bml['damage_df1_s_body.njm']);
		modelLoader.addAnimation(bml['dead1_df1_s_body.njm']);
		modelLoader.addAnimation(bml['df1op_df1_s_body.njm']);
		modelLoader.addAnimation(bml['df2op_df1_s_body.njm']);
		modelLoader.addAnimation(bml['hoe_df1_s_body.njm']);
		modelLoader.addAnimation(bml['lhassya_df1_s_body.njm']);
		modelLoader.addAnimation(bml['ltame_df1_s_body.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_body.njm']);


		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"DarkFalz Form 1 Head A" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df1_s_da") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_da_heada.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_da_heada.nj']);
		
		modelLoader.addAnimation(bml['damage_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['dead1_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df1op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df2op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['haki_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiin_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiout_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hoe_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_da_heada.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"DarkFalz Form 1 Head B" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df1_s") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_db_heada.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_db_heada.nj']);
		
		modelLoader.addAnimation(bml['damage_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['dead1_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df1op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df2op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['haki_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiin_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiout_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hoe_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_da_heada.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"DarkFalz Form 1 Head C" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df1_s") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_dc_heada.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_dc_heada.nj']);
		
		modelLoader.addAnimation(bml['damage_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['dead1_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df1op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['df2op_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['haki_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiin_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hakiout_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['hoe_df1_s_da_heada.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_da_heada.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"DarkFalz Form 1 Base" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df1_s_dodai") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_dodai.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_dodai.nj']);
		
		modelLoader.addAnimation(bml['damage_df1_s_dodai.njm']);
		modelLoader.addAnimation(bml['wait_df1_s_dodai.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 1 Blades" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("simobe") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_simobe.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_simobe.nj']);
		
		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 1 Waist" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("waist") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df1_s_waist.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df1_s_waist.nj']);
		
		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 2 Body" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df2_s_body") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df2_s_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df2_s_body.nj']);
				
		modelLoader.addAnimation(bml['beaml_df2_s_body.njm']);
		modelLoader.addAnimation(bml['beamr_df2_s_body.njm']);
		modelLoader.addAnimation(bml['damage_df2_s_body.njm']);
		modelLoader.addAnimation(bml['dead_df2_s_body.njm']);
		modelLoader.addAnimation(bml['df2op_df2_s_body.njm']);
		modelLoader.addAnimation(bml['df3_op_df2_s_body.njm']);
		modelLoader.addAnimation(bml['hoe_df2_s_body.njm']);
		modelLoader.addAnimation(bml['jisin_df2_s_body.njm']);
		modelLoader.addAnimation(bml['lhassya_df2_s_body.njm']);
		modelLoader.addAnimation(bml['ltame_df2_s_body.njm']);
		modelLoader.addAnimation(bml['wait_df2_s_body.njm']);
		modelLoader.addAnimation(bml['wing_df2_s_body.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 2 Base" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df2_s_dodai") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df2_s_dodai1.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df2_s_dodai1.nj'])

		modelLoader.addAnimation(bml['beaml_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['beamr_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['damage_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['dead_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['df2op_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['df3_op_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['hoe_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['jisin_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['lhassya_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['wait_df2_s_dodai1.njm']);
		modelLoader.addAnimation(bml['wing_df2_s_dodai1.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 3 Body" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df3_s_body") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_s_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_s_body.nj']);
		
		console.log("aaa4");
		//modelLoader.addAnimation(bml['damage_df3_s_body.njm']);
		modelLoader.addAnimation(bml['dead_df3_s_body.njm']);
		modelLoader.addAnimation(bml['df3_op_df3_s_body.njm']);
		//modelLoader.addAnimation(bml['df3dead_df3_s_body.njm']);
		console.log("aaa3");
		modelLoader.addAnimation(bml['dwntec_df3_s_body.njm']);
		modelLoader.addAnimation(bml['jyousyou_df3_s_body.njm']);
		modelLoader.addAnimation(bml['kakou_df3_s_body.njm']);
		modelLoader.addAnimation(bml['laser_df3_s_body.njm']);
		console.log("aaa2");
		modelLoader.addAnimation(bml['lkiri_df3_s_body.njm']);
		modelLoader.addAnimation(bml['rkiri_df3_s_body.njm']);
		modelLoader.addAnimation(bml['wait_df3_s_body.njm']);
		console.log("aaa1");
		modelLoader.addAnimation(bml['yubi_df3_s_body.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 3 Wing" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("s_wing") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_s_wing.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_s_wing.nj']);
		
		modelLoader.addAnimation(bml['damage_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['df3_op_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['jyousyou_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['kakou_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['laser_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['lkiri_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['rkiri_df3_s_wing.njm']);
		modelLoader.addAnimation(bml['wait_df3_s_wing.njm']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 3 BodyS" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df3_sl_body") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_sl_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_sl_body.nj']);
		
		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Dark Falz Form 3 WingS" : async function() {
		
		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1 && key.indexOf("df3_sl_wing") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				//console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				//console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_sl_wing.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_sl_wing.nj']);
		
		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Sil Dragon" : async function() {

		let ark = await NinjaFile.API.load("bm_boss1_dragon_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["boss1_s_nb_dragon.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["boss1_s_nb_dragon.nj"]);

		modelLoader.addAnimation(bml["daml_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["dams_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["dead_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["down_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["fire_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["fly_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["flyshot_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frin_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frloop_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["frout_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["kiri_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["land_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["lift_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nkdown_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nkup_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["nobi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["stand_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tatk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tobidasi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["tukomi_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["walk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wgwalk_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wing_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wngclose_boss1_s_nb_dragon.njm"]);
		modelLoader.addAnimation(bml["wngopn_boss1_s_nb_dragon.njm"]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);


	},

	"Dal Ral Lie" : async function() {
		
		let ark = await NinjaFile.API.load("bm_boss2_de_rol_le_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf("b_body.njm") !== -1 && key.indexOf("b_body.njm") === -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				continue;
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				continue;
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['boss2_b_derorure_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['boss2_b_derorure_body.nj']);
		
		modelLoader.addAnimation(bml['beam02_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beamwait_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_a_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_b_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['beam_c_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['bite_lloop_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['bite_rloop_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['die_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['enter_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['fd02_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['fjump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['forward_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['lrjump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['l_bite_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['rljump_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['r_bite_boss2_b_body.njm']);
		modelLoader.addAnimation(bml['scatter_boss2_b_body.njm']);
		let mdl = modelLoader.getModel();

		let keys = [
			'boss2_b_derorure_fin_b.nj',
			'boss2_b_derorure_fin_a.nj',
			'boss2_b_derorure_sting.nj',
			'boss2_b_derorure_tentacle.nj',
			'boss2_b_helm_break.nj',
			'boss2_b_shell_break.nj'
		];
	
		let meshList = [];
		keys.forEach(key => {

            const frament = key.split('.').shift();
			let loader = new NinjaModel(fragment, tex);
			loader.parse(bml[key]);
			meshList.push(loader.getModel());

		});

		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Vulmer" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_bm1_shark_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1_s_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"GoVulmer" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_bm1_shark_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		console.log(bml);
			
		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1f_s1_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1f_s1_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Melqueek" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_bm1_shark_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		let str = "";

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['bm1tl_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm1tl_s_wala_body.nj']);

		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']); 
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hidelt Baby" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm2_moja_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm2c_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2c_s_moj_body.nj"]);
	
		console.log(bml);

		console.log("a");
		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		console.log("b");
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		console.log("c");
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		console.log("d");
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		console.log("e");
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		console.log("f");
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		console.log("g");
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		console.log("h");
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		console.log("i");
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		console.log("j");
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		console.log("k");
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hidelt" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm2_moja_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm2f_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2f_s_moj_body.nj"]);

		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Hildetor" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm2_moja_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm2w_s_moj_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm2w_s_moj_body.nj"]);

		modelLoader.addAnimation(bml["cstand_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["cwalk_bm2c_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["giva_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["jump_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["punch_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["stand_bm2f_s_moj_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm2f_s_moj_body.njm"]);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Mothvist" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm3_fly_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm3_fly_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm3_s_nest.nj"]);
		modelLoader.addAnimation(bml["dam_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dead_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["down_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwndam_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwnexit_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["dwnwait_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["exit_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["land_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["trance_bm3_s_nest.njm"]);
		modelLoader.addAnimation(bml["wait_bm3_s_nest.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Mothvert" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm3_fly_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm3_fly_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm3_fly_body.nj"]);
		modelLoader.addAnimation(bml["atack_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["fly_bm3_fly_body.njm"]);
		modelLoader.addAnimation(bml["move_bm3_fly_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Gulgus" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm5_wolf_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm5_s_kem_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm5_s_kem_body.nj"]);
		modelLoader.addAnimation(bml["dams_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["deadr_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["eat_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hunt_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okil_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okir_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["run_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["runb_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["sleep_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["stdup_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["wait_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm5_s_kem_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Gulgus-gue" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_bm5_wolf_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["bm5_s_kem_body.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["bm5_s_keml_body.nj"]);
		modelLoader.addAnimation(bml["dams_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["deadr_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["eat_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hoe_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["hunt_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okil_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["okir_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["run_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["runb_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["sleep_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["stdup_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["wait_bm5_s_kem_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm5_s_kem_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Mini Crimson Assassin": async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_cgrass_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['re1c_b_cgrass_base.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re1c_b_cgrass_base.nj']);
		
		modelLoader.addAnimation(bml['re1c_b_cgrass_base.njm']);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Crimson Assassin": async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_grass_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re1_b_grass_base.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re1_b_grass_base.nj']);
		
		modelLoader.addAnimation(bml['damege_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['die_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['lattack_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['mad_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['rattack_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['spit_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['wait_re1_b_base.njm']); 
		modelLoader.addAnimation(bml['walk_re1_b_base.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dark Bringer" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_df2_bringer_a.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['bm8_s_kb_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['bm8_s_kb_body.nj']);

		modelLoader.addAnimation(bml['beam_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['cold_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['damage_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['dead_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['hoe_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['kamae_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['kiri_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['run_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['tpkyu_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['wait_bm8_s_kb_body.njm']);
		modelLoader.addAnimation(bml['walk_bm8_s_kb_body.njm']);

		let mdl = modelLoader.getModel();

		let meshList = [];
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Arlan" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_df3_dimedian_a.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_s_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_s_wala_body.nj']);
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Merlan" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_df3_dimedian_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_ssl_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_ssl_wala_body.nj']);	
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Del-D" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_df3_dimedian_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['df3_sl_wala_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df3_sl_wala_body.nj']);
				
		modelLoader.addAnimation(bml['appear_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackl_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['atackr_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['damage_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['deadb_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['dead_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['leader_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['mihari_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['run_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['stund_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['wakeup_bm1_s_wala_body.njm']);
		modelLoader.addAnimation(bml['walk_bm1_s_wala_body.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dubchich" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_dubchik_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['me2_y_me2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Dubchich Damaged" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_dubchik_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['me2_y_me2_2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2_2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Baranz" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_gyaranzo_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me4_y_me4.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me4_y_me4.nj']);
		
		modelLoader.addAnimation(bml['attack_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['damage01_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['damage02_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['deth_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['wait_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk01_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk02_me4_y_me4.njm']);
		modelLoader.addAnimation(bml['walk03_me4_y_me4.njm']);
		let mdl = modelLoader.getModel();

		let meshList = [];
		let keys = [
			{
				pvm : "me4_y_hahen01.pvm",
				nj : "me4_y_hahen01.nj"
			},
			{
				pvm : "me4_y_hahen02.pvm",
				nj : "me4_y_hahen02.nj"
			},
			{
				pvm : "me4_y_hahen03.pvm",
				nj : "me4_y_hahen03.nj"
			},
			{
				pvm : "me4_y_mine.pvm",
				nj : "me4_y_mine.nj"
			},
			{
				pvm : "me4_y_missile.pvm",
				nj : "me4_y_missile.nj"
			}
		];

		let i = 0;
		keys.forEach(key => {
	
			let t = NinjaTexture.API.parse(bml[key.pvm]);
			let loader = new NinjaModel(this + "_" + i, t);
			i++;
			loader.parse(bml[key.nj]);
			meshList.push(loader.getModel());

		});

		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Canabin" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_me1_mb_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me1_y_mb.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me1n_y_mb.nj']);

		modelLoader.addAnimation(bml['change02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['change01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait02_me1_y_mb.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Canune" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_me1_mb_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me1_y_mb.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me1_y_mb.nj']);

		modelLoader.addAnimation(bml['change02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['change01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['damage02_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait01_me1_y_mb.njm']);
		modelLoader.addAnimation(bml['wait02_me1_y_mb.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Sinow Blue" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_me3_shinowa_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me3_y_me3.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me3_y_me3.nj']);

		modelLoader.addAnimation(bml['apper_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['backstep_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['damage_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['death_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['f_attack_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['sword_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['transform_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['t_wait_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['wait_me3_y_me3.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Sinow Red" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_me3_shinowa_a.bml");
		let bml = NinjaFile.API.bml(ark);
		
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
	
		let tex = NinjaTexture.API.parse(bml['me3_y_me3.pvm']);
		
		tex = [
			tex[3],
			tex[4],
			tex[5],
			tex[0],
			tex[1],
			tex[2]
		];

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me3_y_me3.nj']);

		modelLoader.addAnimation(bml['apper_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['backstep_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['damage_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['death_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['f_attack_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['sword_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['transform_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['t_wait_me3_y_me3.njm']);
		modelLoader.addAnimation(bml['wait_me3_y_me3.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Ob Lily" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_re2_flower_a.bml");
		let bml = NinjaFile.API.bml(ark);

		console.log(bml);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['flower_root.pvm']);
		tex.pop();
		tex.pop();

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['flower_root.nj']);

		modelLoader.addAnimation(bml['attack_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['damege_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['die_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['laugh_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waitc_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waito_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['wake_re2_b_root.njm']);		

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Mil Lily" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_re2_flower_a.bml");
		let bml = NinjaFile.API.bml(ark);
		

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['flower_root.pvm']);
		tex.shift();
		tex.shift();

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['flower_root.nj']);

		modelLoader.addAnimation(bml['attack_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['damege_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['die_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['laugh_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waitc_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['waito_re2_b_root.njm']); 
		modelLoader.addAnimation(bml['wake_re2_b_root.njm']);		

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Gran Sorcerer" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_re4_sorcerer_a.bml");
		let bml = NinjaFile.API.bml(ark);
	
		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re4_b_sorcer_body.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re4_b_sorcer_body.nj']);
				
		modelLoader.addAnimation(bml['attack2_re4_b_body.njm']);
		modelLoader.addAnimation(bml['attack3_re4_b_body.njm']);
		modelLoader.addAnimation(bml['attack1_re4_b_body.njm']);
		modelLoader.addAnimation(bml['cure_re4_b_body.njm']);
		modelLoader.addAnimation(bml['damage_re4_b_body.njm']);
		modelLoader.addAnimation(bml['die_re4_b_body.njm']);
		modelLoader.addAnimation(bml['enter_re4_b_body.njm']);
		modelLoader.addAnimation(bml['wait_re4_b_body.njm']);

		let loader = new NinjaModel(this + "_spell0", tex);
		loader.parse(bml['re4_b_bit.nj']);
		let spell = loader.getModel();
		
		let meshList = [];
		for(let i = 0; i < 4; i++) {
			let geo = spell.geometry.clone();
			let mat = spell.material[0].clone();
			mat.map = tex[3 + i];
			meshList.push(new THREE.Mesh(geo, [mat]));
		}
		
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"Indi Belra" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_re7_berura_a.bml");
		let bml = NinjaFile.API.bml(ark);
	
		console.log(bml);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let tex = NinjaTexture.API.parse(bml['re7_b_bell_body.pvm']);
		console.log(tex);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['re7_b_bell_body.nj']);

		modelLoader.addAnimation(bml['attack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['damege_re7_b_body.njm']);
		modelLoader.addAnimation(bml['die_re7_b_body.njm']);
		modelLoader.addAnimation(bml['lattack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['memai_re7_b_body.njm']);
		modelLoader.addAnimation(bml['rattack_re7_b_body.njm']);
		modelLoader.addAnimation(bml['wait_re7_b_body.njm']);
		modelLoader.addAnimation(bml['walk_re7_b_body.njm']);		
		
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Bartle" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_re8_b_beast_a.bml");
		let bml = NinjaFile.API.bml(ark);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml["re8_b_beast_wala_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_b_beast_wala_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);


	},

	"Barble" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_re8_b_beast_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["re8_b_srbeast_wala_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_b_srbeast_wala_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Tollaw" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_re8_b_beast_a.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml["re8_rdbeast_wala_body.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re8_rdbeast_wala_body.nj"]);
		modelLoader.addAnimation(bml["appear_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackl_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["atackr_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["damage_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["dead_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["deadb_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["leader_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["mihari_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["run_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["stund_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["wakeup_bm1_s_wala_body.njm"]);
		modelLoader.addAnimation(bml["walk_bm1_s_wala_body.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Gillchich" : async function() {
		
		let ark = await NinjaFile.API.load("bm_ene_dubchik_a.bml");
		let bml = NinjaFile.API.bml(ark);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let pvm = await NinjaFile.API.load('me2_y_me2_z_a.pvm');
		let tex = NinjaTexture.API.parse(pvm);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Gilchic" : async function() {
		
		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_ene_dubchik.bml"]);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s']", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}
		
		let pvm = await NinjaFile.API.load('me2_y_me2_z.pvm');
		let tex = NinjaTexture.API.parse(pvm);
		//let tex = NinjaTexture.API.parse(bml['me2_y_me2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me2_y_me2.nj']);

		modelLoader.addAnimation(bml['damage_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['damage_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['kamae02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_b_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['revival_f_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['scratch01_me2_me2.njm']);
		modelLoader.addAnimation(bml['scratch02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['shoot02_me2_me2.njm']);
		modelLoader.addAnimation(bml['starting_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['wait02_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk01_me2_y_me2.njm']);
		modelLoader.addAnimation(bml['walk02_me2_y_me2.njm']);

		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Vol Opt Version 2" : async function() {
		
		let file = await NinjaFile.API.load("bm_boss3_volopt_ap.bml");
		let bml = NinjaFile.API.bml(file);

		console.log(bml);

		for(let key in bml) {
			if(key.indexOf(".njm") !== -1) {
				console.log("modelLoader.addAnimation(bml['%s']);", key)
			} else if(key.indexOf(".nj") !== -1) {
				console.log("modelLoader.parse(bml['%s'])", key);
			} else if(key.indexOf(".pvm") !== -1) {
				console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
			}
		}

		let tex = NinjaTexture.API.parse(bml['me5p02_y_all.pvm']);	
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['me5p02_y_all.nj']);

		modelLoader.addAnimation(bml['b_attack_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['damage_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['death_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['f_attack_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['l_attack_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['r_attack_me5p02_y_all.njm']);
		modelLoader.addAnimation(bml['start_me5p02_y_all.njm']);	
		modelLoader.addAnimation(bml['wait_me5p02_y_all.njm']);
		let mdl = modelLoader.getModel();

		const meshList = [];
		const keys = [
			/*
			{
				nj : 'me5p02_y_broken01.nj',
				pvm : 'me5p02_y_broken01.pvm',
				njm : [
					'b_attack_me5p02_y_all.njm',
					'damage_me5p02_y_all.njm',
					'death_me5p02_y_all.njm',
					'f_attack_me5p02_y_all.njm',
					'l_attack_me5p02_y_all.njm',
					'r_attack_me5p02_y_all.njm',
					'start_me5p02_y_all.njm',
					'wait_me5p02_y_all.njm'
				]
			},
			{
				nj : 'me5_y_all.nj',
				pvm : 'me5_y_all.pvm',
				njm : [
					'wait_me5_y_all.njm',
					'damage_me5_y_all.njm'
				]
			},
			{
				nj : 'me5p01_y_all.nj',
				pvm : 'me5p01_y_all.pvm',
				njm : [
					'wait_me5p01_y_all.njm',
					'start_me5p01_y_all.njm',
					'damage_me5p01_y_all.njm',
					'attack_me5p01_y_all.njm'
				]
			},
			*/
			{
				nj : 'fe_obj_hira_kage.nj',
				pvm : 'fe_obj_hira_kage.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_aka.nj',
				pvm : 'fe_obj_vo_mo_dai_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_ao.nj',
				pvm : 'fe_obj_vo_mo_dai_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_dai_hakai.nj',
				pvm : 'fe_obj_vo_mo_dai_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_aka.nj',
				pvm : 'fe_obj_vo_mo_sho01_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_ao.nj',
				pvm : 'fe_obj_vo_mo_sho01_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho01_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho01_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_aka.nj',
				pvm : 'fe_obj_vo_mo_sho02_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_ao.nj',
				pvm : 'fe_obj_vo_mo_sho02_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho02_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho02_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_aka.nj',
				pvm : 'fe_obj_vo_mo_sho03_aka.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_ao.nj',
				pvm : 'fe_obj_vo_mo_sho03_ao.pvm'
			},
			{
				nj : 'fe_obj_vo_mo_sho03_hakai.nj',
				pvm : 'fe_obj_vo_mo_sho03_hakai.pvm'
			},
			{
				nj : 'fe_obj_vo_futa_moto.nj',
				pvm : 'fe_obj_vo_futa_moto.pvm'
			},
			{
				nj : 'fe_obj_vo_tenjo_hahen01.nj',
				pvm : 'fe_obj_vo_tenjo_hahen01.pvm'
			},
			{
				nj : 'fe_obj_vo_tenjo_hahen02.nj',
				pvm : 'fe_obj_vo_tenjo_hahen02.pvm'
			},
			{
				nj : 'fs_obj_hiraishin_a.nj',
				pvm : 'fs_obj_hiraishin_a.pvm',
				/*
				njm : [
					'fs_obj_hiraishin_a.njm',
					'fs_obj_hiraishin_b.njm',
					'fs_obj_hiraishin_c.njm'
				]
				*/
			},
			{
				nj : 'me5p02_y_cage.nj',
				pvm : 'me5p02_y_cage.pvm'
			},
			{
				nj : 'me5p02_y_missile.nj',
				pvm : 'me5p02_y_missile.pvm'
			},
			{
				nj : 'me5p02_y_pillar.nj',
				pvm : 'me5p02_y_pillar.pvm'
			}
		];

		let i = 0;
		keys.forEach(key => {
			
			let t = NinjaTexture.API.parse(bml[key.pvm]);
			let loader = new NinjaModel(this + "_" + i, t);
			loader.parse(bml[key.nj]);
			i++;

			console.log("reaing animations");

			let njm = key.njm || [];
			for(let i = 0; i < njm.length; i++) {
				modelLoader.addAnimation(bml[key.njm[i]]);
			}

			meshList.push(loader.getModel());

		});
		
		NinjaPlugin.API.setModel(this, mdl, meshList, tex);

	},

	"El Rappy" : async function () {

		let ark = await NinjaFile.API.load("bm_ene_lappy_ap.bml");
		let bml = NinjaFile.API.bml(ark);
		let tex = NinjaTexture.API.parse(bml["re3_b_lappy_base.pvm"]);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re3_b_lappy_base.nj"]);
		modelLoader.addAnimation(bml["attack_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["damage_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["die_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["run_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["tumble_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["walk_re3_b_base.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},

	"Pal Rappy" : async function() {

		let ark = await NinjaFile.API.load("bm_ene_lappy_ap.bml");
		let bml = NinjaFile.API.bml(ark);

		let pvm = await NinjaFile.API.load('re3_b_lappy_base_niji.pvm');
		let tex = NinjaTexture.API.parse(pvm);
		
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["re3_b_lappy_base.nj"]);
		modelLoader.addAnimation(bml["attack_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["damage_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["die_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["run_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["tumble_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wait2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["wake2_re3_b_base.njm"]);
		modelLoader.addAnimation(bml["walk_re3_b_base.njm"]);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], tex);

	},


};

