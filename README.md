# Phantasy Star Online V2

This is a repository for a web tool which is able to read the asset files from Phantasy Star Online Version 2 for Dreamcast and PC. The assets can be previewed in the viewer and then exported as various formats. The viewer is build with Threejs and caches assets in the browser to read them.

![psov2 viewer tool ninja tool](./figs/forest-preview.png)

## Live Version

The live version of this tool can be found [here](https://dashgl.gitlab.io/psov2/).

## Running Locally

This directory can be run locally by copying the contents of the `public` folder to an http server such as Apache or Nginx. Assuming you have [Nodejs](https://nodejs.org) installed, the easiest way to run this directory locally is with
the `serve` command from `npm`.

```
npm i serve -g
git clone https://gitlab.com/dashgl/psov2.git
cd psov2
serve public
```

From there you will be able to see the tool from `http://localhost:3000` in your browser.

## Directory Structure

The tool is built with normal html, css and javascript files inside the `public` directory.

- `./public/css/` contains the `layout.css` file for styling
- `./public/dat/` contains the assets in their original format
- `./public/js/` contains the files needed to parse and display the assets
- `./public/img/` contains images which are used in `index.html`
- `./public/lib/` contains open source libraries which were used for this tool
- `./public/index.html` contains the index html file which loads in the other needed files

## License

Source code is available under the GNU GPLv3 license.
