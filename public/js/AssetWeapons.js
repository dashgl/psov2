/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


const AssetWeapons = {

	"Saber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[0]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[0]);
		let mdl = modelLoader.getModel();
		console.log(mdl);
		mdl.material[1].side = THREE.DoubleSide;
		mdl.material[1].blending = 2;
		mdl.material[1].depthWrite = false;
		mdl.material[2].side = THREE.DoubleSide;
		mdl.material[2].blending = 2;
		mdl.material[2].depthWrite = false;

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[1]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[1]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Dagger" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[2]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[2]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Partisan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[3]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[3]);
		let mdl = modelLoader.getModel();

		mdl.material[2].blending = 2;
		mdl.material[3].blending = 2;

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Slicer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);
		let tex = NinjaTexture.API.parse(texList[4]);

		// Get Slicer

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[4]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[4]);
		let bullet = modelLoader.getModel();

		// Set Model

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Handgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[5]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[5]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[6]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[6]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Mechgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[7]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[7]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Shot" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[8]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[8]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Cane" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[9]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[9]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Rod" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[10]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[10]);
		let mdl = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Wand" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[11], [0]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[11]);
		let mdl = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Claw" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[12]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[12]);
		let mdl = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"DoubleSaber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[13]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[13]);
		let mdl = modelLoader.getModel();
	
		mdl.material.forEach(mat => {

			if(!mat.map) {
				return;
			}

			switch(mat.map.name) {
			case "wxtS01_g_e_saver1":
			case "wxtS01_g_e_saver2":
				mat.side = THREE.DoubleSide;
				mat.blending = 2;
				mat.depthWrite = false;
				break;
			}

		});


		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sonic Knuckle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[14]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[14]);
		let mdl = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Orotiagito" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[15]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[15]);
		let mdl = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Soul Eater" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[16]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[16]);
		let mdl = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Spread Needle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[17]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[17]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[17]);
		let bullet = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Holy Ray" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[18]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[18]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[18]);
		let bullet = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Inferno Bazooka" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[19]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[19]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[19]);
		let bullet = modelLoader.getModel();
		
		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Flame Visit" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[20]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[20]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Akiko" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[21]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[21]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sorcerer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[22]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[22]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Beat" : async function() {


		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[23]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[23]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[23]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"P-Arm" : async function() {


		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[24]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[24]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Delsaber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[25]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[25]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Bringer Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[26]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[26]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Egg Blaster" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[27]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[27]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Psycho Wand" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[28]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[28]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Heavens Punisher" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[29]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[29]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Lavis Cannon" : async function() {


		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[30]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[30]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Victor Axe" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[31]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[31]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Chain Sawd" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[32]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[32]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		mdl.material.forEach(mat => {
			
			if(!mat.map) {
				return;
			}

			switch(mat.map.name) {
			case "wxtS02b_z_w_chain01":
				mat.side = THREE.DoubleSide;
				break;
			}

		});

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Caduceus" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[33]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[33]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sting Tip" : async function() {


		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[34]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[34]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);
	},

	"Magical Piece" : async function() {


		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[35]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[35]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Technical Crozier" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[36]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[36]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Suppressed Gun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[37]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[37]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ancient Saber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[38]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[38]);
		let mdl = modelLoader.getModel();
		
		let debugMat = new THREE.MeshNormalMaterial();
		let debug = new THREE.Mesh(mdl.geometry, debugMat);
		debug.name = this + "_debug";
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [debug], tex);

	},

	"Harisen Battle Fan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[39]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[39]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Yamigarasu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[40]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[40]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Akiko Wok" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[41]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[41]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		mdl.material[0].side = THREE.DoubleSide;

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Toy Hammer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[42]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[42]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Elysion" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[43]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[43]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		mdl.material.forEach(mat => {

			if(!mat.map) {
				return;
			}

			switch(mat.map.name) {
			case "wxtS01_m_t_mahouken_ks":
			case "wxtS01_m_t_mahouken2":
				mat.blending = 2;
				mat.side = THREE.DoubleSide;
				break;
			}

		});

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Saber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[44]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[44]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Meteor Cudgel" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[45]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[45]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		mdl.material.forEach(mat => {

			if(!mat.map) {
				return;
			}

			switch(mat.map.name) {
			case "wxtS01g_z_w_bou02":
				mat.blending = 2;
				mat.side = THREE.DoubleSide;
				break;
			}

		});

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Monkey King Bar" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[46]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[46]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Double Cannon" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[47]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[47]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Huge Battle Fan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[48]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[48]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		mdl.material[0].side = THREE.DoubleSide;

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);
		
	},

	"Tsumikiri J-Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[49]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[49]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sealed J-Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[50]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[50]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[51]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[51]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Crazy Tune" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[52]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[52]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Twin Chakram" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[53]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[53]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[24]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Wok of Akiko" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[54]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[54]);
		let mdl = modelLoader.getModel();
		
		console.log(mdl.material);
		mdl.material[1].side = THREE.DoubleSide;

		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Lavis Blade" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[55]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[55]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Dagger" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[56]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[56]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Maddam Pink" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[57]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[57]);
		let mdl = modelLoader.getModel();
	
		mdl.material[0].side = THREE.DoubleSide;

		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Maddam Purple" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[58]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[58]);
		let mdl = modelLoader.getModel();
		
		mdl.material[0].side = THREE.DoubleSide;
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Imperial Pick" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[59]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[59]);
		let mdl = modelLoader.getModel();
	
		console.log(mdl.material);
		
		mdl.material[2].blending = 2;

		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Berdysh" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[60]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[60]);
		let mdl = modelLoader.getModel();
	
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Partisan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[61]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[61]);
		let mdl = modelLoader.getModel();
	
		console.log(mdl.material);
		mdl.material[2].blending = 2;
		mdl.material[3].blending = 2;

		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_dag", tex);
		modelLoader.parse(mdlList[54]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Flight Cutter" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[62]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[62]);
		let mdl = modelLoader.getModel();

		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[62]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Flight Fan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[63]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[63]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[63]);
		let bullet = modelLoader.getModel();
		bullet.material[0].side = THREE.DoubleSide;

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Red Slicer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[64]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[64]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[64]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Handgun Guld" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[65]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[65]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[65]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Handgun Milla" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[66]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[66]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[65]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Handgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[67]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[67]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[65]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Frozen Shooter" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[68]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[68]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[68]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Anti-Android Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[69]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[69]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[69]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Rocket Punch" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[70]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[70]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet0", tex);
		modelLoader.parse(mdlList[70]);
		let bullet0 = modelLoader.getModel();

		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet1", tex);
		modelLoader.parse(mdlList[70]);
		let bullet1 = modelLoader.getModel();

		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet2", tex);
		modelLoader.parse(mdlList[70]);
		let bullet2 = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet0, bullet1, bullet2], tex);

	},

	"Samba Maracas" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[71]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[71]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[69]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Twin Psychogun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[72]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[72]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[72]);
		let bullet = modelLoader.getModel();
		bullet.material[0].blending = 2;
		bullet.material[0].side = THREE.DoubleSide;

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Drill Launcher" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[73]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[73]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[73]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Guld Milla" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[74]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[74]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[74]);
		let bullet = modelLoader.getModel();

		bullet.material[0].blending = 2;
		bullet.material[0].side = THREE.DoubleSide;
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_gun", tex);
		modelLoader.parse(mdlList[74]);
		let gun = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet, gun], tex);

	},

	"Red Mechguns" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[75]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[75]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[75]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Belra Cannon" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[76]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[76]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[76]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Panzer Faust" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[77]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[77]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[77]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Summit Moon" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[78]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[78]);
		let mdl = modelLoader.getModel();
		
		mdl.material[1].blending = 2;
		mdl.material[1].side = THREE.DoubleSide;

		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[78]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Windmill" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[79]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[79]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[79]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);
	
	},

	"Evil Curst" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[80]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[80]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Flower Cane" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[81]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[81]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Hildebear" : async function() {

	
		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[82]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[82]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Hildeblue" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[83]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[83]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);


	},

	"Rabbit Wand" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[84]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[84]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Plantain Leaf" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[85]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[85]);
		let mdl = modelLoader.getModel();
		mdl.material[0].side = THREE.DoubleSide;

		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Demonic Fork" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[86]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[86]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Striker of Chao" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[87]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[87]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Broom" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[88]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[88]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Prophets of Motav" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[89]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[89]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"The Sigh of a God" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[90]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[90]);
		let mdl = modelLoader.getModel();
		
		console.log(mdl);

		let debugMat = new THREE.MeshNormalMaterial();
		let debug = new THREE.Mesh(mdl.geometry, debugMat);
		debug.name = this + "_debug";

		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [debug], tex);

	},

	"Twinkle Star" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[91]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[91]);
		let mdl = modelLoader.getModel();
		
		/*
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[80]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Plantain Fan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[92]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[92]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[92]);
		let bullet = modelLoader.getModel();
		bullet.material[0].blending = 2;
		bullet.material[1].blending = 2;

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Twin Blaze" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[93]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[93]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[92]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Marina" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[94]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[94]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[92]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Dragon Claw" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[95]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[95]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[92]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Panther Claw" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[96]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[96]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[96]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Dagger" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[97]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[97]);
		let mdl = modelLoader.getModel();
		
		mdl.material[1].blending = 2;

		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[92]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Plantain Huge Fan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[98]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[98]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[98]);
		let bullet = modelLoader.getModel();

		bullet.material[0].blending = 2;
		bullet.material[1].blending = 2;

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Chameleon Scythe" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[99]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[99]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[98]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Yasminkov 3000r" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[100]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[100]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Ano Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[101]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[101]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Baranz Launcher" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[102]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[102]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[102]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Branch of Paku Paku" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[103]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[103]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Heart of Poumn" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[104]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[104]);
		let mdl = modelLoader.getModel();
		
		console.log(mdl.material);
		
		mdl.material[0].side = THREE.DoubleSide;

		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[104]);
		let bullet = modelLoader.getModel();
		bullet.material[0].side = THREE.DoubleSide;		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Yasminkov 2000h" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[105]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[105]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[105]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Yasminkov 7000v" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[106]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[106]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[106]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Yasminkov 9000m" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[107]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[107]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[107]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Maser Beam" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[108]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[108]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Game Magazine" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[109]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[109]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Flower Bouquet" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[110]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[110]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Saber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[111]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[111]);
		let mdl = modelLoader.getModel();
	
		mdl.material[1].side = THREE.DoubleSide;
		mdl.material[1].blending = 2;
		mdl.material[2].side = THREE.DoubleSide;
		mdl.material[2].blending = 2;

		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[112]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[112]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Blade" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[113]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[113]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Partisan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[114]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[114]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		mdl.material[2].blending = 2;
		mdl.material[3].blending = 2;

		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Slicer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[115]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[115]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[115]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"S-Rank Gun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[116]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[116]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[117]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[117]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Mechgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[118]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[118]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Shot" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[119]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[119]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Cane" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[120]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[120]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Rod" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[121]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[121]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Wand" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[122]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[122]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Twin" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[123]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[123]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Claw" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[124]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[124]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Bazooka" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[125]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[125]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[125]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"S-Rank Needle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[126]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[126]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[126]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);
	
	},

	"S-Rank Scythe" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[127]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[127]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Hammer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[128]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[128]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},


	"S-Rank Moon" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[129]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[129]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Psychogun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[130]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[130]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[100]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Punch" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[131]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[131]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		let meshlist = [];

		for(let i = 0; i < 3; i++) {
			console.log("got it fam!");
			modelLoader = new NinjaModel(this + "_bullet" + i, tex);
			modelLoader.parse(mdlList[131]);
			meshlist.push(modelLoader.getModel());
		}
		

		NinjaPlugin.API.setWeapon(this, mdl, meshlist, tex);

	},

	"S-Rank Windmill" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[132]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[132]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"S-Rank Harisen" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[133]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[133]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank Katana" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[134]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[134]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"S-Rank J-Cutter" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[135]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[135]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[135]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Debug Saber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[136]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[136]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Sword" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[138]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[138]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Dagger" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[139]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[139]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Partisan" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[140]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[140]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Slicer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[141]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[141]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[141]);
		let bullet = modelLoader.getModel();
		

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Debug Handgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[142]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[142]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Rifle" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[143]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[143]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Mechgun" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[144]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[144]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Shot" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[145]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[145]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Cane" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[146]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[146]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Rod" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[147]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[147]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Wand" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[148]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[148]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);
	

	},

	"Debug Claw" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[149]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[149]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug DoubleSaber" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[150]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[150]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Mag" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[151]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[151]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Varuna" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[152]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[152]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Mitra" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[153]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[153]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Surya" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[154]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[154]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Vayu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[155]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[155]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Varaha" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[156]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[156]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kama" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[157]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[157]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ushasu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[158]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[158]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Apsaras" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[159]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[159]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kumara" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[160]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[160]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kaitabha" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[161]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[161]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Tapas" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[162]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[162]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Bhirava" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[163]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[163]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kalki" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[164]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[164]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Rudra" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[165]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[165]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Marutah" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[166]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[166]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Yaksa" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[167]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[167]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sita" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[168]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[168]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Garuda" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[169]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[169]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[169]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Nandin" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[170]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[170]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ashvinau" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[171]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[171]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ribhava" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[172]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[172]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Soma" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[173]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[173]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ila" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[174]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[174]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Durga" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[175]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[175]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Vritra" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[176]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[176]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Namuci" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[177]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[177]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sumba" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[178]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[178]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Naga" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[179]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[179]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Pitri" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[180]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[180]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kabanda" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[181]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[181]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Ravana" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[182]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[182]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Marica" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[183]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[183]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Soniti" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[184]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[184]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Pretra" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[185]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[185]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Andhaka" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[186]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[186]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Bana" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[187]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[187]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Naraka" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[188]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[188]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Madhu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[189]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[189]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Churel" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[190]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[190]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Robo Chao" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[191]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[191]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Opa Opa" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[192]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[192]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Pian" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[193]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[193]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Chao" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[194]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[194]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Chu Chu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[195]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[195]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kapu Kapu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[196]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[196]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Angel Wings" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[197]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[197]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[197]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Devil Wings" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[198]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[198]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[198]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Elnoa" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[199]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[199]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[199]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Mark III" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[200]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[200]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Master System" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[201]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[201]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Genesis" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[202]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[202]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Sega Saturn" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[203]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[203]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Dreamcast" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[204]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[204]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Hamburger" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[205]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[205]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Panzer Tail" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[206]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[206]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Devil Tail" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[207]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[207]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Debug Mag" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[208]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[208]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Red Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[209]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[209]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Tripolic Shield" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[210]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[210]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Standstill Shield" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[211]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[211]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Safety Heart" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[212]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[212]);
		let mdl = modelLoader.getModel();
	
		mdl.material[0].side = THREE.DoubleSide;

		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Kasami Bracer" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[213]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[213]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Gods Shield Suzaku" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[214]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[214]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Gods Shield Genbu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[215]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[214]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Gods Shield Byakko" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[216]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[214]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Gods Shield Seiryu" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[217]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[214]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Hunter Shield" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[218]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[218]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Rico Glasses" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[219]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[219]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);
	
	},

	"Rico Earings" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[220]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[220]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Blue Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[221]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[221]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Yellow Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[222]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[222]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[132]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Secure Feet" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[223]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[223]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[223]);
		let bullet = modelLoader.getModel();

		NinjaPlugin.API.setWeapon(this, mdl, [bullet], tex);

	},

	"Purple Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[224]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[224]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[223]);
		let bullet = modelLoader.getModel();
		*/

		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Green Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[225]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[225]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[223]);
		let bullet = modelLoader.getModel();
		*/
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"Black Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[226]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[226]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[223]);
		let bullet = modelLoader.getModel();
		*/
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	},

	"White Ring" : async function() {

		let ark_tex = await NinjaFile.API.load("itemtexture.afs");
		let ark_mdl = await NinjaFile.API.load("itemmodel.afs");

		let texList = NinjaFile.API.afs(ark_tex);
		let mdlList = NinjaFile.API.afs(ark_mdl);

		let tex = NinjaTexture.API.parse(texList[227]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(mdlList[227]);
		let mdl = modelLoader.getModel();
		
		// Get Bullet
		/*
		console.log("got it fam!");
		modelLoader = new NinjaModel(this + "_bullet", tex);
		modelLoader.parse(mdlList[223]);
		let bullet = modelLoader.getModel();
		*/
		NinjaPlugin.API.setWeapon(this, mdl, [], tex);

	}

};
