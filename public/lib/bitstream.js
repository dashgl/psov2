/**
  
  Bit Stream
  Copyright (C) 2018, 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/

function BitStream(name, data, bigEndian) {
	
	this.name = name;
	this.data = new DataView(data);
	this.littleEndian = bigEndian ? false : true;
	this.ofs = 0;
	this.view = this.data;
	this.store = {};
	this.length = data.byteLength;

}

BitStream.bitflag = function(uint, bitPosition) {

	return (uint & (1 << bitPosition)) ? true : false;

}

BitStream.bitmask = function(uint, bitsPositions) {

	let mask = 0;
	for (let i = 0; i < bitsPositions.length; i++) {
		mask |= uint & (1 << bitsPositions[i]);
	}
	return mask >> bitsPositions[0];

}

BitStream.prototype = {

	constructor : BitStream,

	setLittleEndian : function(endian) {

		this.littleEndian = true;

	},

	setBigEndian : function() {

		this.littleEndian = false;

	},

	seekCur : function(whence) {

		this.ofs += whence;

	},

	seekSet : function(whence) {
		
		this.ofs = whence;

	},
	
	seekEnd : function(whence) {

		this.ofs = this.data.byteLength + whence;

	},

	tell : function() {
		
		return this.ofs;

	},

	tellf : function() {
		
		var str = this.ofs.toString(16);
		if(str.length % 2) {
			str = "0" + str;
		}
		return "0x" + str;

	},

	len : function() {

		return this.view.byteLength;

	},

	setOfs : function(len) {

		if(this.view !== this.data) {
			throw new Error("Cannot subdivide once range set");
		}

		let buffer = this.data.buffer;
		this.view = new DataView(buffer, this.ofs, len);
		this.ofs = 0;

	},

	storeOfs : function(key) {
	
		key = key.toString();
		this.store[key] = this.ofs;

	},

	restoreOfs : function(key) {
		
		key = key.toString();
		if(!this.store[key]) {
			throw new Error("Stored offset not in store");
		}
		this.ofs = this.store[key];

	},

	clearOfs : function() {
		
		let offset = this.view.byteOffset;
		let length = this.view.byteLength;
		this.ofs = offset + length;
		this.view = this.data;
		this.store = {};

	},

	readByte : function() {

		let byte = this.view.getUint8(this.ofs);
		this.ofs += 1;
		return byte;

	},

	readShort : function() {

		let short = this.view.getInt16(this.ofs, this.littleEndian);
		this.ofs += 2;
		return short;

	},

	readUShort : function() {

		let ushort = this.view.getUint16(this.ofs, this.littleEndian);
		this.ofs += 2;
		return ushort;

	},

	readInt : function() {

		let int = this.view.getInt32(this.ofs, this.littleEndian);
		this.ofs += 4;
		return int;

	},

	readUInt : function() {
		
		let uint = this.view.getUint32(this.ofs, this.littleEndian);
		this.ofs += 4;
		return uint;

	},

	readFloat : function() {
		
		let float = this.view.getFloat32(this.ofs, this.littleEndian);
		this.ofs += 4;
		return float;

	},

	readVec3 : function() {

		return {
			x : this.readFloat(), 
			y : this.readFloat(),
			z : this.readFloat()
		};

	},

	readVec4 : function() {

		return {
			x : this.readFloat(), 
			y : this.readFloat(),
			z : this.readFloat(),
			w : this.readFloat()
		};

	},

	readRot3 : function() {
		
		return {
			x : this.readInt() * (2 * Math.PI / 0xFFFF),
			y : this.readInt() * (2 * Math.PI / 0xFFFF),
			z : this.readInt() * (2 * Math.PI / 0xFFFF)
		};

	},

	readShortRot3 : function() {
		
		return {
			x : this.readUShort() * (2 * Math.PI / 0xFFFF),
			y : this.readUShort() * (2 * Math.PI / 0xFFFF),
			z : this.readUShort() * (2 * Math.PI / 0xFFFF)
		};

	},

	readColor : function() {
		
		return {
			b : this.readByte() / 255,
			g : this.readByte() / 255,
			r : this.readByte() / 255,
			a : this.readByte() / 255
		}

	},

	readString : function(len) {
		
		var str = "";

		if(len) {
			
			for(let i = 0; i < len; i++) {
				let char = this.readByte();
				str += String.fromCharCode(char);
			}

		} else {

			while(this.ofs < this.view.byteLength) {
				let char = this.readByte();
				if(char === 0) {
					break;
				}
				str += String.fromCharCode(char);
			}

		}

		return str.replace(/\0/g, "");

	},

	copy : function(len) {

		let data = this.view.buffer.slice(this.ofs, this.ofs + len);
		this.ofs += len;
		return data;

	},

	seekNext : function() {
		
		if(this.ofs === this.view.byteLength) {
			return;
		}

		let byte;

		do {
			
			byte = this.view.getUint8(this.ofs);

			if(byte) {
				break;
			}

			this.ofs += 1;

		} while(this.ofs < this.view.byteLength)

	},

	toBlob : function() {
		
		return new Blob([this.view.buffer],{type:"application/octet-stream"});

	}

};
