/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/

const AssetObjects = {

	"Gun Bullet" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		console.log(bml);
		
		let tex = NinjaTexture.API.parse(bml['gun_bullet.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['gun_bullet.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Weapon Box" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		/*
		let bb = await NinjaFile.API.load('item_bb.bml');
		let ixm = NinjaFile.API.bml(bb);
		console.log(ixm);
		saveAs(ixm['gun_bullet.pvm'].toBlob(), 'gun_bullet.xvm');
		*/

		let pvm = await NinjaFile.API.load('fe_obj_ex_box2.pvm');
		console.log(pvm);
		let tlist = NinjaTexture.API.parse(pvm);

		let tex = NinjaTexture.API.parse(bml['ixm_box01.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['ixm_box01.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Armor Box" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		/*
		console.log(bml);
		let ark = await NinjaFile.API.load('st_logo.prs');
		let data = NinjaFile.API.prs(ark.data.buffer);
		let bs = new BitStream('itemrtt', data);
		let tlist = NinjaTexture.API.parse(bs);
		*/

		let tex = NinjaTexture.API.parse(bml['ixm_box02.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['ixm_box02.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Item Box" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		let tex = NinjaTexture.API.parse(bml['ixm_box03.pvm']);
		console.log(tex);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['ixm_box03.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Rare Box" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		let img = new Image();
		img.src = "dat/cbnc7-otncm.png";
		img.onload = () => {

			console.log(img);

			let canvas = document.createElement("canvas");
			canvas.width = img.width;
			canvas.height = img.height;
			let ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);
			let texture = new THREE.Texture(canvas);
			texture.needsUpdate = true;
			texture.name = "ixm_box04";

			let tex = [ texture ];
			let modelLoader = new NinjaModel(this, tex);
			modelLoader.parse(bml['ixm_box03.nj']);
			let model = modelLoader.getItem();
			console.log(model);

			NinjaPlugin.API.setItem(this, model, [], tex);

		}

		/*
		let loader = new THREE.TextureLoader();
		loader.load("dat/cbnc7-otncm.png", texture => {
			texture.name = "ixm_box04";
			let tex = [ texture ];
			let modelLoader = new NinjaModel(this, tex);
			modelLoader.parse(bml['ixm_box03.nj']);
			let model = modelLoader.getItem();
			model.name = "ixm_box04"
			console.log(model);

			NinjaPlugin.API.setItem(this, model, [], tex);

		});
		*/

	},

	"Meseta" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		console.log(bml);
		
		let tex = NinjaTexture.API.parse(bml['ixm_box04.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['ixm_box04.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Armor Scan" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		console.log(bml);
		
		let tex = NinjaTexture.API.parse(bml['armor_scan.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['armor_scan.nj']);
		modelLoader.addAnimation(bml['armor_scan.njm']);
		let model = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Shockwave" : async function() {

		let file = await NinjaFile.API.load("item.bml");
		let bml = NinjaFile.API.bml(file);
		
		console.log(bml);
		
		let tex = NinjaTexture.API.parse(bml['wxmS01_d_w_bullet.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['wxmS01_d_w_bullet.nj']);
		let model = modelLoader.getItem();
		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Door Switch" : async function() {

		let nj = await NinjaFile.API.load("abeno_fs_obj001_fosuno.bml");
		let pvm = await NinjaFile.API.load("fe_obj_switch.pvm");

		let bml = NinjaFile.API.bml(nj);
		let tex = NinjaTexture.API.parse(pvm);

		console.log(tex);

		let modelLoader = new NinjaModel(this, tex);
		console.log("a");
		modelLoader.parse(bml['abesu_fs_obj001_fosu.nj']);
		modelLoader.parse(bml['abeno_fs_obj001_fosuno.nj'], true);
		modelLoader.parse(bml['abesu_fs_obj001_fotutu.nj'], true);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);


	},

	"Biri Ball" : async function() {

		let file = await NinjaFile.API.load("biri_ball.bml");
		let bml = NinjaFile.API.bml(file);

		console.log(bml);

	},

	"Capsule": async function() {

		let file = await NinjaFile.API.load("bm_fe_obj_o_capsule01.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fe_obj_o_capsule01.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_o_capsule01.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

		console.log(bml);

	},

	"Door 01" : async function() {

		let file = await NinjaFile.API.load("bm_fe_obj_o_door01l.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fs_obj_o_door01l.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_door01l.nj']);
		modelLoader.addAnimation(bml['fs_obj_o_door01l.njm']);
		let model = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Door 04" : async function() {

		let file = await NinjaFile.API.load("bm_fe_obj_o_door04l.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fs_obj_o_door04l.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_door04l.nj']);
		modelLoader.addAnimation(bml['fs_obj_o_door04l.njm']);
		let model = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Cake Sisters" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_cakeya.bml");
		let bml = NinjaFile.API.bml(file);
		console.log(bml);
		let tex = NinjaTexture.API.parse(bml["fs_obj_cakeya.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_cakeya.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Hanger Dock" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_cap_moto.bml");
		let bml = NinjaFile.API.bml(file);
		console.log(bml);
		let tex = NinjaTexture.API.parse(bml["fs_obj_cap_moto.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_cap_moto.nj']);
		let model = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Door Panel" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_do_doa_panel.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fs_obj_do_doa_panel.pvm"]);

		let pvm = await NinjaFile.API.load("fs_obj_do_doa_panelr.pvm");
		let red = NinjaTexture.API.parse(pvm);
		tex.push(red[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_do_doa_panel.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},
	
	/*
	"Research Computer" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_kenkyu_com.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fs_obj_kenkyu_com.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_kenkyu_com.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},
	*/

	"Ruins Door Panel" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_doorpanel.bml");
		let bml = NinjaFile.API.bml(file);
		let tex = NinjaTexture.API.parse(bml["fs_obj_o_doorpanel.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_doorpanel.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Ruins Switch" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_doorswitch01.bml");
		let bml = NinjaFile.API.bml(file);

		console.log(bml);
		let gl_tex = NinjaTexture.API.parse(bml["ds_obj_o_doorswitch01_glass.pvm"]);
		let sw_tex = NinjaTexture.API.parse(bml["fs_obj_o_doorswitch01.pvm"]);

		let modelLoader = new NinjaModel(this, sw_tex);
		modelLoader.parse(bml['fs_obj_o_doorswitch01.nj']);
		let model = modelLoader.getItem();

		let loader = new NinjaModel(this, gl_tex);
		loader.parse(bml['ds_obj_o_doorswitch01_glass.nj']);
		let glass = loader.getItem();

		NinjaPlugin.API.setItem(this, model, [glass], sw_tex);

	},

	"Monument 01" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_monument01.bml");
		let bml = NinjaFile.API.bml(file);

		let tex = NinjaTexture.API.parse(bml["fs_obj_o_monument01.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_monument01.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Monument 02" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_monument02.bml");
		let bml = NinjaFile.API.bml(file);

		let tex = NinjaTexture.API.parse(bml["fs_obj_o_monument02.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_monument02.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Monument 03" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_monument03.bml");
		let bml = NinjaFile.API.bml(file);

		let tex = NinjaTexture.API.parse(bml["fs_obj_o_monument03.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_monument03.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);

	},

	"Ruins Crystal" : async function() {

		let file = await NinjaFile.API.load("bm_fs_obj_o_sekihi01.bml");
		let bml = NinjaFile.API.bml(file);

		let tex = NinjaTexture.API.parse(bml["fs_obj_o_sekihi01.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_sekihi01.nj']);
		let model = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, model, [], tex);
	},

	"Cave 01 Rocks" : async function () {

		let file = await NinjaFile.API.load("bm_o_rock_cave01.bml");
		let bml = NinjaFile.API.bml(file);

		let tex, modelLoader, rock0, rock1, rock2;

		tex = NinjaTexture.API.parse(bml["fe_obj_liwa01.pvm"]);
		modelLoader = new NinjaModel(this + "_0", tex);
		modelLoader.parse(bml['fe_obj_liwa01.nj']);
		rock0 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_miwa01.pvm"]);
		modelLoader = new NinjaModel(this + "_1", tex);
		modelLoader.parse(bml['fe_obj_miwa01.nj']);
		rock1 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_siwa01.pvm"]);
		modelLoader = new NinjaModel(this + "_2", tex);
		modelLoader.parse(bml['fe_obj_siwa01.nj']);
		rock2 = modelLoader.getItem();
		
		NinjaPlugin.API.setItem(this, rock0, [rock1, rock2], tex);

		console.log(bml);

	},

	"Cave 02 Rocks" : async function () {

		let file = await NinjaFile.API.load("bm_o_rock_cave02.bml");
		let bml = NinjaFile.API.bml(file);

		let tex, modelLoader, rock0, rock1, rock2;

		tex = NinjaTexture.API.parse(bml["fe_obj_liwa02.pvm"]);
		modelLoader = new NinjaModel(this + "_0", tex);
		modelLoader.parse(bml['fe_obj_liwa02.nj']);
		rock0 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_miwa02.pvm"]);
		modelLoader = new NinjaModel(this + "_1", tex);
		modelLoader.parse(bml['fe_obj_miwa02.nj']);
		rock1 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_siwa02.pvm"]);
		modelLoader = new NinjaModel(this + "_2", tex);
		modelLoader.parse(bml['fe_obj_siwa02.nj']);
		rock2 = modelLoader.getItem();
		
		NinjaPlugin.API.setItem(this, rock0, [rock1, rock2], tex);

	},

	"Cave 03 Rocks" : async function () {

		let file = await NinjaFile.API.load("bm_o_rock_cave03.bml");
		let bml = NinjaFile.API.bml(file);

		let tex, modelLoader, rock0, rock1, rock2;

		tex = NinjaTexture.API.parse(bml["fe_obj_liwa03.pvm"]);
		modelLoader = new NinjaModel(this + "_0", tex);
		modelLoader.parse(bml['fe_obj_liwa03.nj']);
		rock0 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_miwa03.pvm"]);
		modelLoader = new NinjaModel(this + "_1", tex);
		modelLoader.parse(bml['fe_obj_miwa03.nj']);
		rock1 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_siwa03.pvm"]);
		modelLoader = new NinjaModel(this + "_2", tex);
		modelLoader.parse(bml['fe_obj_siwa03.nj']);
		rock2 = modelLoader.getItem();
		
		NinjaPlugin.API.setItem(this, rock0, [rock1, rock2], tex);

	},

	"Ruins Trap" : async function() {

		let file = await NinjaFile.API.load("bm_o_trap_ancient01.bml");
		let bml = NinjaFile.API.bml(file);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml["fs_obj_o_turigane.pvm"]);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_o_turigane.nj']);
		let model = modelLoader.getItem();

		let meshList = [];
		
		NinjaPlugin.API.setItem(this, model, meshList, tex);

	},

	"DarkFalz Event Tower" : async function() {

		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml['df_event_tower.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['df_event_tower.nj']);
		
		let meshList = [];
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ending Blue Title" : async function() {

		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(bml['ending_blue_oya.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['ending_blue_oya.nj']);
		
		let meshList = [];
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Red Ring" : async function() {

		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
			
		let tex = NinjaTexture.API.parse(bml['rico_ring3_rico_ring.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['rico_ring3_rico_ring.nj']);
		
		modelLoader.addAnimation(bml["rico_ring3_rico_ring.njm"]);

		let meshList = [];
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Falz Face Ground" : async function() {

		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
		
		let tex = NinjaTexture.API.parse(bml['fd_obj813_face.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fd_obj813_face.nj']);

		let meshList = [];
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Falz Flowers" : async function() {

		let ark = await NinjaFile.API.load("darkfalz_dat.bml");
		let bml = NinjaFile.API.bml(ark);
		
		let tex = NinjaTexture.API.parse(bml['fd_obj813_flower01.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fd_obj813_flower01.nj']);

		let meshList = [];
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

    "Red Ring Rico (Falz)" : async function() {

        let ark = await NinjaFile.API.load("darkfalz_dat.bml");
        let bml = NinjaFile.API.bml(ark);

        for(let key in bml) {
            if(key.indexOf(".njm") !== -1 && key.indexOf("rikomiraju") !== -1) {
                console.log("modelLoader.addAnimation(bml['%s']);", key)
            } else if(key.indexOf(".nj") !== -1) {
                //console.log("modelLoader.parse(bml['%s'])", key);
            } else if(key.indexOf(".pvm") !== -1) {
                //console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
            }
        }

        let tex = NinjaTexture.API.parse(bml['df_rikomiraju_body.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['df_rikomiraju_body.nj']);

        modelLoader.addAnimation(bml['df_rikomiraju_body.njm']);

        let mdl = modelLoader.getModel();

        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

    },

    "Dark Falz Ring" : async function() {

        let ark = await NinjaFile.API.load("darkfalz_dat.bml");
        let bml = NinjaFile.API.bml(ark);

        for(let key in bml) {
            if(key.indexOf(".njm") !== -1 && key.indexOf("anzen") !== -1) {
                console.log("modelLoader.addAnimation(bml['%s']);", key)
            } else if(key.indexOf(".nj") !== -1) {
                //console.log("modelLoader.parse(bml['%s'])", key);
            } else if(key.indexOf(".pvm") !== -1) {
                //console.log("let tex = NinjaTexture.API.parse(bml['%s']);", key);
            }
        }

        let tex = NinjaTexture.API.parse(bml['df1_anzen.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['df1_anzen.nj']);


        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

    },

	"Mines Door 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_ki_doa.bml']);

		// console.log(gsl);

        let tex = NinjaTexture.API.parse(bml['fs_obj_ki_doa.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_ki_doa.nj']);
		modelLoader.addAnimation(bml['fs_obj_ki_doa.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Mines Door 02" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_ki_doa2.bml']);

		console.log(gsl);

        let tex = NinjaTexture.API.parse(bml['fs_obj_ki_doa2.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_ki_doa2.nj']);
		modelLoader.addAnimation(bml['fs_obj_ki_doa2.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Tank Light" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_tank_hikari.bml']);

		console.log(gsl);

        let tex = NinjaTexture.API.parse(bml['fe_obj_tank_hikari.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fe_obj_tank_hikari.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Research Computer" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_kenkyu_com.bml']);

		console.log(gsl);

        let tex = NinjaTexture.API.parse(bml['fs_obj_kenkyu_com.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_kenkyu_com.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Mecha Firefly" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_mekatombo.bml']);

        let tex = NinjaTexture.API.parse(bml['fs_obj_mekatombo.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_mekatombo.nj']);
		modelLoader.addAnimation(bml['fs_obj_mekatombo.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Mines Monitor" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_monitor.bml']);
		
        let tex = NinjaTexture.API.parse(bml['fs_obj_monitor.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_monitor.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Explosive Machine" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_explosive_machine.bml']);
		
		console.log(bml);
		// return;

        let tex = NinjaTexture.API.parse(bml['fs_obj_bapipe.pvm']);
        let modelLoader = new NinjaModel("fs_obj_bapipe", tex);
        modelLoader.parse(bml['fs_obj_bapipe.nj']);
        let mdl = modelLoader.getModel();

        let meshList = [];

		delete bml["fs_obj_bapipe.nj"];
		delete bml["fs_obj_bapipe.pvm"];

		const files = {};
		for(let key in bml) {
			
			let parts = key.split(".");
			let base = parts[0];
			let ext = parts[1];

			if(!files[base]) {
				files[base] = { "nj" : "", "pvm" : "" };
			}
			
			files[base][ext] = key;

		}

		for(let key in files) {

			tex = NinjaTexture.API.parse(bml[files[key].pvm]);
			modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(bml[files[key].nj]);
	        meshList.push(modelLoader.getModel());

		}


        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Light Machine" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_light_machine01.bml']);
		
		console.log(bml);

        let tex = NinjaTexture.API.parse(bml['fs_obj_light.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_light.nj']);
        let mdl = modelLoader.getModel();
        let meshList = [];

        let ptex = NinjaTexture.API.parse(bml['fs_obj_toudai.pvm']);
        modelLoader = new NinjaModel("light_base", ptex);
        modelLoader.parse(bml['fs_obj_toudai.nj']);
        let base = modelLoader.getModel();
		base.add(mdl);

        NinjaPlugin.API.setItem(this, base, meshList, tex);

	},

	"Mines Console 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fe_obj_tanmatu02.bml']);
		
        let tex = NinjaTexture.API.parse(gsl['fe_obj_computer.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_tanmatu01.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Mines Console 02" : async function() {

		let ark = await NinjaFile.API.load("gsl_machine02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fe_obj_tanmatu02.bml']);
	
		console.log(bml);

        let tex = NinjaTexture.API.parse(gsl['fe_obj_computer.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fe_obj_tanmatu02.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Cargo Hanger" :  async function () {

		let ark = await NinjaFile.API.load("gsl_machine01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_fs_obj_hanger_moto.bml"]);

		let tex = NinjaTexture.API.parse(bml['fs_obj_hanger_moto.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_hanger_moto.nj']);

		let mdl = modelLoader.getModel();
		let meshList = [];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Teleport" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fs_obj_warp.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_warp.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_warp.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fs_obj_warp_beam.nj"]);
		beamLoader.addAnimation(bml["fs_obj_warp_beam.njm"]);
		let beam = beamLoader.getModel();

		mdl.add(beam);
		console.log(bml);

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Boss Teleport" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fs_obj_warp.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_warp.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_warp_dai.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fs_obj_warp_dai_beam.nj"]);
		beamLoader.addAnimation(bml["fs_obj_warp_dai_beam.njm"]);
		let beam = beamLoader.getModel();

		mdl.add(beam);
		console.log(bml);

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Warp" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fs_obj_warp.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_warp.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_warp_n.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fs_obj_warp_n_beam.nj"]);
		beamLoader.addAnimation(bml["fs_obj_warp_n_beam.njm"]);
		let beam = beamLoader.getModel();

		mdl.add(beam);
		console.log(bml);

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Laser Fence 4M" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_lazer2_4m.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_lazer2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_lazer2_4m_moto.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fe_obj_lazer2_4m.nj"]);
		beamLoader.addAnimation(bml["fe_obj_lazer2_4m.njm"]);
		let beam = beamLoader.getModel();

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

		console.log(bml);

	},

	"Laser Fence 6M" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_lazer2_4m.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_lazer2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_lazer2_6m_moto.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fe_obj_lazer2_6m.nj"]);
		beamLoader.addAnimation(bml["fe_obj_lazer2_6m.njm"]);
		let beam = beamLoader.getModel();

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

		console.log(bml);

	},

	"Square Laser Fence 4M" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_lazer4_4m.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_lazer2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_lazer4_4m_moto.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fe_obj_lazer4_4m.nj"]);
		beamLoader.addAnimation(bml["fe_obj_lazer4_4m.njm"]);
		let beam = beamLoader.getModel();

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

		console.log(bml);

	},

	"Square Laser Fence 6M" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_lazer4_4m.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_lazer2.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_lazer4_6m_moto.nj']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["fe_obj_lazer4_6m.nj"]);
		beamLoader.addAnimation(bml["fe_obj_lazer4_6m.njm"]);
		let beam = beamLoader.getModel();

		let meshList = [ beam ];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

		console.log(bml);

	},

	"Forest Door" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_doa_kanban.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_door.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['taiki_fe_obj_doa_oya.nj']);
		modelLoader.addAnimation(bml['fe_obj_doa_oya.njm']);
		let mdl = modelLoader.getModel();

		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml["taiki_fe_obj_doa_laz_oya.nj"]);
		beamLoader.addAnimation(bml["taiki_fe_obj_doa_laz_oya.njm"]);
		let beam = beamLoader.getModel();
		
		let meshList = [ beam ];
		for(let key in bml) {

			switch(key) {
			case 'taiki_fe_obj_doa_oya.nj':
			case 'fe_obj_doa_oya.njm':
			case 'taiki_fe_obj_doa_laz_oya.nj':
			case 'taiki_fe_obj_doa_laz_oya.njm':
				continue;
				break;
			}
			
			console.log(key);

			let loader = new NinjaModel(key.split(".").shift(), tex);
			loader.addAnimation(bml[key]);
			meshList.push(loader.getModel());
		}

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Forest Bridge" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_hashi.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_hashi.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_hashi.nj']);
		let mdl = modelLoader.getModel();
		
		let meshList = [];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Forest Sensor" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_fs_obj_sensor.bml"]);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_sensor.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_sensor_r.pvm']);
		tex.push(red[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_sensor.nj']);
		let mdl = modelLoader.getModel();
		
		let meshList = [];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);


	},

	"Sunrays" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let tex = NinjaTexture.API.parse(gsl['fe_obj001_komo.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(gsl['fe_obj001_komo.nj']);
		let mdl = modelLoader.getModel();
		
		let meshList = [];
		NinjaPlugin.API.setItem(this, mdl, meshList, tex);
		
	},

	"Normal Box" : async function() {


		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_hako01_hahen02.bml"]);
	
		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_hako01_n.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_hako01.nj']);
		let mdl = modelLoader.getModel();

		let meshList = [];

		for(let key in bml) {
			
			if(key === "fs_obj_hako01.nj") {
				continue;
			}

			let loader = new NinjaModel(this, tex);
			loader.parse(bml[key]);
			meshList.push(loader.getModel());

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"High Level Box" : async function() {


		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_con_hahen1.bml"]);
	
		let tex = NinjaTexture.API.parse(gsl['obj_abecon.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_con_body.nj']);
		let mdl = modelLoader.getModel();

		let meshList = [];
		
		for(let key in bml) {
			
			if(key === "fs_obj_con_body.nj") {
				continue;
			}

			let loader = new NinjaModel(this, tex);
			loader.parse(bml[key]);
			meshList.push(loader.getModel());

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Laser Switch" : async function() {


		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_switch_laz.bml"]);

		console.log(bml);
	
		let tex = NinjaTexture.API.parse(gsl['fe_obj_switch_laz.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_switch_laz_moto.nj']);
		modelLoader.addAnimation(bml['fs_obj_switch_laz_moto.njm']);
		let mdl = modelLoader.getModel();
		
		let meshList = [];
		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml['fe_obj_switch_laz.nj']);
		beamLoader.addAnimation(bml['fe_obj_switch_laz.njm']);
		meshList.push(beamLoader.getModel());

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Laser Door" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["bm_fs_obj_lazerguard.bml"]);

		let moto = NinjaTexture.API.parse(gsl['fe_obj_laz_guard_moto.pvm']);
		let pvm = NinjaTexture.API.parse(gsl['fs_obj_laz_guard.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_laz_guard_r.pvm']);
		pvm.push(red[0]);

		let meshList = [];

		let modelLoader = new NinjaModel(this, moto);
		modelLoader.parse(bml['fe_obj_laz_guard_moto.nj']);
		let mdl = modelLoader.getModel();
		
		let beamLoader = new NinjaModel(this + "_beam", pvm);
		beamLoader.parse(bml['fs_obj_laz_guard.nj']);
		beamLoader.addAnimation(bml['fs_obj_laz_guard.njm']);
		meshList.push(beamLoader.getModel());

		NinjaPlugin.API.setItem(this, mdl, meshList, pvm);

	},


	"General Switch" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let body = NinjaFile.API.bml(gsl["fs_obj_hanyousu_body1.bml"]);
		let light = NinjaFile.API.bml(gsl['fs_obj_hanyousu_light.bml']);
		let bodyPvm = NinjaTexture.API.parse(body['fs_obj_hanyousu_body1.pvm']);
		let lightPvm = NinjaTexture.API.parse(gsl['fs_obj_hanyousu_light.pvm']);

		let tex = [];
		for(pvr in bodyPvm) {
			tex.push(pvr);
		}
		for(pvr in lightPvm) {
			tex.push(pvr);
		}

		let modelLoader = new NinjaModel(this, bodyPvm);
		modelLoader.parse(body['fs_obj_hanyousu_body1.nj']);
		let mdl = modelLoader.getModel();
		let meshList = [];

		let beamLoader = new NinjaModel(this + "_beam", lightPvm);
		beamLoader.parse(light['fs_obj_hanyousu_light.nj']);
		beamLoader.addAnimation(light['fs_obj_hanyousu_light.njm']);
		meshList.push(beamLoader.getModel());

		console.log(light);
		NinjaPlugin.API.setItem(this, mdl, meshList, lightPvm);

	},

	"Heal Ring" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_kaifuku_moto.bml']);

		console.log(bml);

        let tex = NinjaTexture.API.parse(gsl['fe_obj_kaifuku_moto.pvm']);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fe_obj_kaifuku_moto.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Mine" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_mine.bml']);
	
		let nodes = {};
		for(let key in bml) {

			let parts = key.split(".");
			let base = parts[0];
			let ext = parts[1];
		
			nodes[base] = nodes[base] || {};
			nodes[base][ext] = bml[key];

		}
		
		let pvm = [];
        let meshList = [];
		for(let key in nodes) {

        	let tex = NinjaTexture.API.parse(nodes[key].pvm);
			tex.forEach(pvr => {
				pvm.push(pvr);
			});

        	let modelLoader = new NinjaModel(key, tex);
        	modelLoader.parse(nodes[key].nj);
       		let mdl = modelLoader.getModel();
			meshList.push(mdl);

		}
        
		NinjaPlugin.API.setItem(this, meshList[0], meshList, pvm);
	},

	"Forest Door Panel" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_ki_doa_panel.bml']);

		console.log(bml);

        let tex = NinjaTexture.API.parse(gsl['fs_obj_ki_doa_panel.pvm']);
        let red = NinjaTexture.API.parse(gsl['fs_obj_ki_doa_panel_r.pvm']);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_ki_doa_panel.nj']);
        let mdl = modelLoader.getModel();
        let meshList = [];

        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Caves Door 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_do_doa01.bml']);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa01.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_do_doa01r.pvm']);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa01.nj']);
		modelLoader.addAnimation(bml['fs_obj_do_doa01.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

		console.log(bml);

	},

	"Caves Door 02" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_do_doa02.bml']);

		console.log(bml);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa02.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_do_doa02r.pvm']);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa02.nj']);
		modelLoader.addAnimation(bml['fs_obj_do_doa02.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);


	},

	"Caves Door 03" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_do_doa03.bml']);

		console.log(bml);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa03.pvm']);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa03.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Caves Crusher" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fe_obj_turiten_moto.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['fe_obj_turiten_moto.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fe_obj_turiten_moto.nj']);
		modelLoader.addAnimation(bml['fe_obj_turiten_moto.njm']);
		modelLoader.addAnimation(bml['fe_obj_turiten_moto_taiki.njm']);
        let mdl = modelLoader.getModel();

        let signal = NinjaTexture.API.parse(gsl['fs_obj_turiten_singo.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_turiten_singo_r.pvm']);

		for(let pvr in signal) {
			tex.push(pvr);
		}

		tex.push(red[0]);

        let meshList = [];
		let loader = new NinjaModel(this + "_signal", signal);
		loader.parse(bml['fs_obj_turiten_singo.nj']);
		meshList.push(loader.getModel());

		modelLoader = new NinjaModel(this + "_black", tex);
		modelLoader.parse(bml['fe_obj_turiten_kuro.nj']);
		meshList.push(modelLoader.getModel());


        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Caves 01 Boulder" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_rb_liwa.bml']);
	
		let nodes = {};
		for(let key in bml) {

			let parts = key.split(".");
			let base = parts[0];
			let ext = parts[1];
		
			nodes[base] = nodes[base] || {};
			nodes[base][ext] = bml[key];

		}
		
		let pvm = [];
        let meshList = [];
		for(let key in nodes) {

        	let tex = NinjaTexture.API.parse(nodes[key].pvm);
			tex.forEach(pvr => {
				pvm.push(pvr);
			});

        	let modelLoader = new NinjaModel(key, tex);
        	modelLoader.parse(nodes[key].nj);
       		let mdl = modelLoader.getModel();
			meshList.push(mdl);

		}
        
		NinjaPlugin.API.setItem(this, meshList[0], meshList, pvm);

		console.log(bml);

	},

	"Caves 01 Bind" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_bind.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['fs_obj_bind_laz_moto.pvm']);
		let modelLoader = new NinjaModel(this + "_laser", tex);
		modelLoader.parse(bml['fs_obj_bind_laz_moto.nj']);
		modelLoader.addAnimation(bml['fs_obj_bind_laz_moto.njm']);
        let mdl = modelLoader.getModel();

		let meshList = [];

		let pvm = NinjaTexture.API.parse(bml['fe_obj_bind_moto.pvm']);
		let loader = new NinjaModel(this, pvm);
		loader.parse(bml['fe_obj_bind_moto.nj']);
		meshList.push(loader.getModel());

		for(let pvr in pvm) {
			tex.push(pvr);
		}

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Caves Sign 1" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_dokanban01.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_dokanban01.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_dokanban01.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves Sign 2" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_dokanban02.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_dokanban02.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_dokanban02.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves Sign 3" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_dokanban03.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_dokanban03.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_dokanban03.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves Aircon 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_aircon01.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_aircon01.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_aircon01.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves Aircon 02" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_aircon02.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_aircon02.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fe_obj_aircon02.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves Spinning Floor Light" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_yukakaiten01.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_yukakaiten01.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_yukakaiten01.nj']);
		modelLoader.addAnimation(bml['fs_obj_yukakaiten01.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Rainbow" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['abeniji_fe_obj001_niji.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['abeniji_fe_obj001_niji.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["abeniji_fe_obj001_niji.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Jellyfish" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_kurage.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_kurage.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_kurage.nj"]);
		modelLoader.addAnimation(bml["fs_obj_kurage.njm"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Dragonfly" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_tombo.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(bml['fs_obj_tombo.pvm']);
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_tombo.nj"]);
		modelLoader.addAnimation(bml["fs_obj_tombo.njm"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Caves 02 Boulder" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave02.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_rb2_liwa.bml']);

		console.log(bml);

		let texList = []
		let meshList = [];
		
		let queue = {};

		for(let key in bml) {

			let parts = key.split(".");

			if(!queue[parts[0]]) {
				queue[parts[0]] = {};
			}

			queue[parts[0]][parts[1]] = bml[key];

		}
		
		let mdl;
		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);
       		let model = modelLoader.getModel();

			if(!mdl) {
				mdl = model;
			} else {
				meshList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, texList);

	},

	"Caves 03 Boulder" : async function() {

		let ark = await NinjaFile.API.load("gsl_cave03.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_rb3_liwa.bml']);

		console.log(bml);

		let texList = []
		let meshList = [];
		
		let queue = {};

		for(let key in bml) {

			let parts = key.split(".");

			if(!queue[parts[0]]) {
				queue[parts[0]] = {};
			}

			queue[parts[0]][parts[1]] = bml[key];

		}
		
		let mdl;
		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);
       		let model = modelLoader.getModel();

			if(!mdl) {
				mdl = model;
			} else {
				meshList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, texList);

	},

	"Ruins Sensor 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_o_sensor01.bml']);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_sensor01.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_o_sensor01r.pvm']);

		red.forEach(t => {
			tex.push(t);
		});

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_sensor01.nj"]);
		modelLoader.addAnimation(bml['fs_obj_o_sensor01.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Sensor 02" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_o_sensor01.bml']);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_sensor02.pvm']);
		let red = NinjaTexture.API.parse(gsl['fs_obj_o_sensor02r.pvm']);

		red.forEach(t => {
			tex.push(t);
		});

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_sensor01.nj"]);
		modelLoader.addAnimation(bml['fs_obj_o_sensor01.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Warp" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_warp_ancient.bml']);

		console.log(bml);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");
			queue[parts[0]] = queue[parts[0]] || {};
			queue[parts[0]][parts[1]] = bml[key];

		}

		let mdl;
		let texList = [];
		let mdlList = [];

		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});
		}

		let red = NinjaTexture.API.parse(gsl['de_obj2_swarp_beam_r.pvm']);
		red.forEach(t => {
			texList.push(t);
		});

		NinjaPlugin.API.setItem(this, mdl, mdlList, texList);
	},

	"Ruins Fence Switch" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fd_obj_n_switch.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fd_obj_n_switch.pvm']);
		let b = NinjaTexture.API.parse(gsl['o_key_ancient01b.pvm']);
		let g = NinjaTexture.API.parse(gsl['o_key_ancient01g.pvm']);
		let p = NinjaTexture.API.parse(gsl['o_key_ancient01p.pvm']);

		tex.push(b[1]);
		tex.push(g[1]);
		tex.push(p[1]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fd_obj_n_switch.nj"]);
		modelLoader.addAnimation(bml['fd_obj_n_switch.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Door Panel" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_o_doorpanel.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_doorpanel.pvm']);
		let a = NinjaTexture.API.parse(gsl['fs_obj_o_doorpanelg.pvm']);

		a.forEach(t => { tex.push(t); });

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_doorpanel.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Door 01" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_o_door01l.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fe_obj_o_door01l.pvm']);
		let a = NinjaTexture.API.parse(gsl['fe_obj_o_door01g.pvm']);

		a.forEach(t => { tex.push(t); });

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fe_obj_o_door01l.nj"]);
		modelLoader.addAnimation(bml['fe_obj_o_door01l.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Door 07" :  async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fs_obj_door07_m.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_door07_m.pvm']);
		let a = NinjaTexture.API.parse(gsl['fs_obj_door07_m_01_g.pvm']);

		a.forEach(t => { tex.push(t); });

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_door07_m.nj"]);
		modelLoader.addAnimation(bml['fs_obj_door07_m.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Door 06" : async function () {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fs_obj_o_door06l.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_door06l.pvm']);
		let a = NinjaTexture.API.parse(gsl['fs_obj_o_door06l_r.pvm']);

		a.forEach(t => { tex.push(t); });

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_door06l.nj"]);
		modelLoader.addAnimation(bml['fs_obj_o_door06l.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);


	},

	"Ruins Door 05" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fs_obj_o_door05l.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_door05l.pvm']);
		let a = NinjaTexture.API.parse(gsl['fs_obj_o_door05l_g.pvm']);

		a.forEach(t => { tex.push(t); });

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_door05l.nj"]);
		modelLoader.addAnimation(bml['fs_obj_o_door05l.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);


	},

	"Ruins Fence 4x2" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fd_obj_n_saku_4x2.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fd_obj_n_saku_4x2.pvm']);
		let b = NinjaTexture.API.parse(gsl['o_fence_ancient_b.pvm']);
		let g = NinjaTexture.API.parse(gsl['o_fence_ancient_g.pvm']);
		let p = NinjaTexture.API.parse(gsl['o_fence_ancient_p.pvm']);

		tex.push(b[0]);
		tex.push(g[0]);
		tex.push(p[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fd_obj_n_saku_4x2.nj"]);
		modelLoader.addAnimation(bml['fd_obj_n_saku_4x2.njm']);
        let mdl = modelLoader.getModel();

		mdl.material[3].blending = 2;
		mdl.material[3].side = THREE.DoubleSide;

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Fence 6x2" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fd_obj_n_saku_6x2.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fd_obj_n_saku_6x2.pvm']);
		let b = NinjaTexture.API.parse(gsl['o_fence_ancient_b.pvm']);
		let g = NinjaTexture.API.parse(gsl['o_fence_ancient_g.pvm']);
		let p = NinjaTexture.API.parse(gsl['o_fence_ancient_p.pvm']);

		tex.push(b[0]);
		tex.push(g[0]);
		tex.push(p[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fd_obj_n_saku_6x2.nj"]);
		modelLoader.addAnimation(bml['fd_obj_n_saku_6x2.njm']);
        let mdl = modelLoader.getModel();

		mdl.material[3].blending = 2;
		mdl.material[3].side = THREE.DoubleSide;

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Fence 4x4" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fd_obj_n_saku_4x4.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fd_obj_n_saku_4x4.pvm']);
		let b = NinjaTexture.API.parse(gsl['o_fence_ancient_b.pvm']);
		let g = NinjaTexture.API.parse(gsl['o_fence_ancient_g.pvm']);
		let p = NinjaTexture.API.parse(gsl['o_fence_ancient_p.pvm']);

		tex.push(b[0]);
		tex.push(g[0]);
		tex.push(p[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fd_obj_n_saku_4x4.nj"]);
		modelLoader.addAnimation(bml['fd_obj_n_saku_4x4.njm']);
        let mdl = modelLoader.getModel();

		mdl.material[3].blending = 2;
		mdl.material[3].side = THREE.DoubleSide;

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Fence 6x4" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fd_obj_n_saku_6x4.bml']);

		console.log(bml);

		let tex = NinjaTexture.API.parse(gsl['fd_obj_n_saku_6x4.pvm']);
		let b = NinjaTexture.API.parse(gsl['o_fence_ancient_b.pvm']);
		let g = NinjaTexture.API.parse(gsl['o_fence_ancient_g.pvm']);
		let p = NinjaTexture.API.parse(gsl['o_fence_ancient_p.pvm']);

		tex.push(b[0]);
		tex.push(g[0]);
		tex.push(p[0]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fd_obj_n_saku_6x4.nj"]);
		modelLoader.addAnimation(bml['fd_obj_n_saku_6x4.njm']);
        let mdl = modelLoader.getModel();

		mdl.material[3].blending = 2;
		mdl.material[3].side = THREE.DoubleSide;

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Blob Container" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fe_obj_h_s_konte.bml']);

		let tex = NinjaTexture.API.parse(bml['fe_obj_h_s_konte.pvm']);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fe_obj_h_s_konte.nj"]);
		modelLoader.addAnimation(bml['fe_obj_h_s_konte.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Blob Container 2" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['fe_obj_h_s_konte.bml']);

		let tex = NinjaTexture.API.parse(bml['fe_obj_h_s_konte.pvm']);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fe_obj_h_s_konte2.nj"]);
		modelLoader.addAnimation(bml['fe_obj_h_s_konte2.njm']);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Tiger Trap" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fs_obj_o_torabasami.bml']);

		let tex = NinjaTexture.API.parse(gsl['fs_obj_o_torabasami.pvm']);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["fs_obj_o_torabasami.nj"]);
        let mdl = modelLoader.getModel();

		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Ruins Wreck" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_o_wreck_ancient.bml']);

		console.log(bml);

		let texList = []
		let meshList = [];
		
		let queue = {};

		for(let key in bml) {

			let parts = key.split(".");

			if(!queue[parts[0]]) {
				queue[parts[0]] = {};
			}

			queue[parts[0]][parts[1]] = bml[key];

		}
		
		let mdl;
		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);
       		let model = modelLoader.getModel();

			if(!mdl) {
				mdl = model;
			} else {
				meshList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, texList);

	},

	"Ruins Fragments" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_fe_obj_o_hahen.bml']);

		console.log(bml);

		let texList = []
		let meshList = [];
		
		let queue = {};

		for(let key in bml) {

			let parts = key.split(".");

			if(!queue[parts[0]]) {
				queue[parts[0]] = {};
			}

			queue[parts[0]][parts[1]] = bml[key];

		}
		
		let mdl;
		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);
       		let model = modelLoader.getModel();

			if(!mdl) {
				mdl = model;
			} else {
				meshList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, texList);

	},

	"Ruins Box" : async function() {

		let file = await NinjaFile.API.load("bm_o_container_ancient.bml");
		let bml = NinjaFile.API.bml(file);

		let tex, modelLoader, box0, box1, box2;

		tex = NinjaTexture.API.parse(bml["fe_obj_o_container.pvm"]);
		modelLoader = new NinjaModel(this + "_0", tex);
		modelLoader.parse(bml['fe_obj_o_container.nj']);
		box0 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_o_container_hahen01.pvm"]);
		modelLoader = new NinjaModel(this + "_1", tex);
		modelLoader.parse(bml['fe_obj_o_container_hahen01.nj']);
		box1 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(bml["fe_obj_o_container_hahen02.pvm"]);
		modelLoader = new NinjaModel(this + "_2", tex);
		modelLoader.parse(bml['fe_obj_o_container_hahen02.nj']);
		box2 = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, box0, [box1, box2], tex);

	},

	"Ruins Box Red" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let file = await NinjaFile.API.load("bm_o_container_ancient.bml");
		let bml = NinjaFile.API.bml(file);

		let tex, modelLoader, box0, box1, box2;

		tex = NinjaTexture.API.parse(gsl["fe_obj_o_container_r.pvm"]);
		modelLoader = new NinjaModel(this + "_0", tex);
		modelLoader.parse(bml['fe_obj_o_container.nj']);
		box0 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(gsl["fe_obj_o_container_hahen01r.pvm"]);
		modelLoader = new NinjaModel(this + "_1", tex);
		modelLoader.parse(bml['fe_obj_o_container_hahen01.nj']);
		box1 = modelLoader.getItem();

		tex = NinjaTexture.API.parse(gsl["fe_obj_o_container_hahen02r.pvm"]);
		modelLoader = new NinjaModel(this + "_2", tex);
		modelLoader.parse(bml['fe_obj_o_container_hahen02.nj']);
		box2 = modelLoader.getItem();

		NinjaPlugin.API.setItem(this, box0, [box1, box2], tex);

	},

	"Ruins Warp Room" : async function() {

		let ark = await NinjaFile.API.load("gsl_ancient03.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_obj_warpboss_ancient.bml']);

		/*
		let str = "";
		for(let key in gsl) {
			str += key;
			str += "\n";
		}

		console.log(str);
		*/

		console.log(bml);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");
			queue[parts[0]] = queue[parts[0]] || {};
			queue[parts[0]][parts[1]] = bml[key];

		}

		let mdl;
		let texList = [];
		let mdlList = [];

		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});
		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, texList);

	},

	"City Common" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_obj_city_common.bml']);

		let str = "";
		for(let key in bml) {
			str += key;
			str += "\n";
		}

		console.log(str);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");

			if(parts[0].indexOf("aircar_path") !== -1) {
				parts[0] = parts[0].replace("aircar_path", "fe_obj_o_aircar0");
			}

			queue[parts[0]] = queue[parts[0]] || {};
			queue[parts[0]][parts[1]] = bml[key];

		}

		let tex = NinjaTexture.API.parse(bml['de_obj_bigbeam.pvm']);
		let mdl;
		let mdlList = [];

		for(let key in queue) {

			console.log(key);

			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, tex);

	},

	"Boss Teleport Inactive" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_obj_warpboss.bml']);

		let mdlList = [];
		let tex = NinjaTexture.API.parse(bml['fs_obj_warp_dai_beam02.pvm']);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_warp_dai_beam02.nj']);
		modelLoader.addAnimation(bml['fs_obj_warp_dai_beam02.njm']);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Butterfly" :  async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_butterfly.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_butterfly.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		let njmPtr = bs.readUInt();


		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr);
		modelLoader.rel("NJCM", bs);
		bs.seekSet(njmPtr);
		modelLoader.rel("NMDM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Motorcycle Broken" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_motorcycle.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_motorcycle.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr1 = bs.readUInt();
		let njPtr2 = bs.readUInt();


		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr1);
		modelLoader.rel("NJCM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Motorcycle Fixed" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_motorcycle.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_motorcycle.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr1 = bs.readUInt();
		let njPtr2 = bs.readUInt();


		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr2);
		modelLoader.rel("NJCM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Forest Tank" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_tank.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_tank.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr1 = bs.readUInt();
		let njPtr2 = bs.readUInt();
		let njPtr3 = bs.readUInt();

		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr1);
		modelLoader.rel("NJCM", bs);
		bs.seekSet(njPtr2);
		modelLoader.rel("NJCM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Forest Battery" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_battery.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_battery.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr1 = bs.readUInt();
		let njPtr2 = bs.readUInt();

		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr1);
		modelLoader.rel("NJCM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Forest Rel Container" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['obj_containerido.rel'];
		let tex = NinjaTexture.API.parse(gsl['obj_containerido.pvm']);
		
		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		let njtlPtr = bs.readUInt();
		let njPtr1 = bs.readUInt();
		let njPtr2 = bs.readUInt();

		let modelLoader = new NinjaModel(this, tex);
		bs.seekSet(njPtr1);
		modelLoader.rel("NJCM", bs);
		bs.seekSet(njPtr2);
		modelLoader.rel("NJCM", bs);

       	let mdl = modelLoader.getModel();
		NinjaPlugin.API.setItem(this, mdl, [], tex);

	},

	"Dragon Boss Common" : async function() {

		let ark = await NinjaFile.API.load("bm_obj_boss1_common.bml");
		let pvm = await NinjaFile.API.load("obj_boss1_common.pvm");
		let bml = await NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(pvm);

		let mdl;
		let mdlList = [];

		for(let key in bml) {

			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(bml[key]);

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, tex);
	},

	"Dragon Boss Common Ultimate" : async function() {

		let ark = await NinjaFile.API.load("bm_obj_boss1_common_a.bml");
		let pvm = await NinjaFile.API.load("obj_boss1_common_a.pvm");
		let bml = await NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(pvm);

		let mdl;
		let mdlList = [];

		for(let key in bml) {

			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(bml[key]);

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, tex);
	},

	"De Rol Le Common" :  async function() {

		let ark = await NinjaFile.API.load("bm_obj_boss2_common.bml");
		let pvm = await NinjaFile.API.load("obj_boss2_common.pvm");
		let bml = await NinjaFile.API.bml(ark);

		let tex = NinjaTexture.API.parse(pvm);

		let mdl;
		let mdlList = [];

		for(let key in bml) {

			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(bml[key]);

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, tex);

	},

    "map_aancient01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_aancient01_00s.nj");
        let pvm = await NinjaFile.API.load("map_aancient01_00s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_aancient01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_aancient01_01s.nj");
        let pvm = await NinjaFile.API.load("map_aancient01_01s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_aancient01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_aancient01_02s.nj");
        let pvm = await NinjaFile.API.load("map_aancient01_02s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_aancient01_03s" : async function() {

        let nj = await NinjaFile.API.load("map_aancient01_03s.nj");
        let pvm = await NinjaFile.API.load("map_aancient01_03s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_aancient01_04s" : async function() {

        let nj = await NinjaFile.API.load("map_aancient01_04s.nj");
        let pvm = await NinjaFile.API.load("map_aancient01_04s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_acave01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_acave01_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_acave01_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_acave01_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave02_00s" : async function() {

        let nj = await NinjaFile.API.load("map_acave02_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave02_01s" : async function() {

        let nj = await NinjaFile.API.load("map_acave02_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave02_02s" : async function() {

        let nj = await NinjaFile.API.load("map_acave02_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave03_00s" : async function() {

        let nj = await NinjaFile.API.load("map_acave03_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave03_01s" : async function() {

        let nj = await NinjaFile.API.load("map_acave03_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_acave03_02s" : async function() {

        let nj = await NinjaFile.API.load("map_acave03_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_aforest01s" : async function() {

        let nj = await NinjaFile.API.load("map_aforest01s.nj");
        let pvm = await NinjaFile.API.load("map_aforest01s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_aforest02s" : async function() {

        let nj = await NinjaFile.API.load("map_aforest02s.nj");
        let pvm = await NinjaFile.API.load("map_aforest02s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_amachine01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine01_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_amachine01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine01_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_amachine01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine01_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_amachine02_00s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine02_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_amachine02_01s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine02_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_amachine02_02s" : async function() {

        let nj = await NinjaFile.API.load("map_amachine02_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_ancient01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_ancient01_00s.nj");
        let pvm = await NinjaFile.API.load("map_ancient01_00s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_ancient01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_ancient01_01s.nj");
        let pvm = await NinjaFile.API.load("map_ancient01_01s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_ancient01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_ancient01_02s.nj");
        let pvm = await NinjaFile.API.load("map_ancient01_02s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_ancient01_03s" : async function() {

        let nj = await NinjaFile.API.load("map_ancient01_03s.nj");
        let pvm = await NinjaFile.API.load("map_ancient01_03s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_ancient01_04s" : async function() {

        let nj = await NinjaFile.API.load("map_ancient01_04s.nj");
        let pvm = await NinjaFile.API.load("map_ancient01_04s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_cave01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_cave01_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_cave01_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_cave01_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave02_00s" : async function() {

        let nj = await NinjaFile.API.load("map_cave02_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave02_01s" : async function() {

        let nj = await NinjaFile.API.load("map_cave02_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave02_02s" : async function() {

        let nj = await NinjaFile.API.load("map_cave02_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave03_00s" : async function() {

        let nj = await NinjaFile.API.load("map_cave03_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave03_01s" : async function() {

        let nj = await NinjaFile.API.load("map_cave03_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_cave03_02s" : async function() {

        let nj = await NinjaFile.API.load("map_cave03_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_forest01s" : async function() {

        let nj = await NinjaFile.API.load("map_forest01s.nj");
        let pvm = await NinjaFile.API.load("map_forest01s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_forest02s" : async function() {

        let nj = await NinjaFile.API.load("map_forest02s.nj");
        let pvm = await NinjaFile.API.load("map_forest02s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_machine01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_machine01_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_machine01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_machine01_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_machine01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_machine01_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_machine02_00s" : async function() {

        let nj = await NinjaFile.API.load("map_machine02_00s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_machine02_01s" : async function() {

        let nj = await NinjaFile.API.load("map_machine02_01s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_machine02_02s" : async function() {

        let nj = await NinjaFile.API.load("map_machine02_02s.nj");

        let modelLoader = new NinjaModel(this, []);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], []);

    },

    "map_vs01_00s" : async function() {

        let nj = await NinjaFile.API.load("map_vs01_00s.nj");
        let pvm = await NinjaFile.API.load("map_vs01_00s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_vs01_01s" : async function() {

        let nj = await NinjaFile.API.load("map_vs01_01s.nj");
        let pvm = await NinjaFile.API.load("map_vs01_01s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_vs01_02s" : async function() {

        let nj = await NinjaFile.API.load("map_vs01_02s.nj");
        let pvm = await NinjaFile.API.load("map_vs01_02s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

    "map_vs02_00s" : async function() {

        let nj = await NinjaFile.API.load("map_vs02_00s.nj");
        let pvm = await NinjaFile.API.load("map_vs02_00s.pvm");

        let tex = NinjaTexture.API.parse(pvm);
        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(nj);
        let model = modelLoader.getModel();

        model.scale.x = 0.001;
        model.scale.y = 0.001;
        model.scale.z = 0.001;

        NinjaPlugin.API.setItem(this, model, [], tex);

    },

	"Ultimate Caves Door 01" : async function() {

		let ark = await NinjaFile.API.load('bm_fs_obj_do_doa01_a.bml');
		let bml = NinjaFile.API.bml(ark);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa01.pvm']);
		let pvm = await NinjaFile.API.load('fs_obj_do_doa01r_a.pvm');
		let red = NinjaTexture.API.parse(pvm);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa01.nj']);
		modelLoader.addAnimation(bml['fs_obj_do_doa01.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ultimate Caves Door 02" : async function() {

		let ark = await NinjaFile.API.load('bm_fs_obj_do_doa02_a.bml');
		let bml = NinjaFile.API.bml(ark);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa02.pvm']);
		let pvm = await NinjaFile.API.load('fs_obj_do_doa02r_a.pvm');
		let red = NinjaTexture.API.parse(pvm);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa02.nj']);
		modelLoader.addAnimation(bml['fs_obj_do_doa02.njm']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ultimate Caves Door 03" : async function() {

		let ark = await NinjaFile.API.load('bm_fs_obj_do_doa03_a.bml');
		let bml = NinjaFile.API.bml(ark);

		console.log(bml);

        let tex = NinjaTexture.API.parse(bml['fs_obj_do_doa03.pvm']);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_do_doa03.nj']);

        let mdl = modelLoader.getModel();
        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ultimate Mines Door 01" : async function() {

		let ark = await NinjaFile.API.load('bm_fs_obj_ki_doa_a.bml');
		let pvm = await NinjaFile.API.load('fs_obj_ki_doa_r_a.pvm');
		let bml = NinjaFile.API.bml(ark);

		let red = NinjaTexture.API.parse(pvm);
        let tex = NinjaTexture.API.parse(bml['fs_obj_ki_doa.pvm']);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_ki_doa.nj']);
		modelLoader.addAnimation(bml['fs_obj_ki_doa.njm']);
        let mdl = modelLoader.getModel();

        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ultimate Mines Door 02" : async function() {

		let ark = await NinjaFile.API.load('bm_fs_obj_ki_doa2_a.bml');
		let pvm = await NinjaFile.API.load('fs_obj_ki_doa2_r_a.pvm');
		let bml = NinjaFile.API.bml(ark);

		let red = NinjaTexture.API.parse(pvm);
        let tex = NinjaTexture.API.parse(bml['fs_obj_ki_doa2.pvm']);
		tex.push(red[0]);

        let modelLoader = new NinjaModel(this, tex);
        modelLoader.parse(bml['fs_obj_ki_doa2.nj']);
		modelLoader.addAnimation(bml['fs_obj_ki_doa2.njm']);
        let mdl = modelLoader.getModel();

        let meshList = [];
        NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Laser Switch Ultimate" : async function() {


		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_switch_laz.bml"]);

		let pvm = await NinjaFile.API.load('fe_obj_switch_laz_a.pvm');
		let orig = NinjaTexture.API.parse(gsl['fe_obj_switch_laz.pvm']);
		let tex = NinjaTexture.API.parse(pvm);

		console.log(orig);
		console.log(tex);

		tex = [
			tex[0],
			tex[1],
			tex[2],
			tex[3],
			tex[6],
			tex[7],
			tex[4],
			orig[7]
		];

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_switch_laz_moto.nj']);
		modelLoader.addAnimation(bml['fs_obj_switch_laz_moto.njm']);
		let mdl = modelLoader.getModel();
		
		let meshList = [];
		let beamLoader = new NinjaModel(this + "_beam", tex);
		beamLoader.parse(bml['fe_obj_switch_laz.nj']);
		beamLoader.addAnimation(bml['fe_obj_switch_laz.njm']);
		meshList.push(beamLoader.getModel());

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Text Ring Background" : async function() {

		let ark = await NinjaFile.API.load('ad_f_moyouring.bml');
		let bml = NinjaFile.API.bml(ark);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");
			queue[parts[0]] = queue[parts[0]] || {};
			queue[parts[0]][parts[1]] = bml[key];

		}

		let mdl;
		let texList = [];
		let mdlList = [];

		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});
		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, texList);

	},

	"Ultimate Normal Box" : async function() {


		let ark = await NinjaFile.API.load("gsl_forest01.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["fe_obj_hako01_hahen02.bml"]);
	
		let pvm = await NinjaFile.API.load('fs_obj_hako01_a.pvm');
		let orig = NinjaTexture.API.parse(gsl['fs_obj_hako01_n.pvm']);
		let alt = NinjaTexture.API.parse(pvm);

		alt.shift();
		alt.push(orig.pop());

		let tex = alt;
		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml['fs_obj_hako01.nj']);
		let mdl = modelLoader.getModel();

		let meshList = [];

		for(let key in bml) {
			
			if(key === "fs_obj_hako01.nj") {
				continue;
			}

			let loader = new NinjaModel(this, tex);
			loader.parse(bml[key]);
			meshList.push(loader.getModel());

		}

		NinjaPlugin.API.setItem(this, mdl, meshList, tex);

	},

	"Ex Box 01" : async function() {
		
		let ark = await NinjaFile.API.load('fe_obj_ex_box1.bml');
		let bml = NinjaFile.API.bml(ark);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");
			queue[parts[0]] = queue[parts[0]] || {};
			queue[parts[0]][parts[1]] = bml[key];

		}

		let mdl;
		let texList = [];
		let mdlList = [];

		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});
		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, texList);

	},

	"Ex Box 02" : async function() {
		
		let ark = await NinjaFile.API.load('fe_obj_ex_box1.bml');
		let bml = NinjaFile.API.bml(ark);

		let queue = {};
		for(let key in bml) {

			let parts = key.split(".");
			queue[parts[0]] = queue[parts[0]] || {};

			if(parts[1] !== "pvm") {
				queue[parts[0]][parts[1]] = bml[key];
				continue;
			}

			let url = key.replace("box1", "box2");
			let p = await NinjaFile.API.load(url);
			queue[parts[0]][parts[1]] = p;

		}

		let mdl;
		let texList = [];
		let mdlList = [];

		for(let key in queue) {

			let tex = NinjaTexture.API.parse(queue[key].pvm);
			let modelLoader = new NinjaModel(key, tex);
			modelLoader.parse(queue[key].nj);

			if(queue[key].njm) {
				modelLoader.addAnimation(queue[key].njm);
			}

       		let model = modelLoader.getModel();
			if(!mdl) {
				mdl = model;
			} else {
				mdlList.push(model);
			}

			tex.forEach(t => {
				texList.push(t);
			});
		}

		NinjaPlugin.API.setItem(this, mdl, mdlList, texList);

	}





};
