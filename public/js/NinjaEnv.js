/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


class NinjaEnv {

	constructor(name, tex) {
		
		this.name = name;
		this.bones = [];
		this.texList = tex || [];
		this.matList = [];
		this.vertexStack = [];
		this.animList = [];
		this.vertexAlpha = false;

		this.v = [];

		this.vertices = [];
		this.matIndex = [];
		this.colors = [];
		this.vcolors = [];
		this.uv = [];
		this.skinWeight = [];
		this.skinIndex = [];

	}

	getModel() {

        let geometry = new THREE.BufferGeometry();
        let vertices = new Float32Array(this.vertices);
        let colors = new Float32Array(this.colors);
        let vcolors = new Float32Array(this.vcolors);
        let uv = new Float32Array(this.uv);
		let skinWeight = new Float32Array(this.skinWeight);
		let skinIndex = new Uint16Array(this.skinIndex);

        geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'vcolor', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'uv', new THREE.BufferAttribute( uv, 2 ) );
        geometry.addAttribute( 'skinWeight', new THREE.BufferAttribute( skinWeight, 4 ) );
        geometry.addAttribute( 'skinIndex', new THREE.BufferAttribute( skinIndex, 4 ) );

        geometry.computeVertexNormals();

		//console.log(this.matList);

        for(let i = 0; i < this.matList.length; i++) {

            let mat = new THREE.MeshBasicMaterial({
                skinning : true,
                vertexColors : THREE.VertexColors,
				transparent : true,
				alphaTest : 0.05
            });

            if(this.matList[i] !== -1) {
                mat.map = this.texList[this.matList[i]];
            }

            this.matList[i] = mat;

        }

        let stack = [];

        for(let i = 0; i < this.matIndex.length; i++) {

            let last = stack[stack.length - 1] || {};

            if(this.matIndex[i] === last.materialIndex) {
                last.count += 3;
                continue;
            }

            stack.push({
                start : i*3,
                count : 3,
                materialIndex : this.matIndex[i]
            });

        }

        stack.forEach(m => {
            geometry.addGroup(m.start, m.count, m.materialIndex);
        });

		/*
		for(let i = 0; i < this.bones.length; i++) {
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			this.bones[i].name = "bone_" + num;
		}
		*/

		let mesh = new THREE.SkinnedMesh(geometry, this.matList);
		mesh.name = this.name;
		let armSkeleton = new THREE.Skeleton(this.bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);
		mesh.geometry.animations = this.animList;
		return mesh;

	}

	readBone(parentBone) {

		//  Read bone structure

		let bone = {
			flag : this.bs.readUInt(),
			chunkOfs : this.bs.readUInt(),
			pos : this.bs.readVec3(),
			rot : this.bs.readRot3(),
			scl : this.bs.readVec3(),
			childOfs : this.bs.readUInt(),
			siblingOfs : this.bs.readUInt()
		};

		// Create New Bone for Three.js

		this.bone = new THREE.Bone();
		this.bone.index = this.bones.length;
		let num = this.bones.length.toString();
		while(num.length < 3) {
			num = "0" + num;
		}

		this.bone.name = "bone_" + num;


		this.bones.push(this.bone);

		// Check flags for LightWave 3d Export

		let zxy = BitStream.bitflag(bone.flag, 5);
		if(zxy) {
			console.error("ZXY FLAG SET!!!!");
		}

		// Update bone local transform matrix

		if (!BitStream.bitflag(bone.flag, 2)) {
			this.bone.scale.x = bone.scl.x;
			this.bone.scale.y = bone.scl.y;
			this.bone.scale.z = bone.scl.z;
		}

		if (!BitStream.bitflag(bone.flag, 1)) {

			let xRotMatrix = new THREE.Matrix4();
			xRotMatrix.makeRotationX(bone.rot.x);
			this.bone.applyMatrix(xRotMatrix);

			let yRotMatrix = new THREE.Matrix4();
			yRotMatrix.makeRotationY(bone.rot.y);
			this.bone.applyMatrix(yRotMatrix);

			let zRotMatrix = new THREE.Matrix4();
			zRotMatrix.makeRotationZ(bone.rot.z);
			this.bone.applyMatrix(zRotMatrix);

		}

		if (!BitStream.bitflag(bone.flag, 0)) {
			this.bone.position.x = bone.pos.x;
			this.bone.position.y = bone.pos.y;
			this.bone.position.z = bone.pos.z;
		}

		this.bone.updateMatrix();
		this.bone.updateMatrixWorld();

		// If parent Bone exists, add bone as child

		if(parentBone) {
			parentBone.add(this.bone);
			this.bone.updateMatrix();
			this.bone.updateMatrixWorld();
		}

		// If polygon exists for bone, seek and read

		if(bone.chunkOfs) {
			this.bs.seekSet(bone.chunkOfs);
			let vertexOfs = this.bs.readUInt();
			let stripOfs = this.bs.readUInt();

			if(vertexOfs) {
				this.bs.seekSet(vertexOfs);
				this.readChunk();
			}

			if(stripOfs) {
				this.bs.seekSet(stripOfs);
				this.readChunk();
			}
		}

		// If child bone exists, read and pass in current bone

		if(bone.childOfs) {
			this.bs.seekSet(bone.childOfs);
			this.readBone(this.bone);
		}

		// If sibling bone exists, read and pass in parent bone

		if(bone.siblingOfs) {
			this.bs.seekSet(bone.siblingOfs);
			this.readBone(parentBone);
		}

	}

	readChunk() {

		const NJD_NULLOFF = 0x00;
		const NJD_BITSOFF = 0x01;
		const NJD_TINYOFF = 0x08;
		const NJD_MATOFF = 0x10;
		const NJD_VERTOFF = 0x20;
		const NJD_VOLOFF = 0x38;
		const NJD_STRIPOFF = 0x40;
		const NJD_ENDOFF = 0xFF;

		this.mat = -1;
		let chunk;

		do {

			chunk = {
				head : this.bs.readByte(),
				flag : this.bs.readByte()
			};

			// Invalid Chunk

			if (chunk.head > NJD_STRIPOFF + 11) {
				continue;
			}

			// Strip Chunk

			if(chunk.head >= NJD_STRIPOFF) {
				this.readStripChunk(chunk);
				continue;
			}

			// Volume Chunk

			if (chunk.head >= NJD_VOLOFF) {
				throw new Error("Volume chunk detected");
			}

			// Vertex Chunk

			if (chunk.head >= NJD_VERTOFF) {
				this.readVertexChunk(chunk);
				continue;
			}

			// Material Chunk

			if (chunk.head >= NJD_MATOFF) {
				this.readMaterialChunk(chunk);
				continue;
			}

			// Tiny Chunk

			if (chunk.head >= NJD_TINYOFF) {
				this.readTinyChunk(chunk);
				continue;
			}

			// Bits Chunk

			if(chunk.head >= NJD_BITSOFF) {
				this.readBitsChunk(chunk);
				continue;
			}

			// End

		} while(chunk.head !== NJD_ENDOFF);


	}

	readBitsChunk(chunk) {

		switch (chunk.head) {
		case 1:
			let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
			let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);
			break;
		case 2:
			let mipmapDepth = chunk.flag & 0x0f;
			break;
		case 3:
			let specularCoef = chunk.flag & 0x1f;
			break;
		case 4:
			this.bs.storeOfs(chunk.flag);
			chunk.head = 0xff;
			break;
		case 5:
			this.bs.restoreOfs(chunk.flag);
			break;
		}

	}

	readTinyChunk(chunk) {
		
		let tinyChunk = this.bs.readUShort();
		chunk.textureId = tinyChunk & 0x1FFF;
		this.mat = chunk.textureId;

		let superSample = BitStream.bitflag(tinyChunk, 13);
		let filterMode = BitStream.bitmask(tinyChunk, [14, 15]);

		let clampU = BitStream.bitflag(chunk.head, 4);
		let clampV = BitStream.bitflag(chunk.head, 5);
		let flipU = BitStream.bitflag(chunk.head, 6);
		let flipV = BitStream.bitflag(chunk.head, 7);

	}

	readMaterialChunk(chunk) {

		let r, g, b, a;

		chunk.length = this.bs.readUShort();

		// Alpha Blending Instructions

		let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
		let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);
		let diffuse, specular, ambient, type;

		// Diffuse

		if (BitStream.bitflag(chunk.head, 0)) {
			diffuse = {
				g : this.bs.readByte() / 255,
				b : this.bs.readByte() / 255,
				r : this.bs.readByte() / 255,
				a : this.bs.readByte() / 255,
			}
		}

		// Specular

		if (BitStream.bitflag(chunk.head, 1)) {
			//type = "phong";
			specular = this.bs.readColor();
			specular = null;
		}

		// Ambient

		if (BitStream.bitflag(chunk.head, 2)) {
			//type = type || "lambert";
			ambient = this.bs.readColor();
			ambient = null;
		}

		type = type || "basic";
		
	}

	readVertexChunk(chunk) {

		let r, g, b, a;

		chunk.length = this.bs.readUShort();

		// Read the index offset and the number of index

		let indexOfs = this.bs.readUShort();
		let nbIndex = this.bs.readUShort();

		// Read the vertex list

		for(let i = 0; i < nbIndex; i++) {

			let stackOfs = indexOfs + i;
			let vertex = new THREE.Vector3();

			// Read the position

			let pos = this.bs.readVec3();
			vertex.x = pos.x;
			vertex.y = pos.y;
			vertex.z = pos.z;
			vertex.applyMatrix4(this.bone.matrixWorld);

			// Read vertex normals

			if (chunk.head > 0x28 && chunk.head < 0x30) {
				let norm = this.bs.readVec3();
				let normal = new THREE.Vector3();
				normal.x = norm.x;
				normal.y = norm.y;
				normal.z = norm.z;
				//normal.applyMatrix3(this.bone.normalMatrix);
				vertex.normal = normal;
			}

			// Read vertex color

			if(chunk.head === 0x23 || chunk.head === 0x2a) {
				vertex.color = {
					b : this.bs.readByte() / 255,
					g : this.bs.readByte() / 255,
					r : this.bs.readByte() / 255,
					a : this.bs.readByte() / 255
				};
			}

			// Read Vertex weight

			let skinWeights = new THREE.Vector4(0, 0, 0, 0);
			let skinIndices = new THREE.Vector4(0, 0, 0, 0);

			if(chunk.head !== 0x2c) {

				skinIndices.x = this.bone.index;
				skinWeights.x = 1.0;

			} else {

				// Read weight values

				let ofs = this.bs.readUShort();
				let weight = this.bs.readUShort();

				// Update current stack position

				stackOfs = indexOfs + ofs;

				// Set the vertex weights

				// If a previous vertex exists, get values

				if(this.vertexStack[stackOfs]) {
					let prev = this.vertexStack[stackOfs];
					let keys = ['x', 'y', 'z'];
					keys.forEach(axis => {
						skinWeights[axis] = prev.skinWeight[axis];
						skinIndices[axis] = prev.skinIndice[axis];
					});
				}

				switch(chunk.flag) {
				case 0x80:
					skinIndices.x = this.bone.index
					skinWeights.x = weight / 255;
					break;
				case 0x81:
					skinIndices.y = this.bone.index
					skinWeights.y = weight / 255;
					break;
				case 0x82:
					skinIndices.z = this.bone.index
					skinWeights.z = weight / 255;
					break;
				}

			}

			// If the global index is set, continue

			// Push the vertex to the stack

			vertex.globalIndex = this.v.length;
			vertex.skinWeight = skinWeights;
			vertex.skinIndice = skinIndices;

			this.vertexStack[stackOfs] = vertex;
			this.v.push(vertex);
		}


	}

	readStripChunk(chunk) {
		
		if(this.matList.indexOf(this.mat) === -1) {
			this.matList.push(this.mat);
		}

		let index = this.matList.indexOf(this.mat);
		chunk.length = this.bs.readUShort();

		// Read the number of strips and user offset

		let stripChunk = this.bs.readUShort();
		let nbStrips = stripChunk & 0x3FFF;
		let userOffset = BitStream.bitmask(stripChunk, [14, 15]);
		let doubleSide = BitStream.bitflag(chunk.flag, 4);

		// Read the list of strips

		for(let i = 0; i < nbStrips; i++) {

			// Read the length and direction

			let strip_length = this.bs.readShort();
			let clockwise = strip_length < 0 ? true : false;
			let length = Math.abs(strip_length);
			let strip = new Array(length);

			// Read the strip

			for (let k = 0; k < strip.length; k++) {

				// Read stack position

				let stackOfs = this.bs.readUShort();

				strip[k] = {
					vertex : this.vertexStack[stackOfs]
				};

				// Read face uv values

				switch (chunk.head) {
				case 0x41:
					this.face_texture = true;
					strip[k].uv = new THREE.Vector2(
						this.bs.readShort() / 255,
						1 - this.bs.readShort() / 255
					);
					break;
				case 0x42:
					this.face_texture = true;
					strip[k].uv = new THREE.Vector2(
						this.bs.readShort() / 1023,
						1 - this.bs.readShort() / 1023
					);
					break;
				default:
					strip[k].uv = new THREE.Vector2();
					break;
				}

				// Seek passed user offset

				if (userOffset && k > 1) {
					this.bs.seekCur(userOffset * 2);
				}

			}

			// Convert strips into faces

			for (let k = 0; k < strip.length - 2; k++) {

				let a, b, c;
				let aPos, bPos, cPos;
				let aClr, bClr, cClr;
				let aUv, bUv, cUv;
				let aIdx, bIdx, cIdx;
				let aWgt, bWgt, cWgt;

				if ((clockwise && !(k % 2)) || (!clockwise && k % 2)) {
					a = strip[k + 0];
					b = strip[k + 2];
					c = strip[k + 1];
				} else {
					a = strip[k + 0];
					b = strip[k + 1];
					c = strip[k + 2];
				}

				this.matIndex.push(index);

				// Positions

				aPos = a.vertex;
				bPos = b.vertex;
				cPos = c.vertex;

				this.vertices.push(aPos.x, aPos.y, aPos.z);
				this.vertices.push(bPos.x, bPos.y, bPos.z);
				this.vertices.push(cPos.x, cPos.y, cPos.z);

				// Colors

				aClr = aPos.color || { r:1, g:1, b:1, a:1 };
				bClr = bPos.color || { r:1, g:1, b:1, a:1 };
				cClr = cPos.color || { r:1, g:1, b:1, a:1 };

				this.colors.push(aClr.r, aClr.g, aClr.b);
				this.colors.push(bClr.r, bClr.g, bClr.b);
				this.colors.push(cClr.r, cClr.g, cClr.b);

				this.vcolors.push(aClr.r, aClr.g, aClr.b, aClr.a);
				this.vcolors.push(bClr.r, bClr.g, bClr.b, bClr.a);
				this.vcolors.push(cClr.r, cClr.g, cClr.b, cClr.a);

				// UV Values

				this.uv.push(a.uv.x, a.uv.y);
				this.uv.push(b.uv.x, b.uv.y);
				this.uv.push(c.uv.x, c.uv.y);

				// Skin Index

				aIdx = aPos.skinIndice;
				bIdx = bPos.skinIndice;
				cIdx = cPos.skinIndice;

				this.skinIndex.push(aIdx.x, aIdx.y, aIdx.z, aIdx.w);
				this.skinIndex.push(bIdx.x, bIdx.y, bIdx.z, bIdx.w);
				this.skinIndex.push(cIdx.x, cIdx.y, cIdx.z, cIdx.w);

				// Skin Index

				aWgt = aPos.skinWeight;
				bWgt = bPos.skinWeight;
				cWgt = cPos.skinWeight;

				this.skinWeight.push(aWgt.x, aWgt.y, aWgt.z, aWgt.w);
				this.skinWeight.push(bWgt.x, bWgt.y, bWgt.z, bWgt.w);
				this.skinWeight.push(cWgt.x, cWgt.y, cWgt.z, cWgt.w);

				// Push to geometry faces

				if(!doubleSide) {
					continue;
				}

				this.matIndex.push(index);

				// Positions

				aPos = a.vertex;
				bPos = b.vertex;
				cPos = c.vertex;

				this.vertices.push(aPos.x, aPos.y, aPos.z);
				this.vertices.push(bPos.x, bPos.y, bPos.z);
				this.vertices.push(cPos.x, cPos.y, cPos.z);

				// Colors

				aClr = aPos.color || { r:1, g:1, b:1, a:1 };
				bClr = bPos.color || { r:1, g:1, b:1, a:1 };
				cClr = cPos.color || { r:1, g:1, b:1, a:1 };

				this.colors.push(aClr.r, aClr.g, aClr.b);
				this.colors.push(bClr.r, bClr.g, bClr.b);
				this.colors.push(cClr.r, cClr.g, cClr.b);

				this.vcolors.push(aClr.r, aClr.g, aClr.b, aClr.a);
				this.vcolors.push(bClr.r, bClr.g, bClr.b, bClr.a);
				this.vcolors.push(cClr.r, cClr.g, cClr.b, cClr.a);

				// UV Values

				this.uv.push(a.uv.x, a.uv.y);
				this.uv.push(b.uv.x, b.uv.y);
				this.uv.push(c.uv.x, c.uv.y);

				// Skin Index

				aIdx = aPos.skinIndice;
				bIdx = bPos.skinIndice;
				cIdx = cPos.skinIndice;

				this.skinIndex.push(aIdx.x, aIdx.y, aIdx.z, aIdx.w);
				this.skinIndex.push(bIdx.x, bIdx.y, bIdx.z, bIdx.w);
				this.skinIndex.push(cIdx.x, cIdx.y, cIdx.z, cIdx.w);

				// Skin Index

				aWgt = aPos.skinWeight;
				bWgt = bPos.skinWeight;
				cWgt = cPos.skinWeight;

				this.skinWeight.push(aWgt.x, aWgt.y, aWgt.z, aWgt.w);
				this.skinWeight.push(bWgt.x, bWgt.y, bWgt.z, bWgt.w);
				this.skinWeight.push(cWgt.x, cWgt.y, cWgt.z, cWgt.w);



			}

		}

	}

	readAnim(name) {

		if(name === "anim_000") {
			this.debug = true;
		}

		const FPS = 5;

		let motionOfs = this.bs.readUInt();
		let nbFrame = this.bs.readUInt();
		let motionType = this.bs.readUShort();
		let motionFlag = this.bs.readUShort();

		let nbElements = motionFlag & 0x0F;

		let motionList = new Array(this.bones.length);

		let motionTypes = {
			pos : BitStream.bitflag(motionType, 0),
			rot : BitStream.bitflag(motionType, 1),
			scl : BitStream.bitflag(motionType, 2),
			quat : motionType & 0x2000
		}

		if(this.debug) {
			console.log("Motion type: 0x%s", motionType.toString(16));
			console.log("Motion offset: 0x%s", motionOfs.toString(16));
			console.log("Number frames: %d", nbFrame);
			console.log(motionTypes);
			console.log(this.bones);
		}
	
		this.bs.seekSet(motionOfs);
		let max_frame = 0;

		// Read offsets to animation list for each bone

		for(let i = 0; i < this.bones.length; i++){

			let motionEntry = {
				bone : i,
				parent : i - 1,
				frames : []
			};

			// Read the offset to each list

			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}

				motionEntry[key] = {
					ofs : this.bs.readUInt()
				};

			}

			// Read the number of entries for each list

			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}

				let num =  this.bs.readUInt();
				max_frame = num > max_frame ? num : max_frame;
				if(num === 0) {
					delete motionEntry[key];
				} else {
					motionEntry[key].num = num;
				}

			}

			motionList[i] = motionEntry;

		}

		motionList.forEach(motion => {

			// Read Position

			if(motion.pos) {

				this.bs.seekSet(motion.pos.ofs);

				for(let i = 0; i < motion.pos.num; i++) {

					let frameNo = this.bs.readUInt();
					let pos = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].pos = pos;
				}

				delete motion.pos;
			}

			// Read Rotation

			if(motion.rot) {

				this.bs.seekSet(motion.rot.ofs);

				for(let i = 0; i < motion.rot.num; i++) {

					let frameNo = this.bs.readUInt();
					let rot = this.bs.readRot3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].rot = rot;
				}

				delete motion.rot;

			}

			// Read Scale

			if(motion.scl) {

				this.bs.seekSet(motion.scl.ofs);

				for(let i = 0; i < motion.scl.num; i++) {

					let frameNo = this.bs.readUInt();
					let scl = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].scl = scl;
				}

				delete motion.scl;

			}

			if(motion.quat) {
				
				this.bs.seekSet(motion.quat.ofs);

				for(let i = 0; i < motion.quat.num; i++) {

					let frameNo = this.bs.readUInt();
					let quat = this.bs.readVec4();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].quat = quat;
				}

				delete motion.quat;
			}

		});

		let animation = {
			name : this.bs.name.replace(".njm", ""),
			fps : FPS,
			length : (nbFrame - 1) / FPS,
			hierarchy : new Array(this.bones.length)
		}

		for(let i = 0; i < this.bones.length; i++) {

			let bone = this.bones[i];
			let motion = motionList[i];

			animation.hierarchy[i] = {
				parent : motion.parent,
				keys : []
			};

			for(let k = 0; k < nbFrame; k++) {

				let frame = motion.frames[k];

				if(frame && frame.pos) {
					let pos = frame.pos;
					frame.pos = [pos.x, pos.y, pos.z];
				}

				if(frame && frame.rot) {

					let obj = new THREE.Bone();

					var xRotMatrix = new THREE.Matrix4();
					xRotMatrix.makeRotationX(frame.rot.x);
					obj.applyMatrix(xRotMatrix);

					var yRotMatrix = new THREE.Matrix4();
					yRotMatrix.makeRotationY(frame.rot.y);
					obj.applyMatrix(yRotMatrix);

					var zRotMatrix = new THREE.Matrix4();
					zRotMatrix.makeRotationZ(frame.rot.z);
					obj.applyMatrix(zRotMatrix);

					let quat = new THREE.Quaternion();
					quat.setFromRotationMatrix(obj.matrix);
					frame.rot = quat.toArray();


				}

				if(frame && frame.quat) {
					frame.rot = [
						frame.quat.x,
						frame.quat.y,
						frame.quat.z,
						frame.quat.w
					];
				}

				if(frame && frame.scl) {
					let scl = frame.scl;
					frame.scl = [scl.x, scl.y, scl.z];
				}

				if(k === 0 || k === nbFrame - 1) {

					frame = frame || {};

					if(!frame.pos) {
						frame.pos = bone.position.toArray();
					}

					if(!frame.rot) {
						frame.rot = bone.quaternion.toArray();
					}

					if(!frame.scl) {
						frame.scl = bone.scale.toArray();
					}

				}

				if(!frame) {
					continue;
				}

				frame.time = k / FPS;

				animation.hierarchy[i].keys.push(frame);

			}

		}

		var clip = THREE.AnimationClip.parseAnimation(animation, this.bones);
		if(!clip) {
			console.warn("Clip is null?!");
			return;
		}

		clip.optimize();
		this.animList.push(clip);

	}


}
