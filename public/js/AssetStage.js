/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


const AssetStage = {

	"Pioneer 2" : async function() {

		let pvm = await NinjaFile.API.load("map_city00.pvm");
		let drel = await NinjaFile.API.load("map_city00_00d.rel");
		let nrel = await NinjaFile.API.load("map_city00_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "n128_kanban01":
			case "n128_kanban02":
			case "n128_kanban03":
			case "n128_cityinfoboard01":
			case "n128_newkanban01":
			case "n128_newkanban02":
			case "n64_citylaser_b01":
			case "h256_lob_kumo01":
			case "h64_hnt_k_light01":
			case "h32_hnt_k_light02":
			case "n064_tensoupnl02":
			case "n064_bl02":
			case "n064_bl01":
			case "g128_omotekan":
			case "o32_m_red01":
			case "o32_m_blue02":
			case "o16_m_blue03":
			case "n064_citymon_s01":
			case "n032_warpgls01":
			case "g064_uv_b0":
			case "h32_hnt_k_blue":
			case "h64_hnt_neon02":
			case "h128_hnt_kanban01":
			case "n064_tensoupnl02":
			case "n064_yajirusi01":
			case "o128_s_monitor01":
			case "o128_s_monitor01b":
			case "o128_s_monitor01c":
			case "o128_s_monitor02":
			case "o128_s_monitor02b":
			case "o128_s_monitor02c":
			case "o128_s_tekkotu02":
			case "o128_m_mark01":
			case "n064_citymon_s05":
			case "o64_s_monitor03":
			case "n064_kanban02":
			case "n032_beam01":
			case "h32_hnt_counter01":
			case "o32_m_orange01":
			case "o32_m_orange02":
			case "n064_yl02":
			case "ha_256_hikarioa":

				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				
				break;
			case "h32_hnt_k_light01":

				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].depthWrite = false;

				break;
			case "n064_ami01":
			case "n064_ami02":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				

				switch(mat.map.name) {
				case "h64_hnt_k_ling":
				case "n032_warpkasan01":
					mat.blending = 2;
					break;
				case "o32_m_blue01":
					mat.blending = 2;
					//mat.map.flipY = false;
					break;
				}

			switch(mat.map.name) {
			case "n128_kanban01":
			case "n128_kanban02":
			case "n128_kanban03":
			case "n128_cityinfoboard01":
			case "n128_newkanban01":
			case "n128_newkanban02":
			case "n64_citylaser_b01":
			case "h256_lob_kumo01":
			case "h64_hnt_k_light01":
			case "h32_hnt_k_light02":
			case "n064_tensoupnl02":
			case "n064_bl02":
			case "n064_bl01":
			case "g128_omotekan":
			case "o32_m_red01":
			case "o32_m_blue02":
			case "o16_m_blue03":
			case "n064_citymon_s01":
			case "n032_warpgls01":
			case "g064_uv_b0":
			case "h32_hnt_k_blue":
			case "h64_hnt_neon02":
			case "h128_hnt_kanban01":
			case "n064_tensoupnl02":
			case "n064_yajirusi01":
			case "o128_s_monitor01":
			case "o128_s_monitor01b":
			case "o128_s_monitor01c":
			case "o128_s_monitor02":
			case "o128_s_monitor02b":
			case "o128_s_monitor02c":
			case "o128_s_tekkotu02":
			case "o128_m_mark01":
			case "n064_citymon_s05":
			case "o64_s_monitor03":
			case "n064_kanban02":
			case "n032_beam01":
			case "h32_hnt_counter01":
			case "o32_m_orange01":
			case "o32_m_orange02":
			case "n064_yl02":

				mat.blending = 2;
				mat.side = THREE.DoubleSide;
				mat.transparent = true;
				mat.alphaTest = 0.05;
				
				break;
			case "h32_hnt_k_light01":

				mat.blending = 2;
				mat.side = THREE.DoubleSide;
				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.depthWrite = false;

				break;
			case "n064_ami01":
			case "n064_ami02":

				mat.transparent = true;
				mat.alphaTest = 0.05;

				break;
			default:
				console.log(mat.map.name);

				break;
			}


			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Forest 01" : async function() {

		let pvm = await NinjaFile.API.load("map_forest01.pvm");
		let drel = await NinjaFile.API.load("map_forest01d.rel");
		let nrel = await NinjaFile.API.load("map_forest01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors
			});

			switch(texList[i].name) {
			case "fo_t032_gaitohikari":
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
			case "fo_024k_fokager01":
			case "fo_064a_kinoko":
			case "fo_a064_kakushi":
			case "fo_k064_ha02":
			case "fo_t128_ha04":
			case "ts_128k_ha04":
			case "ts_128k_kiha02a":
			case "ts_512k_tiki03":
			case "t032_flower02_tm":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Forest 02" : async function() {

		let pvm = await NinjaFile.API.load("map_forest02.pvm");
		let drel = await NinjaFile.API.load("map_forest02d.rel");
		let nrel = await NinjaFile.API.load("map_forest02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "fo_t032_gaitohikari":
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
			case "fo_k064_ha02":
			case "fo_t128_ha04":
			case "k024_kage02":
			case "k128_foshiha01a":
			case "k128_fosidaabe":
			case "t032_flower02_tm":
			case "ts_128k_ha04":
			case "ts_512k_tiki03":
			case "ts_128k_kiha02a":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 00" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		matList[0].onBeforeCompile = function(shader, render) {
			console.log(shader);
		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 01" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_01d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 02" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_02d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 03" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_03d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 04" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_04d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 01 05" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_05d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02 00" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}


			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02 01" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_01d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02 02" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_02d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02 03" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_03d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02 04" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_04d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 00" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 01" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_01d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 02" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_02d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 03" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_03d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 04" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_04d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03 05" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_05d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 00" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_00d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 01" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_01d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 02" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_02d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 03" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_03d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 04" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_04d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01 05" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_05d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 00" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_00d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k064_kashilight01":
				matList[i].blending = 2;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 01" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_01d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k064_kashilight01":
				matList[i].blending = 2;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 02" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_02d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 03" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_03d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 04" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_04d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02 05" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_05d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01 00" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n128_taki_01":
			case "n64_taki_02":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01 01" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_01d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n128_taki_01":
			case "n64_taki_02":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01 02" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_02d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n64_taki_02":
			case "n128_taki_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01 03" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_03d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n64_taki_02":
			case "n128_taki_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01 04" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_04d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n64_taki_02":
			case "n128_taki_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02 00" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02 01" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_01d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02 02" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_02d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02 03" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_03d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02 04" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_04d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03 00" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03 01" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_01d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03 02" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_02d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03 03" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_03d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03 04" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_04d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Spaceship 00" : async function() {

		let pvm = await NinjaFile.API.load("map_vs01.pvm");
		let drel = await NinjaFile.API.load("map_vs01_00d.rel");
		let nrel = await NinjaFile.API.load("map_vs01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "k128_apsen":
			case "k128_apsen1":
			case "k128_apsen2":
			case "k128_apsen3":
					matList[i].blending = 2;
					matList[i].transparent = true;
					matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Spaceship 01" : async function() {

		let pvm = await NinjaFile.API.load("map_vs01.pvm");
		let drel = await NinjaFile.API.load("map_vs01_01d.rel");
		let nrel = await NinjaFile.API.load("map_vs01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "k128_apsen":
			case "k128_apsen1":
			case "k128_apsen2":
			case "k128_apsen3":
					matList[i].blending = 2;
					matList[i].transparent = true;
					matList[i].alphaTest = 0.05;
				break;
			case "":
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Spaceship 02" : async function() {

		let pvm = await NinjaFile.API.load("map_vs01.pvm");
		let drel = await NinjaFile.API.load("map_vs01_02d.rel");
		let nrel = await NinjaFile.API.load("map_vs01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				break;
			case "k128_apsen":
			case "k128_apsen1":
			case "k128_apsen2":
			case "k128_apsen3":
					matList[i].blending = 2;
					matList[i].transparent = true;
					matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Temple 00" : async function() {

		let pvm = await NinjaFile.API.load("map_vs02.pvm");
		let drel = await NinjaFile.API.load("map_vs02_00d.rel");
		let nrel = await NinjaFile.API.load("map_vs02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "n128_ki_ha01":
			case "n128_leaf02":
			case "n64_ami01":
			case "hn032_df_m_kusa02":
			case "n064_ami_yokonaga":
			case "h64_S01_h_w_ken01":
			case "h64_vs_m_kusa01":
			case "h64_vs_m_kusa02":
			case "h128_bg02":
			case "h128_bg04":
			case "h128_bg05":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "n064_sukimalight":
			case "h64_dotaki000":
			case "g064_mizu01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

			if(texList[i].name.indexOf("f064_sunmove") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("f064_sunset") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Temple 01" : async function() {

		let pvm = await NinjaFile.API.load("map_vs02.pvm");
		let drel = await NinjaFile.API.load("map_vs02_01d.rel");
		let nrel = await NinjaFile.API.load("map_vs02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "n128_ki_ha01":
			case "n128_leaf02":
			case "n64_ami01":
			case "hn032_df_m_kusa02":
			case "n064_ami_yokonaga":
			case "h64_S01_h_w_ken01":
			case "h64_vs_m_kusa01":
			case "h64_vs_m_kusa02":
			case "h128_bg02":
			case "h128_bg04":
			case "h128_bg05":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "n064_sukimalight":
			case "h64_dotaki000":
			case "g064_mizu01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

			if(texList[i].name.indexOf("f064_sunmove") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("f064_sunset") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}


		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Temple 02" : async function() {

		let pvm = await NinjaFile.API.load("map_vs02.pvm");
		let drel = await NinjaFile.API.load("map_vs02_02d.rel");
		let nrel = await NinjaFile.API.load("map_vs02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "n128_ki_ha01":
			case "n128_leaf02":
			case "n64_ami01":
			case "hn032_df_m_kusa02":
			case "n064_ami_yokonaga":
			case "h64_S01_h_w_ken01":
			case "h64_vs_m_kusa01":
			case "h64_vs_m_kusa02":
			case "h128_bg02":
			case "h128_bg04":
			case "h128_bg05":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "n064_sukimalight":
			case "h64_dotaki000":
			case "g064_mizu01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

			if(texList[i].name.indexOf("f064_sunmove") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("f064_sunset") !== -1) {
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}


		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Forest 01" : async function() {

		let pvm = await NinjaFile.API.load("map_aforest01.pvm");
		let drel = await NinjaFile.API.load("map_aforest01d.rel");
		let nrel = await NinjaFile.API.load("map_aforest01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors
			});

			switch(texList[i].name) {
			case "fo_t032_gaitohikari":
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
			case "fo_024k_fokager01":
			case "fo_064a_kinoko":
			case "fo_a064_kakushi":
			case "fo_k064_ha02":
			case "fo_t128_ha04":
			case "ts_128k_ha04":
			case "ts_128k_kiha02a":
			case "ts_512k_tiki03":
			case "t032_flower02_tm":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Forest 02" : async function() {

		let pvm = await NinjaFile.API.load("map_aforest02.pvm");
		let drel = await NinjaFile.API.load("map_aforest02d.rel");
		let nrel = await NinjaFile.API.load("map_aforest02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "fo_t032_gaitohikari":
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;
			case "fo_k064_ha02":
			case "fo_t128_ha04":
			case "k024_kage02":
			case "k128_foshiha01a":
			case "k128_fosidaabe":
			case "t032_flower02_tm":
			case "ts_128k_ha04":
			case "ts_512k_tiki03":
			case "ts_128k_kiha02a":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 00" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_00d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		matList[0].onBeforeCompile = function(shader, render) {
			console.log(shader);
		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 01" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_01d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 02" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_02d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 03" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_03d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 04" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_04d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 01 05" : async function() {

		let pvm = await NinjaFile.API.load("map_acave01.pvm");
		let drel = await NinjaFile.API.load("map_acave01_05d.rel");
		let nrel = await NinjaFile.API.load("map_acave01_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 02 00" : async function() {

		let pvm = await NinjaFile.API.load("map_acave02.pvm");
		let drel = await NinjaFile.API.load("map_acave02_00d.rel");
		let nrel = await NinjaFile.API.load("map_acave02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 02 01" : async function() {

		let pvm = await NinjaFile.API.load("map_acave02.pvm");
		let drel = await NinjaFile.API.load("map_acave02_01d.rel");
		let nrel = await NinjaFile.API.load("map_acave02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 02 02" : async function() {

		let pvm = await NinjaFile.API.load("map_acave02.pvm");
		let drel = await NinjaFile.API.load("map_acave02_02d.rel");
		let nrel = await NinjaFile.API.load("map_acave02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 02 03" : async function() {

		let pvm = await NinjaFile.API.load("map_acave02.pvm");
		let drel = await NinjaFile.API.load("map_acave02_03d.rel");
		let nrel = await NinjaFile.API.load("map_acave02_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 02 04" : async function() {

		let pvm = await NinjaFile.API.load("map_acave02.pvm");
		let drel = await NinjaFile.API.load("map_acave02_04d.rel");
		let nrel = await NinjaFile.API.load("map_acave02_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			if(texList[i].name.indexOf("ha_u064_dohamon") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u064_domizu") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("ha_u128_dotaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			} else if(texList[i].name.indexOf("k128_ktaki") !== -1) {
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
			}

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 00" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_00d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 01" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_01d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 02" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_02d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 03" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_03d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 04" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_04d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Cave 03 05" : async function() {

		let pvm = await NinjaFile.API.load("map_acave03.pvm");
		let drel = await NinjaFile.API.load("map_acave03_05d.rel");
		let nrel = await NinjaFile.API.load("map_acave03_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 00" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_00d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 01" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_01d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 02" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_02d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 03" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_03d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 04" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_04d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 01 05" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine01.pvm");
		let drel = await NinjaFile.API.load("map_amachine01_05d.rel");
		let nrel = await NinjaFile.API.load("map_amachine01_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});



		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 00" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_00d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k064_kashilight01":
				matList[i].blending = 2;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 01" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_01d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k064_kashilight01":
				matList[i].blending = 2;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 02" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_02d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 03" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_03d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 04" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_04d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_04n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Mines 02 05" : async function() {

		let pvm = await NinjaFile.API.load("map_amachine02.pvm");
		let drel = await NinjaFile.API.load("map_amachine02_05d.rel");
		let nrel = await NinjaFile.API.load("map_amachine02_05n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
			case "k064_kashilight01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Boss 1 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_boss01.pvm");
		let drel = await NinjaFile.API.load("map_boss01d.rel");
		let nrel = await NinjaFile.API.load("map_boss01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "fo_k512_dragake02":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Boss 2 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_boss02.pvm");
		let drel = await NinjaFile.API.load("map_boss02d.rel");
		let nrel = await NinjaFile.API.load("map_boss02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Boss 3 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_boss03.pvm");
		let drel = await NinjaFile.API.load("map_boss03d.rel");
		let nrel = await NinjaFile.API.load("map_boss03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			}

		}

		let stageLoader = new NinjaStage(texList, matList);

		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.executeMines(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Boss 4 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_darkfalz00.pvm");
		let drel = await NinjaFile.API.load("map_darkfalz00d.rel");
		let nrel = await NinjaFile.API.load("map_darkfalz00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});
			
			/*
			if(i >= 20 && i < 24) {
				matList[i].visible = false;
				console.log(texList[i].name);
			}
			*/

			switch(texList[i].name) {
			case "h64_df_flower01":
			case "h64_df_flower01":
			case "h64_df_kusa02":
			case "h64_df_m_kusa01":
			case "h64_df_m_kusa02":
			case "h256_df_sora02":
			case "h256_df2_kumo01":
			case "h256_df_zangai01":
			case "h256_df_zangai02":
			case "h256_df_kumo_kage":
			case "h256_df_kumo01":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].side = THREE.DoubleSide;

				break;
			case "h64_df3_moyou03":
			case "h128_df_sun":
			case "h128_df_sun02":
			case "h128_df3_lightsuji01":
			case "h128_df3_maho_moji01":
			case "h128_df3_mahou01":
			case "h128_df3_rainbow":
			case "h256_df3_k_moji01":
			case "h64_df3_moyou02":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);

		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		
		/*
		stageLoader.sections.section_000.bone.position.x = -5000;
		console.log(stageLoader);
		*/

		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		console.log(animList);
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				switch(mat.map.name) {
				case "h256_df3_k_moji01":
				case "h64_df3_moyou02":
				case "h128_df3_mahou01":

					break;
				default:
					return;
					break;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Boss 1 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_aboss01.pvm");
		let drel = await NinjaFile.API.load("map_aboss01d.rel");
		let nrel = await NinjaFile.API.load("map_aboss01n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "fo_k512_dragake02":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				
				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Boss 2 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_aboss02.pvm");
		let drel = await NinjaFile.API.load("map_aboss02d.rel");
		let nrel = await NinjaFile.API.load("map_aboss02n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ultimate Boss 3 Area" : async function() {

		let pvm = await NinjaFile.API.load("map_aboss03.pvm");
		let drel = await NinjaFile.API.load("map_aboss03d.rel");
		let nrel = await NinjaFile.API.load("map_aboss03n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			}

		}

		let stageLoader = new NinjaStage(texList, matList);

		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.executeAltMines(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Lobby Black" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_black00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_black00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_black00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Blue" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_blue00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_blue00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_blue00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Bluegreen" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_bluegreen00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_bluegreen00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_bluegreen00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Green" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_green00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_green00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_green00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Orange" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_orange00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_orange00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_orange00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Purple" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_purple00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_purple00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_purple00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Red" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_red00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_red00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_red00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby White" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_white00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_white00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_white00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Yellow" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_yellow00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_yellow00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_yellow00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Lobby Yellow Green" : async function() {

		let pvm = await NinjaFile.API.load("map_lobby_y_green00.pvm");
		let drel = await NinjaFile.API.load("map_lobby_y_green00d.rel");
		let nrel = await NinjaFile.API.load("map_lobby_y_green00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);


	},

	"Map Soccer 11" : async function() {

		let pvm = await NinjaFile.API.load("map_soccer11.pvm");
		let drel = await NinjaFile.API.load("map_soccer11d.rel");
		let nrel = await NinjaFile.API.load("map_soccer11n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Map Soccer 12" : async function() {

		let pvm = await NinjaFile.API.load("map_soccer12.pvm");
		let drel = await NinjaFile.API.load("map_soccer12d.rel");
		let nrel = await NinjaFile.API.load("map_soccer12n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Map Soccer 13" : async function() {

		let pvm = await NinjaFile.API.load("map_soccer13.pvm");
		let drel = await NinjaFile.API.load("map_soccer13d.rel");
		let nrel = await NinjaFile.API.load("map_soccer13n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "h128_lob_no0":
			case "h256_lob_kumo01":
			case "h128_lob_wind01":
			case "h64_lob_movelight":
			case "ha_256_hikarioa":
			case "ha_hikari_ao":

				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				matList[i].blending = 2;
				matList[i].side = THREE.DoubleSide;

				break;
			}

		}

		let stageLoader = new NinjaStage(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},





}
