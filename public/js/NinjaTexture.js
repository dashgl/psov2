/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


"use strict";

const ARGB_1555 = 0x00;
const RGB_565 = 0x01;
const ARGB_4444 = 0x02;
const YUV_422 = 0x03;
const BUMP = 0x04;

const TWIDDLED = 0x01;
const TWIDDLED_MM = 0x02;
const VQ = 0x03;
const VQ_MM = 0x04;
const PALLET4 = 0x05;
const PALLET4_MM = 0x06;
const PALLET8 = 0x07;
const PALLET8_MM = 0x08;
const RECTANGLE = 0x09;
const STRIDE = 0x0B;
const RECT_TWIDDLED = 0x0D;
const SMALL_VQ = 0x10;
const SMALL_VQ_MM = 0x11;
const TWIDDLED_MM_ALT = 0x12;

const NinjaTexture = (function () {

	this.API = {
		parse : api_parse.bind(this),
		readTexture : api_readTexture.bind(this),
		readTexturePack : api_readTexturePack.bind(this),
		read : api_read.bind(this),
		checkMipmap : api_checkMipmap.bind(this),
		checkSmall : api_checkSmall.bind(this),
		decodeTwiddle : api_decodeTwiddle.bind(this),
		decodeVector : api_decodeVector.bind(this),
		decodeRectangle : api_decodeRectangle.bind(this),
		readTwiddled : api_readTwiddled.bind(this)
	};
	return this;

	function api_parse(bs, whiteFill) {
		
		this.whiteFill = whiteFill || [];
		let magic = bs.readString(4);
		switch(magic) {
		case "GBIX":
		case "PVRT":
			
			bs.seekCur(-4);
			return this.API.readTexture(bs);

			break;
		case "PVMH":

			return this.API.readTexturePack(bs);

			break;
		default:
			console.log(magic);
			
			let blob = new Blob([bs.data.buffer]);
			saveAs(blob, bs.name);

			throw new Error("unknown magic for texture");
			break;
		}

	}

	function api_readTexture(bs) {

		let pvrt;
		do {
			pvrt = bs.readString(4);
		} while (pvrt !== "PVRT");

		let len = bs.readUInt();
		bs.setOfs(len);
		
		let tex = { name : bs.name };
		let texture = this.API.read(bs, tex);
		return texture;

	}

	function api_readTexturePack(bs) {

		console.log("Read texture pack");

		let len = bs.readUInt();
		bs.setOfs(len);

		let texFlags = bs.readUShort();
		let nbTex = bs.readUShort();
		let pvrList = new Array(nbTex);

		for(let i = 0; i < nbTex; i++) {
			
			let tex = {};
			tex.id = bs.readUShort();
			tex.index = i;

			if (BitStream.bitflag(texFlags, 3)) {
				tex.name = bs.readString(0x1c);
			}

			if (BitStream.bitflag(texFlags, 2)) {
				tex.format = bs.readUShort();
			}

			if (BitStream.bitflag(texFlags, 1)) {
				tex.dimensions = bs.readUShort();
			}

			if (BitStream.bitflag(texFlags, 0)) {
				tex.gbix = bs.readUInt();
			}

			pvrList[i] = tex;

		}

		bs.clearOfs();
		let texList = [];

		pvrList.forEach(tex => {
			
			let pvrt;
			do {
				pvrt = bs.readString(4);
			} while (pvrt !== "PVRT");

			let len = bs.readUInt();
			bs.setOfs(len);

			let texture = this.API.read(bs, tex);
			texList.push(texture);

			bs.clearOfs();

		});
	
		return texList;
	}

	function api_read(bs, tex) {


		let pixelFormat = bs.readByte();
		let dataFormat = bs.readByte();
		bs.seekCur(0x02);

		let width = bs.readUShort();
		let height = bs.readUShort();

		let canvas = document.createElement("canvas");
		canvas.width = width;
		canvas.height = height;
		let ctx = canvas.getContext("2d");

		let isMipmap = this.API.checkMipmap(dataFormat);
		let isSmall = this.API.checkSmall(dataFormat);

		let image;
		switch (dataFormat) {
		case TWIDDLED_MM:
		case TWIDDLED:
		case TWIDDLED_MM_ALT:
			image = this.API.decodeTwiddle(bs, width, height, isMipmap);
			break;
		case VQ:
		case VQ_MM:
		case SMALL_VQ:
		case SMALL_VQ_MM:
			image = this.API.decodeVector(bs, width, height, isMipmap, isSmall);
			break;
		case RECTANGLE:
			image = this.API.decodeRectangle(bs, width, height);
			break;
		default:
			throw new Error("Unknown PVR Image format: " + dataFormat);
			break;
		}

		if(this.whiteFill.indexOf(tex.index) !== -1) {
			ctx.fillStyle = "#fff";
			ctx.fillRect(0, 0, width, height);
		}

		let transparent = false;
		for (let y = 0; y < height; y++) {
			for (let x = 0; x < width; x++) {
				let i = y * width + x;

				let r, g, b, a;
				switch (pixelFormat) {
				case ARGB_1555:
					a = (image[i] & 0x8000) ? 1 : 0;
					r = (image[i] & 0x7C00) >> 7;
					g = (image[i] & 0x03E0) >> 2;
					b = (image[i] & 0x001F) << 3;

					break;
				case RGB_565:
					r = (image[i] >> 8) & (0x1f << 3);
					g = (image[i] >> 3) & (0x3f << 2);
					b = (image[i] << 3) & (0x1f << 3);
					a = 1;

					break;
				case ARGB_4444:
					a = ((image[i] >> 8) & 0xf0) / 255;
					r = (image[i] >> 4) & 0xf0;
					g = (image[i] >> 0) & 0xf0;
					b = (image[i] << 4) & 0xf0;

					break;
				}

				if (a < 1) {
					transparent = true;
				}

				ctx.fillStyle = "rgba(" + r + "," + g + "," + b + "," + a + ")";
				ctx.fillRect(x, y, 1, 1);
			}
		}

		let texture = new THREE.Texture(canvas);
		//texture.flipY = false;

		texture.wrapS = THREE.MirroredRepeatWrapping;
		texture.wrapT = THREE.MirroredRepeatWrapping;

		const REPEAT_TEXTURES = [
			"k128_yukaani"
		];

		for(let i = 0; i < REPEAT_TEXTURES.length; i++) {
			if(tex.name.indexOf(REPEAT_TEXTURES[i]) === -1){
				continue;
			}
			texture.wrapS = THREE.RepeatWrapping;
			texture.wrapT = THREE.RepeatWrapping;
			break;
		}

		texture.name = tex.name;
		texture.needsUpdate = true;
		texture.transparent = transparent;
		return texture;

	}

	function api_checkMipmap(needle) {

		const haystack = [
			TWIDDLED_MM,
			VQ_MM,
			PALLET4_MM,
			PALLET8_MM,
			SMALL_VQ_MM,
			TWIDDLED_MM_ALT
		];

		return haystack.indexOf(needle) !== -1;

	}

	function api_checkSmall(needle) {

		const haystack = [
			SMALL_VQ,
			SMALL_VQ_MM
		];

		return haystack.indexOf(needle) !== -1;

	}

	function api_decodeTwiddle(bs, width, height, isMipmap) {

		if (isMipmap) {
			let seekOfs = 0x02;
			for (let i = 0; i <= 10; i++) {
				let mipWidth = 0x01 << i;
				if (width === mipWidth) {
					break;
				}
				seekOfs += mipWidth * mipWidth * 2;
			}
			bs.seekCur(seekOfs);
		}

		let image = this.API.readTwiddled(bs, width, false);
		return image;

	}

	function api_decodeVector(bs, width, height, isMipmap, isSmall) {

		let clutSize = 256;
		if (isSmall) {
			if (isMipmap) {
				switch (width) {
				case 8:
				case 16:
					clutSize = 16;
					break;
				case 32:
					clutSize = 64;
					break;
				}
			} else {
				switch (width) {
				case 8:
				case 16:
					clutSize = 16;
					break;
				case 32:
					clutSize = 32;
					break;
				case 64:
					clutSize = 128;
					break;
				}
			}
		}
		let clut = new Array(clutSize * 4);
		for (let i = 0; i < clut.length; i++) {
			clut[i] = bs.readUShort();
		}

		if (isMipmap) {
			let seekOfs = 0x01;

			for (let i = 0; i <= 10; i++) {
				let mipWidth = 0x01 << i;

				if (width === mipWidth) {
					break;
				}

				seekOfs += parseInt(mipWidth * mipWidth / 4);
			}

			bs.seekCur(seekOfs);
		}

		let image = new Array(width * height);
		let dataBody = this.API.readTwiddled(bs, width / 2, true);

		let x = 0;
		let y = 0;
		for (let i = 0; i < dataBody.length; i++) {
			let clutOfs = dataBody[i] * 4;

			for (let xOfs = 0; xOfs < 2; xOfs++) {
				for (let yOfs = 0; yOfs < 2; yOfs++) {
					let pix = (y * 2 + yOfs) * width + (x * 2 + xOfs);
					image[pix] = clut[clutOfs++];
				}
			}

			x++;
			if (x === parseInt(width / 2)) {
				x = 0;
				y++;
			}
		}

		return image;

	
	}

	function api_decodeRectangle(bs, width, height) {

		let image = new Array(width * height);

		for (let y = 0; y < height; y++) {
			for (let x = 0; x < width; x++) {
				image[y * height + x] = bs.readUShort();
			}
		}

		return image;

	}

	function api_readTwiddled(bs, width, isVq) {

		let list = new Array(width * width);
		subdivideAndMove.apply(this, [0, 0, width]);
		return list;

		function subdivideAndMove(x, y, mipSize) {
			if (mipSize === 1) {
				if (isVq) {
					list[y * width + x] = bs.readByte();
				} else {
					list[y * width + x] = bs.readUShort();
				}
			} else {
				let ns = parseInt(mipSize / 2);
				subdivideAndMove.apply(this, [x, y, ns]);
				subdivideAndMove.apply(this, [x, y + ns, ns]);
				subdivideAndMove.apply(this, [x + ns, y, ns]);
				subdivideAndMove.apply(this, [x + ns, y + ns, ns]);
			}
		}


	}

}).apply({});

console.log(NinjaTexture);

