/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/

const AssetRooms = {

    "Forest 01" : async function() {

        let pvm = await NinjaFile.API.load("map_forest01.pvm");
        let drel = await NinjaFile.API.load("map_forest01d.rel");
        let nrel = await NinjaFile.API.load("map_forest01n.rel");

        let texList = NinjaTexture.API.parse(pvm);
        let matList = new Array(texList.length);
        for(let i = 0; i < matList.length; i++) {

            let num = i.toString();
            while(num.length < 3) {
                num = "0" + num;
            }

            matList[i] = new THREE.MeshBasicMaterial({
                name : "material_" + num,
                map : texList[i],
                vertexColors : THREE.VertexColors
            });

            switch(texList[i].name) {
            case "fo_t032_gaitohikari":
                matList[i].blending = 2;
                matList[i].side = THREE.DoubleSide;
            case "fo_024k_fokager01":
            case "fo_064a_kinoko":
            case "fo_a064_kakushi":
            case "fo_k064_ha02":
            case "fo_t128_ha04":
            case "ts_128k_ha04":
            case "ts_128k_kiha02a":
            case "ts_512k_tiki03":
            case "t032_flower02_tm":
                matList[i].transparent = true;
                matList[i].alphaTest = 0.05;
                break;
            }

        }

        let stageLoader = new NinjaRoom(texList, matList);
        stageLoader.prepare(drel);
        stageLoader.prepare(nrel);
        stageLoader.execute(this);
        let meshList = stageLoader.getMeshList();
        let animList = stageLoader.getModelList();
        NinjaPlugin.API.setStage(this, meshList, animList, texList);

    },

    "Forest 02" : async function() {

        let pvm = await NinjaFile.API.load("map_forest02.pvm");
        let drel = await NinjaFile.API.load("map_forest02d.rel");
        let nrel = await NinjaFile.API.load("map_forest02n.rel");

        let texList = NinjaTexture.API.parse(pvm);
        let matList = new Array(texList.length);
        for(let i = 0; i < matList.length; i++) {

            let num = i.toString();
            while(num.length < 3) {
                num = "0" + num;
            }

            matList[i] = new THREE.MeshBasicMaterial({
                name : "material_" + num,
                map : texList[i],
                vertexColors : THREE.VertexColors
            });

            switch(texList[i].name) {
            case "fo_t032_gaitohikari":
                matList[i].blending = 2;
                matList[i].side = THREE.DoubleSide;
            case "fo_k064_ha02":
            case "fo_t128_ha04":
            case "k024_kage02":
            case "k128_foshiha01a":
            case "k128_fosidaabe":
            case "t032_flower02_tm":
            case "ts_128k_ha04":
            case "ts_512k_tiki03":
            case "ts_128k_kiha02a":
                matList[i].transparent = true;
                matList[i].alphaTest = 0.05;
                break;
            }

        }

        let stageLoader = new NinjaRoom(texList, matList);
        stageLoader.prepare(drel);
        stageLoader.prepare(nrel);
        stageLoader.execute(this);
        let meshList = stageLoader.getMeshList();
        let animList = stageLoader.getModelList();
        NinjaPlugin.API.setStage(this, meshList, animList, texList);

    },


	"Cave 01" : async function() {

		let pvm = await NinjaFile.API.load("map_cave01.pvm");
		let drel = await NinjaFile.API.load("map_cave01_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors
			});

			switch(texList[i].name) {
			case "a256_ro":
			case "k064_dok3yogamata":
			case "k128_doksakeyoga02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 02" : async function() {

		let pvm = await NinjaFile.API.load("map_cave02.pvm");
		let drel = await NinjaFile.API.load("map_cave02_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Cave 03" : async function() {

		let pvm = await NinjaFile.API.load("map_cave03.pvm");
		let drel = await NinjaFile.API.load("map_cave03_00d.rel");
		let nrel = await NinjaFile.API.load("map_cave03_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors,
				transparent : true
			});

			switch(texList[i].name) {
			case "a128_karikusa":
			case "k128_casakoke02":
			case "k128_dokokemu02":
			case "sa_128k_happa04":
			case "zk128k_kiha02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 01" : async function() {

		let pvm = await NinjaFile.API.load("map_machine01.pvm");
		let drel = await NinjaFile.API.load("map_machine01_00d.rel");
		let nrel = await NinjaFile.API.load("map_machine01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();

		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Mines 02" : async function() {

		let pvm = await NinjaFile.API.load("map_machine02.pvm");
		let drel = await NinjaFile.API.load("map_machine02_00d.rel");
		let nrel = await NinjaFile.API.load("map_machine02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "a128_yuka":
			case "a256_hakai":
			case "ki_u256_vo_hakai01":
			case "k064_krail01":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k128_r7yukahyo5":
			case "k256_r7yukahyo":
			case "k256_r7yukahyo1":
			case "k256_r7yukahyo3":
			case "k256_r7yukahyo4":
			case "k128_ktlaz01":
			case "k064_bkorelaz01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "k064_kashilight01":
				matList[i].blending = 2;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}
				
				if(mat.map.name.indexOf("k128_klani") === -1) {
					return;
				}

				mat.transparent = true;
				mat.alphaTest = 0.05;
				mat.blending = 2;
				mat.side = THREE.DoubleSide;

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 01" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient01.pvm");
		let drel = await NinjaFile.API.load("map_ancient01_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient01_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "n64_grnlght01":
			case "n128_k2kasan03":
			case "n128_kasan02":
			case "n256_hone01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				break;
			case "g032_k_red":
			case "h32_k_red":
			case "h32_k_yellow":
			case "n128_taki_01":
			case "n64_taki_02":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 02" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient02.pvm");
		let drel = await NinjaFile.API.load("map_ancient02_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient02_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki03":
			case "n64_grnlght01":
			case "n64_grnlght03":
			case "n64_yukaami01":
			case "n128_k2mcn01":
			case "n128_kasan01":
			case "n128_kasan02":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "g64_blue_k":
			case "h32_k_orange":
			case "h32_k_red02":
			case "h32_k_yellow":
			case "h64_k_hikari_01":
			case "h64_light_k_01":
			case "h64_light_k_02":
			case "h64_light_k_03":
			case "h64_k_hikari_01":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "h64_light_k_02":
				case "h64_light_k_03":
				case "h64_light_k_01":
				case "g64_blue_k":
				case "h32_k_orange":
				case "h32_k_red02":
				case "h32_k_yellow":
				case "h64_k_hikari_01":
				case "h64_light_k_01":
				case "h64_light_k_02":
				case "h64_light_k_03":
					mat.blending = 2;
					mat.transparent = true;
					mat.alphaTest = 0.05;
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	},

	"Ruins 03" : async function() {

		let pvm = await NinjaFile.API.load("map_ancient03.pvm");
		let drel = await NinjaFile.API.load("map_ancient03_00d.rel");
		let nrel = await NinjaFile.API.load("map_ancient03_00n.rel");

		let texList = NinjaTexture.API.parse(pvm);
		let matList = new Array(texList.length);
		for(let i = 0; i < matList.length; i++) {
			
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			matList[i] = new THREE.MeshBasicMaterial({
				name : "material_" + num,
				map : texList[i],
				vertexColors : THREE.VertexColors 
			});

			switch(texList[i].name) {
			case "g128_nuki01":
			case "h64_k_koushi":
			case "h128_cord03":
			case "h128_main3_05":
			case "h128_main3_suji":
			case "h128_mayu02":
			case "h128_meca03":
			case "n128_k2mcn01":
			case "n128_kasan02":
			case "n256_kumonos02":
			case "n256_kumonos03":
			case "n256_mado01":
			case "o64_t_sikiri4":
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			case "h64_k_hikari_02":
			case "h64_light_k_01":
			case "h128_kumonosu01":
			case "h128_kumonosu02":
			case "h128_kumonosu03":
				matList[i].blending = 2;
				matList[i].transparent = true;
				matList[i].alphaTest = 0.05;
				break;
			}

		}

		let stageLoader = new NinjaRoom(texList, matList);
		stageLoader.prepare(drel);
		stageLoader.prepare(nrel);
		stageLoader.execute(this, true);
		let meshList = stageLoader.getMeshList();
		let animList = stageLoader.getModelList();
		
		animList.forEach(mesh => {
			
			mesh.material.forEach(mat => {
				
				if(!mat.map) {
					return;
				}

				switch(mat.map.name) {
				case "":
					break;
				}

			});

		});

		NinjaPlugin.API.setStage(this, meshList, animList, texList);

	}

}
