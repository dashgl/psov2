/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/

class NinjaModel {

	constructor(name, tex) {
		
		this.name = name;
		this.bones = [];
		this.texList = tex || [];
		this.matList = [];
		this.vertexStack = [];
		this.animList = [];

		this.v = [];

		this.vertices = [];
		this.matIndex = [];
		this.colors = [];
		this.vcolors = [];
		this.uv = [];
		this.skinWeight = [];
		this.skinIndex = [];

	}

	parse(bs, forceMerge) {

		let len;
		this.bs = bs;
		
		do {
			
			const magic = this.bs.readString(4)
			switch(magic) {
			case "NJTL":
				
				len = this.bs.readUInt();
				this.bs.seekCur(len);

				break;
			case "NJCM":
				
				if(this.bones.length && !forceMerge) {
					this.bs.seekCur(-4);
					return;
				} else if(forceMerge) {
					this.bones = [];
				}

				len = this.bs.readUInt();
				this.bs.setOfs(len);
				this.readBone();
				this.bs.clearOfs();

				break;
			case "NMDM":

				len = this.bs.readUInt();
				this.bs.setOfs(len);
				this.readAnim();
				this.bs.clearOfs();
				break;
			}
			
		} while(this.bs.tell() < this.bs.len() - 4);


	}

	rel(type, bs) {
		
		if(bs) {
			this.bs = bs;
		}

		switch(type) {
		case "NJCM":
			this.readBone();
			break;
		case "NMDM":
			this.readAnim();
			break;
		}

	}

	addAnimation(bs) {

		this.parse(bs);

	}

	getItem() {

        let geometry = new THREE.BufferGeometry();
        let vertices = new Float32Array(this.vertices);
        let colors = new Float32Array(this.colors);
        let vcolors = new Float32Array(this.vcolors);
        let uv = new Float32Array(this.uv);
		let skinWeight = new Float32Array(this.skinWeight);
		let skinIndex = new Uint16Array(this.skinIndex);

        geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'vcolor', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'uv', new THREE.BufferAttribute( uv, 2 ) );
        geometry.addAttribute( 'skinWeight', new THREE.BufferAttribute( skinWeight, 4 ) );
        geometry.addAttribute( 'skinIndex', new THREE.BufferAttribute( skinIndex, 4 ) );

        geometry.computeVertexNormals();

        let stack = [];

        for(let i = 0; i < this.matIndex.length; i++) {

            let last = stack[stack.length - 1] || {};

            if(this.matIndex[i] === last.materialIndex) {
                last.count += 3;
                continue;
            }

            stack.push({
                start : i*3,
                count : 3,
                materialIndex : this.matIndex[i]
            });

        }

        stack.forEach(m => {
            geometry.addGroup(m.start, m.count, m.materialIndex);
        });

		let materials = [];
		for(let i = 0; i < this.matList.length; i++) {

			let mat = new THREE.MeshBasicMaterial({
				skinning : true,
				vertexColors : THREE.VertexColors,
				transparent : true
			});
			
			if(this.matList[i].blending) {
				mat.blending = 2;
			}
			
			if(this.matList[i].doubleSide) {
				mat.side = THREE.DoubleSide;
			}

			if(this.matList[i].texId !== -1) {
				mat.map = this.texList[this.matList[i].texId];
				if(mat.map.transparent) {
					mat.transparent = true;
					mat.alphaTest = 0.05;
				}

			}

			materials.push(mat);
		}

		let mesh = new THREE.Mesh(geometry, materials);
		mesh.name = this.name;
		return mesh;

	}

	getModel() {

        let geometry = new THREE.BufferGeometry();
        let vertices = new Float32Array(this.vertices);
        let colors = new Float32Array(this.colors);
        let vcolors = new Float32Array(this.vcolors);
        let uv = new Float32Array(this.uv);
		let skinWeight = new Float32Array(this.skinWeight);
		let skinIndex = new Uint16Array(this.skinIndex);

        geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
        geometry.addAttribute( 'color', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'vcolor', new THREE.BufferAttribute( vcolors, 4 ) );
        geometry.addAttribute( 'uv', new THREE.BufferAttribute( uv, 2 ) );
        geometry.addAttribute( 'skinWeight', new THREE.BufferAttribute( skinWeight, 4 ) );
        geometry.addAttribute( 'skinIndex', new THREE.BufferAttribute( skinIndex, 4 ) );

        geometry.computeVertexNormals();

        let stack = [];

        for(let i = 0; i < this.matIndex.length; i++) {

            let last = stack[stack.length - 1] || {};

            if(this.matIndex[i] === last.materialIndex) {
                last.count += 3;
                continue;
            }

            stack.push({
                start : i*3,
                count : 3,
                materialIndex : this.matIndex[i]
            });

        }

        stack.forEach(m => {
            geometry.addGroup(m.start, m.count, m.materialIndex);
        });

		let materials = [];
		for(let i = 0; i < this.matList.length; i++) {

			let mat = new THREE.MeshBasicMaterial({
				skinning : true,
				vertexColors : THREE.VertexColors
			});
			
			if(this.matList[i].blending) {
				mat.blending = 2;
			}
			
			if(this.matList[i].doubleSide) {
				mat.side = THREE.DoubleSide;
			}

			if(this.matList[i].texId !== -1 && this.texList[this.matList[i].texId]) {
				mat.map = this.texList[this.matList[i].texId];
				if(mat.map.transparent) {
					mat.transparent = true;
					mat.alphaTest = 0.05;
				}

			}

			materials.push(mat);
		}

		let mesh = new THREE.SkinnedMesh(geometry, materials);
		mesh.name = this.name;
		let armSkeleton = new THREE.Skeleton(this.bones);
		let rootBone = armSkeleton.bones[0];
		mesh.add(rootBone);
		mesh.bind(armSkeleton);
		mesh.geometry.animations = this.animList;
		return mesh;

	}

	readBone(parentBone) {

		//  Read bone structure

		const bone = {
			ofs : this.bs.tellf(),
			flag : this.bs.readUInt()
		}

		const { flag } = bone;

		if (flag & 0x400) {
			bone.chunkOfs = this.bs.readUInt();
			bone.pos = this.bs.readVec3();
			bone.rot = this.bs.readVec3();
			bone.scl = this.bs.readVec3();
			bone.childOfs = this.bs.readUInt();
			bone.siblingOfs = this.bs.readUInt();
			bone.rot.w = this.bs.readFloat();
		} else {
			bone.chunkOfs = this.bs.readUInt();
			bone.pos = this.bs.readVec3();
			bone.rot = this.bs.readRot3();
			bone.scl = this.bs.readVec3();
			bone.childOfs = this.bs.readUInt();
			bone.siblingOfs = this.bs.readUInt();
		}

		// Create New Bone for Three.js

		let num = this.bones.length.toString();
		while(num.length < 3) {
			num = "0" + num;
		}

		this.bone = new THREE.Bone();
		this.bone.name = "bone_" + num;
		this.bones.push(this.bone);

		// Check flags for LightWave 3d Export

		let zxy = BitStream.bitflag(bone.flag, 5);
		if(zxy) {
			console.error("ZXY FLAG SET!!!!");
		}

		// Update bone local transform matrix

		if (!BitStream.bitflag(bone.flag, 2)) {
			this.bone.scale.x = bone.scl.x;
			this.bone.scale.y = bone.scl.y;
			this.bone.scale.z = bone.scl.z;
		}

		if (!BitStream.bitflag(bone.flag, 1)) {
			
			if(!bone.rot.w) {

				const xRotMatrix = new THREE.Matrix4();
				xRotMatrix.makeRotationX(bone.rot.x);
				this.bone.applyMatrix(xRotMatrix);

				const yRotMatrix = new THREE.Matrix4();
				yRotMatrix.makeRotationY(bone.rot.y);
				this.bone.applyMatrix(yRotMatrix);

				const zRotMatrix = new THREE.Matrix4();
				zRotMatrix.makeRotationZ(bone.rot.z);
				this.bone.applyMatrix(zRotMatrix);

			} else {
				
				const { x, y, z, w } = bone.rot;
				const q = new THREE.Quaternion(x, y, z, w);
				const rotMatrix = new THREE.Matrix4();
				rotMatrix.makeRotationFromQuaternion(q);
				this.bone.applyMatrix(rotMatrix);

			}

		}

		if (!BitStream.bitflag(bone.flag, 0)) {
			this.bone.position.x = bone.pos.x;
			this.bone.position.y = bone.pos.y;
			this.bone.position.z = bone.pos.z;
		}

		this.bone.updateMatrix();
		this.bone.updateMatrixWorld();

		// If parent Bone exists, add bone as child

		if(parentBone) {
			parentBone.add(this.bone);
			this.bone.updateMatrix();
			this.bone.updateMatrixWorld();
		}

		// If polygon exists for bone, seek and read

		if(bone.chunkOfs) {
			this.bs.seekSet(bone.chunkOfs);
			let vertexOfs = this.bs.readUInt();
			let stripOfs = this.bs.readUInt();

			if(vertexOfs) {
				this.bs.seekSet(vertexOfs);
				this.readChunk();
			}

			if(stripOfs) {
				this.bs.seekSet(stripOfs);
				this.readChunk();
			}
		}

		// If child bone exists, read and pass in current bone

		if(bone.childOfs) {
			this.bs.seekSet(bone.childOfs);
			this.readBone(this.bone);
		}

		// If sibling bone exists, read and pass in parent bone

		if(bone.siblingOfs) {
			this.bs.seekSet(bone.siblingOfs);
			this.readBone(parentBone);
		}

	}

	readChunk() {

		const NJD_NULLOFF = 0x00;
		const NJD_BITSOFF = 0x01;
		const NJD_TINYOFF = 0x08;
		const NJD_MATOFF = 0x10;
		const NJD_VERTOFF = 0x20;
		const NJD_VOLOFF = 0x38;
		const NJD_STRIPOFF = 0x40;
		const NJD_ENDOFF = 0xFF;

		this.mat = {
			texId : -1,
			blending : false,
			doubleSide : false
		};

		let chunk;

		this.color = {
			r : 1,
			g : 1,
			b : 1,
			a : 1
		}

		do {

			chunk = {
				head : this.bs.readByte(),
				flag : this.bs.readByte()
			};

			// Invalid Chunk

			if (chunk.head > NJD_STRIPOFF + 11) {
				continue;
			}

			// Strip Chunk

			if(chunk.head >= NJD_STRIPOFF) {
				this.readStripChunk(chunk);
				continue;
			}

			// Volume Chunk

			if (chunk.head >= NJD_VOLOFF) {
				throw new Error("Volume chunk detected");
			}

			// Vertex Chunk

			if (chunk.head >= NJD_VERTOFF) {
				this.readVertexChunk(chunk);
				continue;
			}

			// Material Chunk

			if (chunk.head >= NJD_MATOFF) {
				this.readMaterialChunk(chunk);
				continue;
			}

			// Tiny Chunk

			if (chunk.head >= NJD_TINYOFF) {
				this.readTinyChunk(chunk);
				continue;
			}

			// Bits Chunk

			if(chunk.head >= NJD_BITSOFF) {
				this.readBitsChunk(chunk);
				continue;
			}

			// End

		} while(chunk.head !== NJD_ENDOFF);

		if(this.mem_stack && this.mem_stack.length) {
			let ofs = this.mem_stack.pop();
			this.bs.seekSet(ofs);
			this.readChunk();
		}

	}

	readBitsChunk(chunk) {

		switch (chunk.head) {
		case 1:
			let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
			let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);

			if(srcAlpha === 4 && dstAlpha === 1) {
				this.mat.blending = true;
			} else {
				this.mat.blending = false;
			}

			break;
		case 2:
			let mipmapDepth = chunk.flag & 0x0f;
			break;
		case 3:
			let specularCoef = chunk.flag & 0x1f;
			break;
		case 4:
			this.bs.storeOfs(chunk.flag);
			chunk.head = 0xff;
			break;
		case 5:
			
			this.mem_stack = this.mem_stack || [];
			this.mem_stack.push(this.bs.tell());
			this.bs.restoreOfs(chunk.flag);
			break;
		}

	}

	readTinyChunk(chunk) {
		
		let tinyChunk = this.bs.readUShort();
		chunk.textureId = tinyChunk & 0x1FFF;

		this.mat.texId = chunk.textureId;


		let superSample = BitStream.bitflag(tinyChunk, 13);
		let filterMode = BitStream.bitmask(tinyChunk, [14, 15]);

		let clampU = BitStream.bitflag(chunk.head, 4);
		let clampV = BitStream.bitflag(chunk.head, 5);
		let flipU = BitStream.bitflag(chunk.head, 6);
		let flipV = BitStream.bitflag(chunk.head, 7);

		//this.flipV = flipV;

	}

	readMaterialChunk(chunk) {

		let r, g, b, a;

		chunk.length = this.bs.readUShort();

		// Alpha Blending Instructions

		let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
		let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);

		if(srcAlpha === 4 && dstAlpha === 1) {
			this.mat.blending = true;
		} else {
			this.mat.blending = false;
		}

		let diffuse, specular, ambient, type;

		// Diffuse

		if (BitStream.bitflag(chunk.head, 0)) {
			this.color = {
				b : this.bs.readByte() / 255,
				g : this.bs.readByte() / 255,
				r : this.bs.readByte() / 255,
				a : this.bs.readByte() / 255,
			}
		}

		// Specular

		if (BitStream.bitflag(chunk.head, 1)) {
			//type = "phong";
			specular = this.bs.readColor();
			specular = null;
		}

		// Ambient

		if (BitStream.bitflag(chunk.head, 2)) {
			//type = type || "lambert";
			ambient = this.bs.readColor();
			ambient = null;
		}


	}

	readVertexChunk(chunk) {

		let r, g, b, a;

		chunk.length = this.bs.readUShort();

		// Read the index offset and the number of index

		let indexOfs = this.bs.readUShort();
		let nbIndex = this.bs.readUShort();

		// Read the vertex list

		for(let i = 0; i < nbIndex; i++) {

			let stackOfs = indexOfs + i;
			let vertex = new THREE.Vector3();

			// Read the position

			let pos = this.bs.readVec3();
			vertex.x = pos.x;
			vertex.y = pos.y;
			vertex.z = pos.z;
			vertex.applyMatrix4(this.bone.matrixWorld);

			// Read vertex normals

			if (chunk.head > 0x28 && chunk.head < 0x30) {
				let norm = this.bs.readVec3();
				let normal = new THREE.Vector3();
				normal.x = norm.x;
				normal.y = norm.y;
				normal.z = norm.z;
				//normal.applyMatrix3(this.bone.normalMatrix);
				vertex.normal = normal;
			}

			// Read vertex color

			if(chunk.head === 0x23 || chunk.head === 0x2a) {
				vertex.color = {
					b : this.bs.readByte() / 255,
					g : this.bs.readByte() / 255,
					r : this.bs.readByte() / 255,
					a : this.bs.readByte() / 255
				};
				//throw new Error("Model has vertex color don't use this");
			}

			// Read Vertex weight

			let skinWeights = new THREE.Vector4(0, 0, 0, 0);
			let skinIndices = new THREE.Vector4(0, 0, 0, 0);

			if(chunk.head !== 0x2c) {

				skinIndices.x = this.bones.length - 1;
				skinWeights.x = 1.0;

			} else {

				// Read weight values

				let ofs = this.bs.readUShort();
				let weight = this.bs.readUShort();

				// Update current stack position

				stackOfs = indexOfs + ofs;

				// Set the vertex weights

				// If a previous vertex exists, get values

				if(this.vertexStack[stackOfs]) {
					let prev = this.vertexStack[stackOfs];
					let keys = ['x', 'y', 'z'];
					keys.forEach(axis => {
						skinWeights[axis] = prev.skinWeight[axis];
						skinIndices[axis] = prev.skinIndice[axis];
					});
				}

				switch(chunk.flag) {
				case 0x80:
					skinIndices.x = this.bones.length - 1
					skinWeights.x = weight / 255;
					break;
				case 0x81:
					skinIndices.y = this.bones.length - 1
					skinWeights.y = weight / 255;
					break;
				case 0x82:
					skinIndices.z = this.bones.length - 1
					skinWeights.z = weight / 255;
					break;
				}

			}

			// If the global index is set, continue

			// Push the vertex to the stack

			vertex.globalIndex = this.v.length;
			vertex.skinWeight = skinWeights;
			vertex.skinIndice = skinIndices;

			this.vertexStack[stackOfs] = vertex;
			this.v.push(vertex);
		}


	}

	getMaterialIndex() {

		for(let i = 0; i < this.matList.length; i++) {

			if(this.mat.texId !== this.matList[i].texId) {
				continue;
			}
			
			if(this.mat.blending !== this.matList[i].blending) {
				continue;
			}
			
			if(this.mat.doubleSide !== this.matList[i].doubleSide) {
				continue;
			}

			return i;
		}

		let mat = {};
		for(let key in this.mat) {
			mat[key] = this.mat[key];
		}

		let matId = this.matList.length;
		this.matList.push(mat);
		return(matId);

	}

	readStripChunk(chunk) {
		
		chunk.length = this.bs.readUShort();

		// Read the number of strips and user offset

		let stripChunk = this.bs.readUShort();
		let nbStrips = stripChunk & 0x3FFF;
		let userOffset = BitStream.bitmask(stripChunk, [14, 15]);
		
		this.mat.doubleSide = BitStream.bitflag(chunk.flag, 4);
		let index = this.getMaterialIndex();

		// Read the list of strips

		for(let i = 0; i < nbStrips; i++) {

			// Read the length and direction

			let strip_length = this.bs.readShort();
			let clockwise = strip_length < 0 ? true : false;
			let length = Math.abs(strip_length);
			let strip = new Array(length);

			// Read the strip

			for (let k = 0; k < strip.length; k++) {

				// Read stack position

				let stackOfs = this.bs.readUShort();

				strip[k] = {
					vertex : this.vertexStack[stackOfs]
				};

				// Read face uv values

				switch (chunk.head) {
				case 0x41:

					if(!this.flipV) {
						strip[k].uv = new THREE.Vector2(
							this.bs.readShort() / 255,
							1 - this.bs.readShort() / 255
						);
					} else {
						strip[k].uv = new THREE.Vector2(
							this.bs.readShort() / 255,
							this.bs.readShort() / 255
						);
					}

					break;
				case 0x42:
					
					if(!this.flipV) {
						strip[k].uv = new THREE.Vector2(
							this.bs.readShort() / 1023,
							1 - this.bs.readShort() / 1023
						);
					} else {
						strip[k].uv = new THREE.Vector2(
							this.bs.readShort() / 1023,
							this.bs.readShort() / 1023
						);
					}

					break;
				default:

					strip[k].uv = new THREE.Vector2();

					break;
				}

				// Seek passed user offset

				if (userOffset && k > 1) {
					this.bs.seekCur(userOffset * 2);
				}

			}

			// Convert strips into faces

			for (let k = 0; k < strip.length - 2; k++) {

				let a, b, c;
				let aPos, bPos, cPos;
				let aClr, bClr, cClr;
				let aUv, bUv, cUv;
				let aIdx, bIdx, cIdx;
				let aWgt, bWgt, cWgt;

				if ((clockwise && !(k % 2)) || (!clockwise && k % 2)) {
					a = strip[k + 0];
					b = strip[k + 2];
					c = strip[k + 1];
				} else {
					a = strip[k + 0];
					b = strip[k + 1];
					c = strip[k + 2];
				}

				this.matIndex.push(index);

				// Positions

				aPos = a.vertex;
				bPos = b.vertex;
				cPos = c.vertex;

				this.vertices.push(aPos.x, aPos.y, aPos.z);
				this.vertices.push(bPos.x, bPos.y, bPos.z);
				this.vertices.push(cPos.x, cPos.y, cPos.z);

				// Colors

				aClr = aPos.color || this.color;
				bClr = bPos.color || this.color;
				cClr = cPos.color || this.color;
	
				if(aClr.a < 0.3) {
					aClr.a = 0.3;
				}

				this.vcolors.push(aClr.r, aClr.g, aClr.b, aClr.a);
				this.vcolors.push(bClr.r, bClr.g, bClr.b, bClr.a);
				this.vcolors.push(cClr.r, cClr.g, cClr.b, cClr.a);

				// UV Values

				this.uv.push(a.uv.x, a.uv.y);
				this.uv.push(b.uv.x, b.uv.y);
				this.uv.push(c.uv.x, c.uv.y);

				// Skin Index

				aIdx = aPos.skinIndice;
				bIdx = bPos.skinIndice;
				cIdx = cPos.skinIndice;

				this.skinIndex.push(aIdx.x, aIdx.y, aIdx.z, aIdx.w);
				this.skinIndex.push(bIdx.x, bIdx.y, bIdx.z, bIdx.w);
				this.skinIndex.push(cIdx.x, cIdx.y, cIdx.z, cIdx.w);

				// Skin Weight

				aWgt = aPos.skinWeight;
				bWgt = bPos.skinWeight;
				cWgt = cPos.skinWeight;

				this.skinWeight.push(aWgt.x, aWgt.y, aWgt.z, aWgt.w);
				this.skinWeight.push(bWgt.x, bWgt.y, bWgt.z, bWgt.w);
				this.skinWeight.push(cWgt.x, cWgt.y, cWgt.z, cWgt.w);

			}

		}


	}

	readAnim() {

		let motionOfs = this.bs.readUInt();
		let nbFrame = this.bs.readUInt();
		let motionType = this.bs.readUShort();
		let motionFlag = this.bs.readUShort();

		let nbElements = motionFlag & 0x0F;

		let motionList = new Array(this.bones.length);

		let motionTypes = {
			pos : BitStream.bitflag(motionType, 0),
			rot : BitStream.bitflag(motionType, 1),
			scl : BitStream.bitflag(motionType, 2),
			quat: motionType & 0x2000
		}

		this.bs.seekSet(motionOfs);

		// Read offsets to animation list for each bone

		let firstOfs = this.bs.length;
		for(let i = 0; i < this.bones.length; i++){

			let motionEntry = {
				bone : i,
				parent : i - 1,
				frames : []
			};

			if(this.bs.tell() === firstOfs) {
				motionList[i] = motionEntry;
				continue;
				// break;
			}

			// Read the offset to each list
			
			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}
				
				const ofs = this.bs.readUInt()
				if(ofs && ofs < firstOfs) {
					firstOfs = ofs;
				};

				motionEntry[key] = { ofs };

			}

			// Read the number of entries for each list

			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}

				let num = this.bs.readUInt();

				if(num === 0) {
					delete motionEntry[key];
				} else {
					motionEntry[key].num = num;
				}

			}
			
			motionList[i] = motionEntry;

		}

		motionList.forEach(motion => {

			// Read Position

			if(motion.pos) {

				this.bs.seekSet(motion.pos.ofs);

				for(let i = 0; i < motion.pos.num; i++) {

					let frameNo = this.bs.readUInt();
					let pos = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].pos = pos;
				}

				delete motion.pos;
			}

			// Read Rotation

			if(motion.rot) {

				this.bs.seekSet(motion.rot.ofs);

				for(let i = 0; i < motion.rot.num; i++) {

					let frameNo = this.bs.readUInt();
					let rot = this.bs.readRot3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].rot = rot;
				}

				delete motion.rot;

			}

			if(motion.quat) {

				this.bs.seekSet(motion.quat.ofs);

				for(let i = 0; i < motion.quat.num; i++) {

					let frameNo = this.bs.readUInt();
					const w = this.bs.readFloat();
					const x = this.bs.readFloat();
					const y = this.bs.readFloat();
					const z = this.bs.readFloat();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].quat = [ x, y, z, w ];
				}

				delete motion.quat;

			}

			// Read Scale

			if(motion.scl) {

				this.bs.seekSet(motion.scl.ofs);

				for(let i = 0; i < motion.scl.num; i++) {

					let frameNo = this.bs.readUInt();
					let scl = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].scl = scl;
				}

				delete motion.scl;

			}

		});

		let animation = {
			name : this.bs.name.replace(".njm", ""),
			fps : 30,
			length : (nbFrame - 1) / 30,
			hierarchy : new Array(this.bones.length)
		}

		for(let i = 0; i < this.bones.length; i++) {

			let bone = this.bones[i];
			let motion = motionList[i];

			animation.hierarchy[i] = {
				parent : motion.parent,
				keys : []
			};

			for(let k = 0; k < nbFrame; k++) {

				let frame = motion.frames[k];

				if(frame && frame.pos) {
					let pos = frame.pos;
					frame.pos = [pos.x, pos.y, pos.z];
				}

				if(frame && frame.rot) {

					let obj = new THREE.Bone();

					var xRotMatrix = new THREE.Matrix4();
					xRotMatrix.makeRotationX(frame.rot.x);
					obj.applyMatrix(xRotMatrix);

					var yRotMatrix = new THREE.Matrix4();
					yRotMatrix.makeRotationY(frame.rot.y);
					obj.applyMatrix(yRotMatrix);

					var zRotMatrix = new THREE.Matrix4();
					zRotMatrix.makeRotationZ(frame.rot.z);
					obj.applyMatrix(zRotMatrix);

					let quat = new THREE.Quaternion();
					quat.setFromRotationMatrix(obj.matrix);
					frame.rot = quat.toArray();

				}

				if(frame && frame.quat) {
					frame.rot = frame.quat;
				}

				if(frame && frame.scl) {
					let scl = frame.scl;
					frame.scl = [scl.x, scl.y, scl.z];
				}

				if(k === 0 || k === nbFrame - 1) {

					frame = frame || {};

					if(!frame.pos) {
						frame.pos = bone.position.toArray();
					}

					if(!frame.rot) {
						frame.rot = bone.quaternion.toArray();
					}

					if(!frame.scl) {
						frame.scl = bone.scale.toArray();
					}

				}

				if(!frame) {
					continue;
				}

				frame.time = k / 30;

				animation.hierarchy[i].keys.push(frame);

			}

		}

		var clip = THREE.AnimationClip.parseAnimation(animation, this.bones);
		clip.optimize();
		this.animList.push(clip);

	}

	plyMotion (bs) {

		const PLYMOTION = [
			"000_A_HANDGUN_CAUTION WALK",
			"001_A_HANDGUN_TECHNIQUE",
			"002_A_HANDGUN_ATTACK 01",
			"003_A_HANDGUN_CAUTION WAIT",
			"004_A_HANDGUN_CAUTION GUARD",
			"005_A_HANDGUN_WALK",
			"006_A_HANDGUN_WAIT",
			"007_A_HANDGUN_BIG DAMAGE",
			"008_A_HANDGUN_STAND UP",
			"009_A_HANDGUN_PHOTON BLAST",
			"010_A_RIFLE_CAUTION WALK",
			"011_A_RIFLE_TECHNIQUE",
			"012_A_RIFLE_ATTACK 01",
			"013_A_RIFLE_CAUTION WAIT",
			"014_A_RIFLE_CAUTION GUARD",
			"015_A_RIFLE_WALK",
			"016_A_RIFLE_WAIT",
			"017_A_RIFLE_BIG DAMAGE",
			"018_A_RIFLE_STAND UP",
			"019_A_RIFLE_DAMAGE",
			"020_A_RIFLE_BUTTON",
			"021_A_RIFLE_RUN",
			"022_A_MECHGUN_CAUTION WALK",
			"023_A_MECHGUN_TECHNIQUE",
			"024_A_MECHGUN_ATTACK 01",
			"025_A_MECHGUN_CAUTION WAIT",
			"026_A_MECHGUN_CAUTION GUARD",
			"027_A_MECHGUN_WALK",
			"028_A_MECHGUN_WAIT",
			"029_A_MECHGUN_BIG DAMAGE",
			"030_A_MECHGUN_STAND UP",
			"031_A_MECHGUN_DAMAGE",
			"032_A_MECHGUN_PHOTON BLAST",
			"033_A_MECHGUN_RUN",
			"034_A_SHOT_CAUTION WALK",
			"035_A_SHOT_TECHNIQUE",
			"036_A_SHOT_ATTACK 01",
			"037_A_SHOT_CAUTION WAIT",
			"038_A_SHOT_CAUTION GUARD",
			"039_A_SHOT_WALK",
			"040_A_SHOT_WAIT",
			"041_A_SHOT_BIG DAMAGE",
			"042_A_SHOT_STAND UP",
			"043_A_SHOT_DAMAGE",
			"044_A_SHOT_PHOTON BLAST",
			"045_A_SHOT_RUN",
			"046_A_ROD_CAUTION WALK",
			"047_A_ROD_TECHNIQUE",
			"048_A_ROD_ATTACK 01",
			"049_A_ROD_ATTACK 02",
			"050_A_ROD_ATTACK 03",
			"051_A_ROD_CAUTION WAIT",
			"052_A_ROD_CAUTION GUARD",
			"053_A_ROD_WALK",
			"054_A_ROD_WAIT",
			"055_A_ROD_BIG DAMAGE",
			"056_A_ROD_STAND UP",
			"057_A_ROD_DAMAGE",
			"058_A_ROD_PHOTON BLAST",
			"059_A_WAND_CAUTION WALK",
			"060_A_WAND_TECHNIQUE",
			"061_A_WAND_ATTACK 01",
			"062_A_WAND_ATTACK 02",
			"063_A_WAND_ATTACK 03",
			"064_A_WAND_CAUTION WAIT",
			"065_A_WAND_CAUTION GUARD",
			"066_A_WAND_BIG DAMAGE",
			"067_A_WAND_STAND UP",
			"068_A_WAND_DAMAGE",
			"069_A_WAND_PHOTON BLAST",
			"070_A_SABER-KATANA-CANE_CAUTION WALK",
			"071_A_SABER-KATANA-CANE_TECHNIQUE",
			"072_A_SABER-KATANA-CANE_ATTACK 01",
			"073_A_SABER-KATANA-CANE_ATTACK 02",
			"074_A_SABER-KATANA-CANE_ATTACK 03",
			"075_A_SABER-KATANA-CANE_CAUTION WAIT",
			"076_A_SABER-KATANA-CANE_CAUTION GUARD",
			"077_A_SABER-KATANA-CANE_WALK",
			"078_A_SABER-KATANA-CANE_BIG DAMAGE",
			"079_A_SABER-KATANA-CANE_STAND UP",
			"080_A_SABER-KATANA-CANE_DAMAGE",
			"081_A_SABER-KATANA-CANE_BUTTON",
			"082_A_SABER-KATANA-CANE_PHOTON BLAST",
			"083_A_SABER-KATANA-CANE_RUN",
			"084_A_SWORD_CAUTION WALK",
			"085_A_SWORD_TECHNIQUE",
			"086_A_SWORD_ATTACK 01",
			"087_A_SWORD_ATTACK 02",
			"088_A_SWORD_ATTACK 03",
			"089_A_SWORD_CAUTION WAIT",
			"090_A_SWORD_CAUTION GUARD",
			"091_A_SWORD_WALK",
			"092_A_SWORD_BIG DAMAGE",
			"093_A_SWORD_STAND UP",
			"094_A_SWORD_DAMAGE",
			"095_A_SWORD_PHOTON BLAST",
			"096_A_DAGGER_CAUTION WALK",
			"097_A_DAGGER_TECHNIQUE",
			"098_A_DAGGER_ATTACK 01",
			"099_A_DAGGER_ATTACK 02",
			"100_A_DAGGER_ATTACK 03",
			"101_A_DAGGER_CAUTION WAIT",
			"102_A_DAGGER_CAUTION GUARD",
			"103_A_DAGGER_WALK",
			"104_A_DAGGER_BIG DAMAGE",
			"105_A_DAGGER_STAND UP",
			"106_A_DAGGER_DAMAGE",
			"107_A_DAGGER_PHOTON BLAST",
			"108_A_PARTISAN_CAUTION WALK",
			"109_A_PARTISAN_TECHNIQUE",
			"110_A_PARTISAN_ATTACK 01",
			"111_A_PARTISAN_ATTACK 02",
			"112_A_PARTISAN_ATTACK 03",
			"113_A_PARTISAN_CAUTION WAIT",
			"114_A_PARTISAN_CAUTION GUARD",
			"115_A_PARTISAN_WALK",
			"116_A_PARTISAN_BIG DAMAGE",
			"117_A_PARTISAN_STAND UP",
			"118_A_PARTISAN_DAMAGE",
			"119_A_PARTISAN_PHOTON BLAST",
			"120_A_PARTISAN_RUN",
			"121_A_SLICER_CAUTION WALK",
			"122_A_SLICER_ATTACK 01",
			"123_A_SLICER_ATTACK 02",
			"124_A_SLICER_ATTACK 03",
			"125_A_SLICER_CAUTION WAIT",
			"126_A_SLICER_CAUTION GUARD",
			"127_A_SLICER_WALK",
			"128_A_SLICER_WAIT",
			"129_A_SLICER_BIG DAMAGE",
			"130_A_SLICER_STAND UP",
			"131_A_SLICER_DAMAGE",
			"132_A_SLICER_PHOTON BLAST",
			"133_A_DOUBLE-SABER_CAUTION WALK",
			"134_A_DOUBLE-SABER_ATTACK 01",
			"135_A_DOUBLE-SABER_ATTACK 02",
			"136_A_DOUBLE-SABER_ATTACK 03",
			"137_A_DOUBLE-SABER_CAUTION WAIT",
			"138_A_CLAW_CAUTION WALK",
			"139_A_CLAW_ATTACK 01",
			"140_A_CLAW_ATTACK 02",
			"141_A_CLAW_ATTACK 03",
			"142_A_CLAW_CAUTION WAIT",
			"143_A_FIST_CAUTION WALK",
			"144_A_FIST_TECHNIQUE",
			"145_A_FIST_ATTACK 01",
			"146_A_FIST_ATTACK 02",
			"147_A_FIST_ATTACK 03",
			"148_A_FIST_CAUTION WAIT",
			"149_A_FIST_CAUTION GUARD",
			"150_A_FIST_WALK",
			"151_A_FIST_WAIT",
			"152_A_FIST_BIG DAMAGE",
			"153_A_FIST_STAND UP",
			"154_A_FIST_DAMAGE",
			"155_A_FIST_DIE",
			"156_A_FIST_PHOTON BLAST",
			"157_A_FIST_RUN",
			"158_B_HANDGUN_CAUTION WALK",
			"159_B_HANDGUN_TECHNIQUE",
			"160_B_HANDGUN_ATTACK 01",
			"161_B_HANDGUN_CAUTION WAIT",
			"162_B_HANDGUN_CAUTION GUARD",
			"163_B_HANDGUN_WALK",
			"164_B_HANDGUN_BIG DAMAGE",
			"165_B_HANDGUN_STAND UP",
			"166_B_HANDGUN_DAMAGE",
			"167_B_HANDGUN_PHOTON BLAST",
			"168_B_RIFLE_CAUTION WALK",
			"169_B_RIFLE_TECHNIQUE",
			"170_B_RIFLE_ATTACK 01",
			"171_B_RIFLE_CAUTION WAIT",
			"172_B_RIFLE_CAUTION GUARD",
			"173_B_RIFLE_WALK",
			"174_B_RIFLE_BIG DAMAGE",
			"175_B_RIFLE_STAND UP",
			"176_B_RIFLE_DAMAGE",
			"177_B_RIFLE_BUTTON",
			"178_B_RIFLE_PHOTON BLAST",
			"179_B_RIFLE_RUN",
			"180_B_MECHGUN_CAUTION WALK",
			"181_B_MECHGUN_ATTACK 01",
			"182_B_MECHGUN_CAUTION WAIT",
			"183_B_MECHGUN_CAUTION GUARD",
			"184_B_MECHGUN_WALK",
			"185_B_MECHGUN_BIG DAMAGE",
			"186_B_MECHGUN_STAND UP",
			"187_B_MECHGUN_DAMAGE",
			"188_B_MECHGUN_RUN",
			"189_B_SHOT_CAUTION WALK",
			"190_B_SHOT_TECHNIQUE",
			"191_B_SHOT_ATTACK 01",
			"192_B_SHOT_CAUTION WAIT",
			"193_B_SHOT_CAUTION GUARD",
			"194_B_SHOT_WALK",
			"195_B_SHOT_WAIT",
			"196_B_SHOT_BIG DAMAGE",
			"197_B_SHOT_STAND UP",
			"198_B_SHOT_DAMAGE",
			"199_B_SHOT_PHOTON BLAST",
			"200_B_SHOT_RUN",
			"201_B_ROD_CAUTION WALK",
			"202_B_ROD_TECHNIQUE",
			"203_B_ROD_ATTACK 01",
			"204_B_ROD_ATTACK 02",
			"205_B_ROD_ATTACK 03",
			"206_B_ROD_CAUTION WAIT",
			"207_B_ROD_CAUTION GUARD",
			"208_B_ROD_WALK",
			"209_B_ROD_WAIT",
			"210_B_ROD_PHOTON BLAST",
			"211_B_WAND_CAUTION WALK",
			"212_B_WAND_TECHNIQUE",
			"213_B_WAND_CAUTION GUARD",
			"214_B_WAND_WALK",
			"215_B_WAND_BIG DAMAGE",
			"216_B_WAND_STAND UP",
			"217_B_WAND_DAMAGE",
			"218_B_WAND_PHOTON BLAST",
			"219_B_SABER-KATANA-CANE_CAUTION WALK",
			"220_B_SABER-KATANA-CANE_TECHNIQUE",
			"221_B_SABER-KATANA-CANE_ATTACK 01",
			"222_B_SABER-KATANA-CANE_ATTACK 02",
			"223_B_SABER-KATANA-CANE_ATTACK 03",
			"224_B_SABER-KATANA-CANE_CAUTION WAIT",
			"225_B_SABER-KATANA-CANE_CAUTION GUARD",
			"226_B_SABER-KATANA-CANE_WALK",
			"227_B_SABER-KATANA-CANE_WAIT",
			"228_B_SABER-KATANA-CANE_BIG DAMAGE",
			"229_B_SABER-KATANA-CANE_STAND UP",
			"230_B_SABER-KATANA-CANE_DAMAGE",
			"231_B_SABER-KATANA-CANE_BUTTON",
			"232_B_SABER-KATANA-CANE_PHOTON BLAST",
			"233_B_SABER-KATANA-CANE_RUN",
			"234_B_SWORD_CAUTION WALK",
			"235_B_SWORD_TECHNIQUE",
			"236_B_SWORD_CAUTION GUARD",
			"237_B_SWORD_WALK",
			"238_B_SWORD_BIG DAMAGE",
			"239_B_SWORD_STAND UP",
			"240_B_SWORD_DAMAGE",
			"241_B_SWORD_PHOTON BLAST",
			"242_B_DAGGER_CAUTION WALK",
			"243_B_DAGGER_TECHNIQUE",
			"244_B_DAGGER_CAUTION GUARD",
			"245_B_DAGGER_BIG DAMAGE",
			"246_B_DAGGER_STAND UP",
			"247_B_DAGGER_DAMAGE",
			"248_B_DAGGER_PHOTON BLAST",
			"249_B_PARTISAN_CAUTION WALK",
			"250_B_PARTISAN_DAMAGE",
			"251_B_PARTISAN_WALK",
			"252_B_PARTISAN_RUN",
			"253_B_SLICER_CAUTION WALK",
			"254_B_SLICER_WALK",
			"255_B_DOUBLE-SABER_CAUTION WALK",
			"256_B_DOUBLE-SABER_ATTACK 01",
			"257_B_DOUBLE-SABER_ATTACK 02",
			"258_B_DOUBLE-SABER_ATTACK 03",
			"259_B_DOUBLE-SABER_CAUTION WAIT",
			"260_B_CLAW_CAUTION WALK",
			"261_B_CLAW_ATTACK 01",
			"262_B_CLAW_ATTACK 02",
			"263_B_CLAW_ATTACK 03",
			"264_B_CLAW_CAUTION WAIT",
			"265_B_FIST_CAUTION WALK",
			"266_B_FIST_TECHNIQUE",
			"267_B_FIST_ATTACK 01",
			"268_B_FIST_ATTACK 02",
			"269_B_FIST_ATTACK 03",
			"270_B_FIST_CAUTION WAIT",
			"271_B_FIST_CAUTION GUARD",
			"272_B_FIST_WALK",
			"273_B_FIST_DAMAGE",
			"274_B_FIST_DIE",
			"275_B_FIST_PHOTON BLAST",
			"276_B_FIST_RUN",
			"277_C_HANDGUN_CAUTION WALK",
			"278_C_HANDGUN_ATTACK 01",
			"279_C_HANDGUN_CAUTION WAIT",
			"280_C_MECHGUN_CAUTION WALK",
			"281_C_MECHGUN_TECHNIQUE",
			"282_C_MECHGUN_ATTACK 01",
			"283_C_MECHGUN_CAUTION WAIT",
			"284_C_MECHGUN_CAUTION GUARD",
			"285_C_MECHGUN_WALK",
			"286_C_MECHGUN_BIG DAMAGE",
			"287_C_MECHGUN_STAND UP",
			"288_C_MECHGUN_DAMAGE",
			"289_C_MECHGUN_PHOTON BLAST",
			"290_C_MECHGUN_RUN",
			"291_C_ROD_CAUTION WALK",
			"292_C_ROD_TECHNIQUE",
			"293_C_ROD_ATTACK 01",
			"294_C_ROD_ATTACK 02",
			"295_C_ROD_ATTACK 03",
			"296_C_ROD_CAUTION WAIT",
			"297_C_ROD_CAUTION GUARD",
			"298_C_ROD_WALK",
			"299_C_ROD_WAIT",
			"300_C_ROD_BIG DAMAGE",
			"301_C_ROD_STAND UP",
			"302_C_ROD_DAMAGE",
			"303_C_ROD_PHOTON BLAST",
			"304_C_WAND_CAUTION WALK",
			"305_C_WAND_TECHNIQUE",
			"306_C_WAND_ATTACK 01",
			"307_C_WAND_ATTACK 02",
			"308_C_WAND_ATTACK 03",
			"309_C_WAND_CAUTION WAIT",
			"310_C_WAND_PHOTON BLAST",
			"311_C_SABER-KATANA-CANE_CAUTION WALK",
			"312_C_SABER-KATANA-CANE_TECHNIQUE",
			"313_C_SABER-KATANA-CANE_ATTACK 01",
			"314_C_SABER-KATANA-CANE_ATTACK 02",
			"315_C_SABER-KATANA-CANE_ATTACK 03",
			"316_C_SABER-KATANA-CANE_CAUTION WAIT",
			"317_C_SABER-KATANA-CANE_CAUTION GUARD",
			"318_C_SABER-KATANA-CANE_WAIT",
			"319_C_SABER-KATANA-CANE_BIG DAMAGE",
			"320_C_SABER-KATANA-CANE_STAND UP",
			"321_C_SABER-KATANA-CANE_DAMAGE",
			"322_C_SABER-KATANA-CANE_DIE",
			"323_C_SABER-KATANA-CANE_PHOTON BLAST",
			"324_C_SABER-KATANA-CANE_RUN",
			"325_C_SLICER_CAUTION WALK",
			"326_C_SLICER_ATTACK 01",
			"327_C_SLICER_ATTACK 02",
			"328_C_SLICER_ATTACK 03",
			"329_C_SLICER_CAUTION WAIT",
			"330_C_FIST_CAUTION WALK",
			"331_C_FIST_TECHNIQUE",
			"332_C_FIST_ATTACK 01",
			"333_C_FIST_ATTACK 02",
			"334_C_FIST_ATTACK 03",
			"335_C_FIST_CAUTION WAIT",
			"336_C_FIST_CAUTION GUARD",
			"337_C_FIST_WALK",
			"338_C_FIST_PHOTON BLAST",
			"339_C_FIST_RUN"
		];

		const NEEDLE = 0x20003;
		this.bs = bs;

		let stack = [];

		do {

			let num = this.bs.readUInt();
			if(num !== NEEDLE) {
				continue;
			}

			stack.push(this.bs.tell() - 12);

		} while(this.bs.tell() < this.bs.len());

		for(let i = 0; i < stack.length; i++) {

			this.bs.name = PLYMOTION[i];
			this.bs.seekSet(stack[i]);
			this.readAnim();

		}

	}


	psobbMotion(list) {
		
		for(let i = 0; i < list.length; i++) {
			
			let bs = list[i];

			bs.seekSet(0);

	        const NEEDLE = 0x20003;
        	this.bs = bs;
			
    	    do {

           		let num = this.bs.readUInt();
       		   	if(num !== NEEDLE) {
           	    	continue;
            	}

            	this.bs.seekCur(-12);
				break;

			} while(this.bs.tell() < this.bs.len());

			
			if(i === 428) {
				bs.seekSet(0x14e0);
			}

			this.bs = bs;
			try {
				this.readPsobbAnim();
			} catch(err) {
				console.log("%d could not parse", i);
			}
			
		}

	}

	readPsobbAnim() {

		let motionOfs = this.bs.readUInt();
		let nbFrame = this.bs.readUInt();
		let motionType = this.bs.readUShort();
		let motionFlag = this.bs.readUShort();

		let nbElements = motionFlag & 0x0F;

		let motionList = new Array(this.bones.length);

		let motionTypes = {
			pos : BitStream.bitflag(motionType, 0),
			rot : BitStream.bitflag(motionType, 1),
			scl : BitStream.bitflag(motionType, 2)
		}

		this.bs.seekSet(motionOfs);

		// Read offsets to animation list for each bone

		for(let i = 0; i < this.bones.length; i++){

			let motionEntry = {
				bone : i,
				parent : i - 1,
				frames : []
			};

			// Read the offset to each list

			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}

				motionEntry[key] = {
					ofs : this.bs.readUInt()
				};

			}

			// Read the number of entries for each list

			for(let key in motionTypes) {

				if(!motionTypes[key]) {
					continue;
				}

				let num =  this.bs.readUInt();

				if(num === 0) {
					delete motionEntry[key];
				} else {
					motionEntry[key].num = num;
				}

			}

			motionList[i] = motionEntry;

		}


		motionList.forEach(motion => {

			// Read Position

			if(motion.pos) {

				this.bs.seekSet(motion.pos.ofs);

				for(let i = 0; i < motion.pos.num; i++) {

					let frameNo = this.bs.readUInt();
					let pos = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].pos = pos;
				}

				delete motion.pos;
			}

			// Read Rotation

			if(motion.rot) {

				this.bs.seekSet(motion.rot.ofs);

				for(let i = 0; i < motion.rot.num; i++) {

					let frameNo = this.bs.readUShort();
					let rot = this.bs.readShortRot3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].rot = rot;
				}

				delete motion.rot;

			}

			// Read Scale

			if(motion.scl) {

				this.bs.seekSet(motion.scl.ofs);

				for(let i = 0; i < motion.scl.num; i++) {

					let frameNo = this.bs.readUInt();
					let scl = this.bs.readVec3();

					if(!motion.frames[frameNo]) {
						motion.frames[frameNo] = {};
					}

					motion.frames[frameNo].scl = scl;
				}

				delete motion.scl;

			}

		});

		let animation = {
			name : this.bs.name.replace(".njm", ""),
			fps : 30,
			length : (nbFrame - 1) / 30,
			hierarchy : new Array(this.bones.length)
		}

		for(let i = 0; i < this.bones.length; i++) {

			let bone = this.bones[i];
			let motion = motionList[i];

			animation.hierarchy[i] = {
				parent : motion.parent,
				keys : []
			};

			for(let k = 0; k < nbFrame; k++) {

				let frame = motion.frames[k];

				if(frame && frame.pos) {
					let pos = frame.pos;
					frame.pos = [pos.x, pos.y, pos.z];
				}

				if(frame && frame.rot) {

					let obj = new THREE.Bone();

					var xRotMatrix = new THREE.Matrix4();
					xRotMatrix.makeRotationX(frame.rot.x);
					obj.applyMatrix(xRotMatrix);

					var yRotMatrix = new THREE.Matrix4();
					yRotMatrix.makeRotationY(frame.rot.y);
					obj.applyMatrix(yRotMatrix);

					var zRotMatrix = new THREE.Matrix4();
					zRotMatrix.makeRotationZ(frame.rot.z);
					obj.applyMatrix(zRotMatrix);

					let quat = new THREE.Quaternion();
					quat.setFromRotationMatrix(obj.matrix);
					frame.rot = quat.toArray();


				}

				if(frame && frame.scl) {
					let scl = frame.scl;
					frame.scl = [scl.x, scl.y, scl.z];
				}

				if(k === 0 || k === nbFrame - 1) {

					frame = frame || {};

					if(!frame.pos) {
						frame.pos = bone.position.toArray();
					}

					if(!frame.rot) {
						frame.rot = bone.quaternion.toArray();
					}

					if(!frame.scl) {
						frame.scl = bone.scale.toArray();
					}

				}

				if(!frame) {
					continue;
				}

				frame.time = k / 30;

				animation.hierarchy[i].keys.push(frame);

			}

		}

		var clip = THREE.AnimationClip.parseAnimation(animation, this.bones);
		clip.optimize();
		this.animList.push(clip);

	}


}
