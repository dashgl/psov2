/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


"use strict";

const store = localforage.createInstance({
	name: "NinjaPlugin"
});

const NinjaFile = (function() {

	this.MEM = {};

	this.API = {
		load : api_load.bind(this),
		gsl : api_gsl.bind(this),
		bml : api_bml.bind(this),
		afs : api_afs.bind(this),
		prs : api_prs.bind(this),
		prc : api_prc.bind(this),
		rlc : api_rlc.bind(this)
	};

	return this;

	async function api_load(file) {

		const url = `dat/${file.toLowerCase()}`;
		console.log(url);

		const asset = await localforage.getItem(url);
		if(asset) {
			return new BitStream(file, asset);
		}

		const req = await fetch(url);
		const res = await req.arrayBuffer();
		await localforage.setItem(url, res);
		return new BitStream(file, res);

	}

	function api_gsl(bs) {

        let file;
        let file_list = [];

        do {

            file = {
                name : bs.readString(0x20),
                offset : bs.readUInt() * 2048,
                length : bs.readUInt()
            };

            bs.seekCur(8);

            file_list.push(file);

        } while(file.name.length);

        file_list.pop();

        for(let i = 0; i < file_list.length; i++) {
            bs.seekSet(file_list[i].offset);
            let data = bs.copy(file_list[i].length);
            file_list[i] = new BitStream(file_list[i].name, data);
        }

        let ark = {};
        file_list.forEach(file => {
            ark[file.name] = file;
        });

        return ark;

	}

	function api_bml(bs) {

        bs.seekSet(4);
        let count = bs.readUInt();
        let file_list = new Array(count);
        let asset_list = [];

        // Read the BML Archive header
		
        bs.seekSet(0x40);
        for(let i = 0; i < file_list.length; i++) {
            file_list[i] = {
                name : bs.readString(0x20),
                len : bs.readUInt(),
                type : bs.readUInt(),
                unpacked : bs.readUInt(),
                pvm : bs.readUInt()
            };

            bs.seekCur(0x10);
        }

        // Seek to the the start of the files

        let ofs = bs.tell();
        if (ofs < 0x800) {
            ofs = 0x800;
        } else {
            ofs = (0xFFFFF800 & ofs) + 0x800;
        }
        bs.seekSet(ofs);

        // Read the files

        for(let i = 0; i < file_list.length; i++) {

            let raw = bs.copy(file_list[i].len);
			let data = this.API.prs(raw);
            asset_list.push(new BitStream(file_list[i].name, data));
            bs.seekNext();

            if(!file_list[i].pvm) {
                continue;
            }

            let name = file_list[i].name.split(".");
            name[1] = "pvm";

            raw = bs.copy(file_list[i].pvm);
            data = this.API.prs(raw);
            asset_list.push(new BitStream(name.join("."), data));
            bs.seekNext();

        }

        let ark = {};
        asset_list.forEach(file => {
            ark[file.name] = file;
        });

        return ark;

	}

	function api_afs(bs, skipPrs) {

        bs.seekSet(4);
        let count = bs.readUInt();

        let header = new Array(count);
        for(let i = 0; i < header.length; i++) {
            header[i] = {
                offset : bs.readUInt(),
                length : bs.readUInt()
            }
        }

        let base = bs.name.split(".")[0];
        let asset_list = new Array(count);

        for(let i = 0; i < header.length; i++) {

            bs.seekSet(header[i].offset);

            let raw = bs.copy(header[i].length);
            if(!skipPrs) {
                raw = this.API.prs(raw);
            }

            let str = i.toString();
            while(str.length < 3) {
                str = "0" + str;
            }
            asset_list[i] = new BitStream(base + "_" + str, raw);

        }

        return asset_list;

	}

	function api_prs(raw) {

        var self = {
            ibuf: new Uint8Array(raw),
            obuf: [],
            iofs: 0,
            bit: 0,
            cmd: 0,
            getByte : function() {
                var val = self.ibuf[self.iofs];
                self.iofs += 1;
                return parseInt(val);
            },
            getBit : function() {
                if (self.bit == 0) {
                    self.cmd = self.getByte();
                    self.bit = 8;
                }
                var bit = self.cmd & 1;
                self.cmd >>= 1;
                self.bit -= 1;
                return parseInt(bit);
            }
        };

        var t, a, b, j, cmd, offset, amount, start;

        while (self.iofs < self.ibuf.length) {
            cmd = self.getBit();

            if (cmd) {
                self.obuf.push(self.ibuf[self.iofs]);
                self.iofs += 1;
                continue;
            }

            t = self.getBit();

            if (t) {

                a = self.getByte();
                b = self.getByte();

                offset = ((b << 8) | a) >> 3;
                amount = a & 7;

                if (self.iofs < self.ibuf.length) {
                    if (amount == 0) {
                        amount = self.getByte() + 1;
                    } else {
                        amount += 2;
                    }
                }

                start = self.obuf.length - 0x2000 + offset;

            } else {

                amount = 0;

                for (j = 0; j < 2; j++) {
                    amount <<= 1;
                    amount |= self.getBit();
                }

                offset = self.getByte();
                amount += 2;

                start = self.obuf.length - 0x100 + offset;

            }

            for (j = 0; j < amount; j++) {

                if (start < 0) {
                    self.obuf.push(0);
                } else if (start < self.obuf.length) {
                    self.obuf.push(self.obuf[start]);
                } else {
                    self.obuf.push(0);
                }

                start += 1;
            }

        }

        return new Uint8Array(self.obuf).buffer;

	}

	function api_prc(bs) {

        let ctx = {
            stream : new Uint32Array(56),
            key : null,
            pos : null
        }

        let buf = [];
        let buffer = new Uint8Array(bs.data.buffer)
        for (var i = 0; i < 8; i++) {
            buf[i] = buffer[i];
        }

        buffer = buffer.slice(8);
        let unc_len = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
        let key = buf[4] | (buf[5] << 8) | (buf[6] << 16) | (buf[7] << 24);

        ctx.stream[55] = key;
        ctx.key = key;

        let idx;
        let tmp = 1;

        for (let i = 0x15; i <= 0x46E; i += 0x15) {
            idx = i % 55;
            key -= tmp;
            ctx.stream[idx] = tmp;
            tmp = key;
            key = ctx.stream[idx];
        }

        mix_stream(ctx);
        mix_stream(ctx);
        mix_stream(ctx);
        mix_stream(ctx);

        ctx.pos = 56;

        let pos, dword;
        let len = buffer.length + 3;
        len = len & 0xFFFFFFFC;
        let view = new DataView(buffer.buffer);

        pos = 0;
        while (len > 0) {
            dword = view.getUint32(pos, true);
            tmp = crypt_dword(ctx, dword);
            view.setUint32(pos, tmp, true);
            pos += 4;
            len -= 4;
        }

        let dst = this.API.prs(buffer.buffer);
        return new BitStream(bs.name, dst);

        function mix_stream(ctx) {
            let ptr;

            ptr = 1;
            for (let i = 24; i; --i, ++ptr) {
                ctx.stream[ptr] -= ctx.stream[ptr + 31];
            }

            ptr = 25;
            for (let i = 31; i; --i, ++ptr) {
                ctx.stream[ptr] -= ctx.stream[ptr - 24];
            }
        }

        function crypt_dword(ctx, data) {

            if (ctx.pos === 56) {
                mix_stream(ctx);
                ctx.pos = 1;
            }

            var res = new Uint32Array(1);
            res[0] = data ^ ctx.stream[ctx.pos++];

            return res[0];
        }

	}

	function api_rlc(bs) {
		
		bs.setBigEndian();

		const header = {
			name : bs.readString(0x10),
			count : bs.readUInt(),
			offset : bs.readUInt(),
			end : bs.readUInt(),
			nop : bs.readUInt()
		};
		
		bs.seekSet(header.offset);
		const files = new Array(header.count);

		let base = bs.name.split(".").shift();
		console.log(base);

		for(let i = 0; i < header.count; i++) {
			
			let entry = {
				offset : bs.readUInt(),
				length : bs.readUInt()
			};
			
			bs.storeOfs("table");
			bs.seekSet(entry.offset);

			let bytes = bs.copy(entry.length);
			let num = i.toString();
			while(num.length < 3) {
				num = "0" + num;
			}

			if(entry.length % 4) {
				
				let diff = 4 - (entry.length % 4);
				let len = entry.length + diff;
				let buffer = new ArrayBuffer(len);
				let src = new Uint8Array(bytes);
				let dst = new Uint8Array(buffer);

				for(let i = 0; i < src.length; i++) {
					dst[i] = src[i];
				}
				
				bytes = buffer;

			}

			let cmp = new BitStream(base + "_" + num, bytes);
			files[i] = this.API.prc(cmp);

			bs.restoreOfs("table");

		}
		
		return files;

	}


}).apply({});
