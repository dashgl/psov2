/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

THREE.DashExporter = function() {
			
	this.blocks = [];
	this.textures = [];
	this.materials = [];
	this.vertices = [];
	this.indexLookup = [];
	this.faces = [];
	this.bones = [];
	this.animations = [];

}

THREE.DashExporter.prototype = {

	constructor: THREE.DashExporter,

	parse : function(input) {

		const DASH_MAGIC_NUMBER        = 0x48534144;
		const DASH_MAJOR_VERSION             = 0x01;
		const DASH_MINOR_VERSION             = 0x05;
		const DASH_MIME_TYPE = { type : 'application/octet-stream' };

		// Header Label

		const DASH_LABEL_TEX           = 0x00786574;
		const DASH_LABEL_MAT           = 0x0074616D;
		const DASH_LABEL_VERT          = 0x74726576;
		const DASH_LABEL_FACE          = 0x65636166;
		const DASH_LABEL_BONE          = 0x656E6F62;
		const DASH_LABEL_ANIM          = 0x6D696E61;

		// Check type or initialize

		if(["Mesh", "SkinnedMesh"].indexOf(input.type) === -1) {
			throw new Error("Invalid input type: " + input.type);
		}
		
		// Initialize Header

		this.offset = 0x70;
		let buffer = new ArrayBuffer(0x70);
		this.header = new DataView(buffer);
		this.header.setUint32(0x00, DASH_MAGIC_NUMBER, true);
		this.header.setUint16(0x04, DASH_MAJOR_VERSION, true);
		this.header.setUint16(0x06, DASH_MINOR_VERSION, true);
		this.header.setUint32(0x08, 0x10, true);
		this.header.setUint32(0x0c, 0x06, true);

		this.header.setUint32(0x10, DASH_LABEL_TEX, true);
		this.header.setUint32(0x20, DASH_LABEL_MAT, true);
		this.header.setUint32(0x30, DASH_LABEL_VERT, true);
		this.header.setUint32(0x40, DASH_LABEL_FACE, true);
		this.header.setUint32(0x50, DASH_LABEL_BONE, true);
		this.header.setUint32(0x60, DASH_LABEL_ANIM, true);

		this.blocks.push(buffer);

		// Read Properties (Materials)

		let materials = input.material;
		if(!materials) {
			materials = [];
		} else if(!Array.isArray(materials)) {
			materials = [ materials ];
		}

		this.readTextures(materials);
		this.readMaterials(materials);

		// Read Properties (Geometry)

		let geometry = input.geometry;

		if(!geometry.isBufferGeometry) {
			let buffer = new THREE.BufferGeometry();
			buffer.fromGeometry(geometry);
			geometry = buffer;
		}

		this.readVertices(geometry);
		this.readFaces(geometry);

		// Read Properties (Skeleton)

		this.readBones(input);
		this.readAnimations(input);

		// Generate and return blob
		
		return new Blob(this.blocks, DASH_MIME_TYPE);

	},

	readTextures : function(mats) {
		
		this.textureLookup = [];

		mats.forEach(mat => {
			
			if(!mat.map) {
				return;
			}

			if(!mat.map.image) {
				return;
			}
			
			let image = mat.map.image;
			
			let canvas = image;
			let width = canvas.width;
			let height = canvas.height;
			let data = canvas.toDataURL();
			let index = data.indexOf(",") + 1;
			data = data.substr(index);
			data = atob(data);
			
			let name = mat.map.name;
			if(!name || name === "") {
				let num = this.textures.length.toString();
				while(num.length < 3) {
					num = "0" + num;
				}
				name = "texture_" + num;
			}

			this.textures.push({
				id : this.textures.length,
				flipY : mat.map.flipY ? 1 : 0,
				width : width,
				height : height,
				wrapS : mat.map.wrapS - 1000,
				wrapT : mat.map.wrapT - 1000,
				data : data,
				name : name
			});
			
			this.textureLookup.push(mat.map.uuid);

		});
		
		if(!this.textures.length) {
			return;
		}

		let byteLen = 0x40 * this.textures.length;
		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x18, this.offset, true);
		this.header.setUint32(0x1c, this.textures.length, true);
		this.blocks.push(buffer);

		let ofs = 0;
		this.offset += byteLen;

		this.textures.forEach(img => {
			
			let len = img.data.length;
			let round = len % 16;
			if(round) {
				len += 16 - round;
			}
			
			for(let i = 0; i < img.name.length && i < 0x1f; i++) {
				view.setUint8(ofs + i, img.name.charCodeAt(i));
			}

			view.setInt16(ofs + 0x20, img.id, true);
			view.setInt16(ofs + 0x22, img.flipY, true);
			view.setUint32(ofs + 0x24, this.offset, true);
			view.setUint32(ofs + 0x28, img.data.length, true);
			view.setUint16(ofs + 0x2c, img.width, true);
			view.setUint16(ofs + 0x2e, img.height, true);
			view.setUint16(ofs + 0x30, img.wrapS, true);
			view.setUint16(ofs + 0x32, img.wrapT, true);
			
			let buffer = new ArrayBuffer(len);
			let imgView = new DataView(buffer);

			for(let i = 0; i < img.data.length; i++) {
				imgView.setUint8(i, img.data.charCodeAt(i));
			}
			
			this.offset += len;
			this.blocks.push(buffer);
			ofs += 0x40;

		});

		// End Read Images

	},

	readMaterials : function(mats) {
			
		const DASH_MATERIAL_BASIC               = 0;
		const DASH_MATERIAL_LAMBERT             = 1;
		const DASH_MATERIAL_PHONG               = 2;

		mats.forEach(mat => {
			
			let type = 0;

			switch(mat.type) {
			case "MeshBasicMaterial":
				type = DASH_MATERIAL_BASIC;
				break;
			case "MeshLambertMaterial":
				type = DASH_MATERIAL_LAMBERT;
				break;
			case "MeshPhongMaterial":
				type = DASH_MATERIAL_PHONG;
				break;
			default:
				console.warn("Use Basic, Lambert or Phong for best compatibility");
				console.warn("The exporter will attempt to export as phong");
				type = 2;
				break;
			}

			let name = mat.name;
			if(!name || name === "") {
				let num = this.materials.length.toString();
				while(num.length < 3) {
					num = "0" + num;
				}
				name = "material_" + num;
			}

			if(!mat.color) {
				mat.color = {
					r : 1,
					g : 1,
					b : 1,
					opacity : 1
				}
				mat.isMeshBasicMaterial = true;
			};

			let material = {
				id : this.materials.length,
				textureId : -1,
				name : name,
				blending : {
					use : mat.blending ? 1 : 0,
					equation : mat.blendEquation,
					src : mat.blendSrc,
					dst : mat.blendDst 
				},
				skinning : mat.skinning ? 1 : 0,
				shader_type : type,
				ignore_light : mat.lights ? 0 : 1,
				render_side : mat.side,
				shadow_side : mat.shadowSide,
				use_alpha : mat.transparent ? 1 : 0,
				vertex_color : mat.vertexColors,
				skip_render : mat.visible ? 0 : 1,
				alphaTest : mat.alphaTest,
				diffuse : [
					mat.color.r,
					mat.color.b,
					mat.color.g,
					mat.opacity
				],
				emissive : [ 0, 0, 0, 1 ],
				specular: [ 0, 0, 0, 30 ]
			};
			
			if(mat.emissive) {
				material.emissive[0] = mat.emissive.r;
				material.emissive[1] = mat.emissive.g;
				material.emissive[2] = mat.emissive.b;
				material.emissive[3] = mat.emissiveIntensity;
			}

			if(mat.specular) {
				material.specular[0] = mat.specular.r;
				material.specular[1] = mat.specular.g;
				material.specular[2] = mat.specular.b;
				material.specular[3] = mat.shininess;
			}
			
			if(mat.map) {
				material.textureId = this.textureLookup.indexOf(mat.map.uuid);
			}

			this.materials.push(material);

		});
		
		delete this.textureLookup;
		if(!this.materials.length) {
			return;
		}

		let byteLen = 0x70 * this.materials.length;
		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x28, this.offset, true);
		this.header.setUint32(0x2c, this.materials.length, true);
		this.blocks.push(buffer);
		this.offset += byteLen;

		let ofs = 0;
		this.materials.forEach(mat => {
			
			for(let i = 0; i < mat.name.length && i < 0x1f; i++) {
				view.setUint8(ofs + i, mat.name.charCodeAt(i));
			}

			view.setInt16(ofs + 0x20, mat.id, true);
			view.setInt16(ofs + 0x22, mat.textureId, true);

			view.setUint16(ofs + 0x24, mat.blending.use, true);
			view.setUint16(ofs + 0x26, mat.blending.equation, true);
			view.setUint16(ofs + 0x28, mat.blending.src, true);
			view.setUint16(ofs + 0x2a, mat.blending.dst, true);
			
			view.setUint16(ofs + 0x2c, mat.skinning, true);
			view.setUint16(ofs + 0x2e, mat.shader_type, true);
			
			view.setUint16(ofs + 0x30, mat.render_side, true);
			view.setUint16(ofs + 0x32, mat.shadow_side, true);
			
			view.setUint16(ofs + 0x34, mat.ignore_light, true);
			view.setUint16(ofs + 0x36, mat.vertex_color, true);
			
			view.setUint16(ofs + 0x38, mat.use_alpha, true);
			view.setUint16(ofs + 0x3a, mat.skip_render, true);
			
			view.setFloat32(ofs + 0x3c, mat.alphaTest, true);
			
			view.setFloat32(ofs + 0x40, mat.diffuse[0], true);
			view.setFloat32(ofs + 0x44, mat.diffuse[1], true);
			view.setFloat32(ofs + 0x48, mat.diffuse[2], true);
			view.setFloat32(ofs + 0x4c, mat.diffuse[3], true);
			
			view.setFloat32(ofs + 0x50, mat.emissive[0], true);
			view.setFloat32(ofs + 0x54, mat.emissive[1], true);
			view.setFloat32(ofs + 0x58, mat.emissive[2], true);
			view.setFloat32(ofs + 0x5c, mat.emissive[3], true);
			
			view.setFloat32(ofs + 0x60, mat.specular[0], true);
			view.setFloat32(ofs + 0x64, mat.specular[1], true);
			view.setFloat32(ofs + 0x68, mat.specular[2], true);
			view.setFloat32(ofs + 0x6c, mat.specular[3], true);
			
			ofs += 0x70;
		});

	},

	readVertices : function(geometry) {

		let position = geometry.attributes.position;
		let skinIndex = geometry.attributes.skinIndex;
		let skinWeight = geometry.attributes.skinWeight;
		
		let vertexKeys = {};

		for(let i = 0; i < position.count; i++) {
			
			let vertex = {
				position : new Float32Array([0, 0, 0]),
				skinIndex : new Uint16Array([0, 0, 0, 0]),
				skinWeight : new Float32Array([0, 0, 0, 0])
			}

			// Set Position

			vertex.position[0] = position.array[i*3 + 0];
			vertex.position[1] = position.array[i*3 + 1];
			vertex.position[2] = position.array[i*3 + 2];
			
			// Set index
			
			if(skinIndex) {
				vertex.skinIndex[0] = skinIndex.array[i*4 + 0];
				vertex.skinIndex[1] = skinIndex.array[i*4 + 1];
				vertex.skinIndex[2] = skinIndex.array[i*4 + 2];
				vertex.skinIndex[3] = skinIndex.array[i*4 + 3];
			}
			
			// Set weight

			if(skinWeight) {
				vertex.skinWeight[0] = skinWeight.array[i*4 + 0];
				vertex.skinWeight[1] = skinWeight.array[i*4 + 1];
				vertex.skinWeight[2] = skinWeight.array[i*4 + 2];
				vertex.skinWeight[3] = skinWeight.array[i*4 + 3];
			}
	
			// Create key to check against

			let key = "";
			key += vertex.position[0].toFixed(3);
			key += "." + vertex.position[1].toFixed(3);
			key += "." + vertex.position[2].toFixed(3);

			key += "." + vertex.skinIndex[0];
			key += "." + vertex.skinIndex[1];
			key += "." + vertex.skinIndex[2];
			key += "." + vertex.skinIndex[3];

			key += "." + vertex.skinWeight[0].toFixed(3);
			key += "." + vertex.skinWeight[1].toFixed(3);
			key += "." + vertex.skinWeight[2].toFixed(3);
			key += "." + vertex.skinWeight[3].toFixed(3);

			if(!vertexKeys[key]) {
				vertexKeys[key] = this.vertices.length;
				this.vertices.push(vertex);
			}
			
			this.indexLookup[i] = vertexKeys[key];

		}

		if(!this.vertices.length) {
			return;
		}

		let byteLen = 0x24 * this.vertices.length;
		let round = byteLen % 16;
		if(round) {
			byteLen += 16 - round;
		}

		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x38, this.offset, true);
		this.header.setUint32(0x3c, this.vertices.length, true);
		this.blocks.push(buffer);
		this.offset += byteLen;
		
		let ofs = 0;
		this.vertices.forEach(vertex => {

			view.setFloat32(ofs + 0x00, vertex.position[0], true);
			view.setFloat32(ofs + 0x04, vertex.position[1], true);
			view.setFloat32(ofs + 0x08, vertex.position[2], true);
			
			view.setUint16(ofs + 0x0c, vertex.skinIndex[0], true);
			view.setUint16(ofs + 0x0e, vertex.skinIndex[1], true);
			view.setUint16(ofs + 0x10, vertex.skinIndex[2], true);
			view.setUint16(ofs + 0x12, vertex.skinIndex[3], true);
			
			view.setFloat32(ofs + 0x14, vertex.skinWeight[0], true);
			view.setFloat32(ofs + 0x18, vertex.skinWeight[1], true);
			view.setFloat32(ofs + 0x1c, vertex.skinWeight[2], true);
			view.setFloat32(ofs + 0x20, vertex.skinWeight[3], true);

			ofs += 0x24;

		});

	},

	readFaces : function(geometry) {

		let groups = geometry.groups;
		
		let position = geometry.attributes.position;
		let color = geometry.attributes.color.array;
		let normals = geometry.attributes.normal.array;
		let uv = geometry.attributes.uv.array;
		
		let use_alpha = false;

		if(geometry.attributes.vcolor) {
			color = geometry.attributes.vcolor.array;
			use_alpha = true;
		}

		let clrIndex = 0;
		let nrmIndex = 0;
		let uvIndex = 0;

		for(let i = 0; i < position.count; i += 3) {
			
			// Get Material Index

			let matIndex = -1;

			for(let k = 0; k < groups.length; k++) {

				if(i < groups[k].start) {
					continue;
				}

				if(i >= groups[k].start + groups[k].count) {
					continue;
				}
				
				matIndex = groups[k].materialIndex;
				break;
			}

			// Get Indices

			let a = this.indexLookup[i + 0];
			let b = this.indexLookup[i + 1];
			let c = this.indexLookup[i + 2];

			// Get Vertex Color
			
			let aColor, bColor, cColor;

			if(!use_alpha) {

				aColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : 255
				};
			
				bColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : 255
				};
			
				cColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : 255
				};

			} else {

				aColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : parseInt(color[clrIndex++] * 255)
				};
			
				bColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : parseInt(color[clrIndex++] * 255)
				};
			
				cColor = {
					r : parseInt(color[clrIndex++] * 255),
					g : parseInt(color[clrIndex++] * 255),
					b : parseInt(color[clrIndex++] * 255),
					a : parseInt(color[clrIndex++] * 255)
				};

			}

			// Get Vertex Normals
			
			let aNormal = {
				x : normals[nrmIndex++],
				y : normals[nrmIndex++],
				z : normals[nrmIndex++]
			};
			
			let bNormal = {
				x : normals[nrmIndex++],
				y : normals[nrmIndex++],
				z : normals[nrmIndex++]
			};
			
			let cNormal = {
				x : normals[nrmIndex++],
				y : normals[nrmIndex++],
				z : normals[nrmIndex++]
			};

			// Get Vertex uv0 

			let aUv = {
				u : uv[uvIndex++],
				v : uv[uvIndex++]
			};
			
			let bUv = {
				u : uv[uvIndex++],
				v : uv[uvIndex++]
			};
			
			let cUv = {
				u : uv[uvIndex++],
				v : uv[uvIndex++]
			};

			let face = {
				materialIndex : matIndex,
				indices : [ a, b, c ],
				color : [ aColor, bColor, cColor ],
				normals : [ aNormal, bNormal, cNormal ],
				uv : [ aUv, bUv, cUv ]
			};
			
			this.faces.push(face);

		}
		
		if(!this.faces.length) {
			return;
		}

		let byteLen = 0x58 * this.faces.length;
		let round = byteLen % 16;
		if(round) {
			byteLen += 16 - round;
		}

		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x48, this.offset, true);
		this.header.setUint32(0x4c, this.faces.length, true);
		this.blocks.push(buffer);
		this.offset += byteLen;
		let ofs = 0;

		this.faces.forEach(face => {
			
			// Material Index

			view.setInt16(ofs + 0x00, face.materialIndex, true);

			// Indices

			view.setUint32(ofs + 0x04, face.indices[0], true);
			view.setUint32(ofs + 0x08, face.indices[1], true);
			view.setUint32(ofs + 0x0c, face.indices[2], true);

			// Colors

			view.setUint8(ofs + 0x10, face.color[0].r);
			view.setUint8(ofs + 0x11, face.color[0].g);
			view.setUint8(ofs + 0x12, face.color[0].b);
			view.setUint8(ofs + 0x13, face.color[0].a);

			view.setUint8(ofs + 0x14, face.color[1].r);
			view.setUint8(ofs + 0x15, face.color[1].g);
			view.setUint8(ofs + 0x16, face.color[1].b);
			view.setUint8(ofs + 0x17, face.color[1].a);

			view.setUint8(ofs + 0x18, face.color[2].r);
			view.setUint8(ofs + 0x19, face.color[2].g);
			view.setUint8(ofs + 0x1a, face.color[2].b);
			view.setUint8(ofs + 0x1b, face.color[2].a);

			// Normals

			view.setFloat32(ofs + 0x1c, face.normals[0].x, true);
			view.setFloat32(ofs + 0x20, face.normals[0].y, true);
			view.setFloat32(ofs + 0x24, face.normals[0].z, true);

			view.setFloat32(ofs + 0x28, face.normals[1].x, true);
			view.setFloat32(ofs + 0x2c, face.normals[1].y, true);
			view.setFloat32(ofs + 0x30, face.normals[1].z, true);

			view.setFloat32(ofs + 0x34, face.normals[2].x, true);
			view.setFloat32(ofs + 0x38, face.normals[2].y, true);
			view.setFloat32(ofs + 0x3c, face.normals[2].z, true);
			
			// UV

			view.setFloat32(ofs + 0x40, face.uv[0].u, true);
			view.setFloat32(ofs + 0x44, face.uv[0].v, true);

			view.setFloat32(ofs + 0x48, face.uv[1].u, true);
			view.setFloat32(ofs + 0x4c, face.uv[1].v, true);

			view.setFloat32(ofs + 0x50, face.uv[2].u, true);
			view.setFloat32(ofs + 0x54, face.uv[2].v, true);

			ofs += 0x58;

		});

	},

	readBones : function(mesh) {

		let skeleton = mesh.skeleton || {};
		let bones = skeleton.bones || [];

		for(let i = 0; i < bones.length; i++) {
			
			let bone = bones[i];

			let name = bone.name;
			if(!name || name === "") {
				let num = i.toString();
				while(num.length < 3) {
					num = "0" + num;
				}
				name = "bone_" + num;
			}
			
			this.bones.push({
				name : name,
				id : i,
				parentId : bones.indexOf(bone.parent),
				matrix : bones[i].matrix.elements
			});

		}

		if(!this.bones.length) {
			return;
		}

		let byteLen = 0x70 * this.bones.length;
		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x58, this.offset, true);
		this.header.setUint32(0x5c, this.bones.length, true);
		this.blocks.push(buffer);
		this.offset += byteLen;
		
		let ofs = 0;
		this.bones.forEach(bone => {
			
			for(let i = 0; i < bone.name.length && i < 0x1f; i++) {
				view.setUint8(ofs + i, bone.name.charCodeAt(i));
			}
			
			view.setInt16(ofs + 0x20, bone.id, true);
			view.setInt16(ofs + 0x22, bone.parentId, true);

			view.setFloat32(ofs + 0x30, bone.matrix[0x00], true);
			view.setFloat32(ofs + 0x34, bone.matrix[0x01], true);
			view.setFloat32(ofs + 0x38, bone.matrix[0x02], true);
			view.setFloat32(ofs + 0x3c, bone.matrix[0x03], true);
			view.setFloat32(ofs + 0x40, bone.matrix[0x04], true);
			view.setFloat32(ofs + 0x44, bone.matrix[0x05], true);
			view.setFloat32(ofs + 0x48, bone.matrix[0x06], true);
			view.setFloat32(ofs + 0x4c, bone.matrix[0x07], true);
			view.setFloat32(ofs + 0x50, bone.matrix[0x08], true);
			view.setFloat32(ofs + 0x54, bone.matrix[0x09], true);
			view.setFloat32(ofs + 0x58, bone.matrix[0x0a], true);
			view.setFloat32(ofs + 0x5c, bone.matrix[0x0b], true);
			view.setFloat32(ofs + 0x60, bone.matrix[0x0c], true);
			view.setFloat32(ofs + 0x64, bone.matrix[0x0d], true);
			view.setFloat32(ofs + 0x68, bone.matrix[0x0e], true);
			view.setFloat32(ofs + 0x6c, bone.matrix[0x0f], true);

			ofs += 0x70;

		});

	},

	readAnimations : function(mesh) {

		const DASH_ANIMATION_POS             = 0x01;
		const DASH_ANIMATION_ROT             = 0x02;
		const DASH_ANIMATION_SCL             = 0x04;

		let geometry = mesh.geometry || {};
		let animations = geometry.animations || [];

		animations.forEach(animation => {

			let name = animation.name;
			if(!name || name === "") {
				let num = i.toString();
				while(num.length < 3) {
					num = "0" + num;
				}
				name = "anim_" + num;
			}

			let anim = {
				name : name,
				id : this.animations.length,
				duration : animation.duration,
				fps : 30,
				tracks : []
			};
			
			for(let i = 0; i < animation.tracks.length/3; i++) {

				let pos = animation.tracks[i*3+0];
				let rot = animation.tracks[i*3+1];
				let scl = animation.tracks[i*3+2];
				
				// First we get a unique list of times

				let times = [];
				pos.times.forEach(time => {
					if(times.indexOf(time) !== -1) {
						return;
					}
					times.push(time);
				});

				rot.times.forEach(time => {
					if(times.indexOf(time) !== -1) {
						return;
					}
					times.push(time);
				});

				scl.times.forEach(time => {
					if(times.indexOf(time) !== -1) {
						return;
					}
					times.push(time);
				});
				
				times.sort( (a, b) => {
					return a - b;
				});

				// Then we make a placeholder

				let placeholder = new Array(times.length);
				for(let k = 0; k < placeholder.length; k++) {
					
					placeholder[k] = {
						time : times[k],
						bone : i,
						flags : 0,
						pos : { x : 0, y : 0, z : 0 },
						rot : { x : 0, y : 0, z : 0, w :1 },
						scl : { x : 1, y : 1, z : 1 }
					};

				}

				// Then we loop through position

				for(let k = 0; k < pos.times.length; k++) {
					let time = pos.times[k];
					let index = times.indexOf(time);
					let ref = placeholder[index];
					ref.flags |= DASH_ANIMATION_POS;
					ref.pos.x = pos.values[k*3 + 0];
					ref.pos.y = pos.values[k*3 + 1];
					ref.pos.z = pos.values[k*3 + 2];
				}

				// Then we loop through rotation

				for(let k = 0; k < rot.times.length; k++) {
					let time = rot.times[k];
					let index = times.indexOf(time);
					let ref = placeholder[index];
					ref.flags |= DASH_ANIMATION_ROT;
					ref.rot.x = rot.values[k*4 + 0];
					ref.rot.y = rot.values[k*4 + 1];
					ref.rot.z = rot.values[k*4 + 2];
					ref.rot.w = rot.values[k*4 + 3];
				}

				// Then we loop through scale

				for(let k = 0; k < scl.times.length; k++) {
					let time = scl.times[k];
					let index = times.indexOf(time);
					let ref = placeholder[index];
					ref.flags |= DASH_ANIMATION_SCL;
					ref.scl.x = scl.values[k*3 + 0];
					ref.scl.y = scl.values[k*3 + 1];
					ref.scl.z = scl.values[k*3 + 2];
				}
			
				// Then we push back to anim tracks

				for(let k = 0; k < placeholder.length; k++) {
					anim.tracks.push(placeholder[k]);
				}
				
			}
			
			this.animations.push(anim);
		});

		if(!this.animations.length) {
			return;
		}

		let byteLen = 0x30 * this.animations.length;
		let buffer = new ArrayBuffer(byteLen);
		let view = new DataView(buffer);
		this.header.setUint32(0x68, this.offset, true);
		this.header.setUint32(0x6c, this.animations.length, true);
		this.blocks.push(buffer);
		this.offset += byteLen;
		let ofs = 0;

		this.animations.forEach(anim => {
			
			for(let i = 0; i < anim.name.length && i < 0x1f; i++) {
				view.setUint8(ofs + i, anim.name.charCodeAt(i));
			}
			
			view.setInt16(ofs + 0x20, anim.id, true);
			view.setInt16(ofs + 0x22, anim.fps, true);
			view.setUint32(ofs + 0x24, this.offset, true);
			view.setUint32(ofs + 0x28, anim.tracks.length, true);
			view.setFloat32(ofs + 0x2c, anim.duration, true);

			ofs += 0x30;

			let len = 0x30 * anim.tracks.length;
			let trackBuffer = new ArrayBuffer(len);
			let trackView = new DataView(trackBuffer);
			let trackOfs = 0;
			this.blocks.push(trackBuffer);

			this.offset += len;
			
			anim.tracks.forEach(track => {
				
				// Write Bone and boolean

				trackView.setInt16(trackOfs + 0x00, track.bone, true);
				trackView.setInt16(trackOfs + 0x02, track.flags, true);

				// Write Duration
				
				trackView.setFloat32(trackOfs + 0x04, track.time, true);
				
				// Write Position
				
				trackView.setFloat32(trackOfs + 0x08, track.pos.x, true);
				trackView.setFloat32(trackOfs + 0x0c, track.pos.y, true);
				trackView.setFloat32(trackOfs + 0x10, track.pos.z, true);
				
				// Write Rotation
				
				trackView.setFloat32(trackOfs + 0x14, track.rot.x, true);
				trackView.setFloat32(trackOfs + 0x18, track.rot.y, true);
				trackView.setFloat32(trackOfs + 0x1c, track.rot.z, true);
				trackView.setFloat32(trackOfs + 0x20, track.rot.w, true);
				
				// Write Scale
				
				trackView.setFloat32(trackOfs + 0x24, track.scl.x, true);
				trackView.setFloat32(trackOfs + 0x28, track.scl.y, true);
				trackView.setFloat32(trackOfs + 0x2c, track.scl.z, true);

				trackOfs += 0x30;

			});

		});

	
	}

}
