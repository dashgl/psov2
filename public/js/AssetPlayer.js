/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


const AssetPlayer = {


	"Dress" : async () => {
		
		const tex = new Array(5);
		tex[0] = await NinjaFile.API.load("quats/ASKIN001_T.PVR");
		tex[1] = await NinjaFile.API.load("quats/M_FL003_W1.PVR");
		tex[2] = await NinjaFile.API.load("quats/M_FL003_W2.PVR");
		tex[3] = await NinjaFile.API.load("quats/M_FL003_W3.PVR");
		tex[4] = await NinjaFile.API.load("quats/M_FL003_W4.PVR");
		
		const nj = await NinjaFile.API.load("quats/M_FL003F_BIP01.NJ");
		const njm = await NinjaFile.API.load("quats/N_MT_IDL2_B01_BIP01.NJM");

		const texList = new Array(tex.length);
		for(let i = 0; i < tex.length; i++) {
			texList[i] = NinjaTexture.API.parse(tex[i]);
		}
		
		const modelLoader = new NinjaModel("dress", texList);
		modelLoader.parse(nj);
		modelLoader.addAnimation(njm);
		
		const mdl = modelLoader.getModel();
		NinjaPlugin.API.setModel(this, mdl, [], texList);

	},


	"Humar" : async function() {

		let plMod = await NinjaFile.API.load("planj.bml");
		let plTex = await NinjaFile.API.load("platex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[86],
			texList[0],
			texList[1],
			texList[2],
			texList[77]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plAbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[27],
			texList[28]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plAhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[67],
			texList[68]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plAhai00.nj"]);
		let hair = haiLoader.getModel();

		let meshList = [head, hair];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Hunewearl" : async function() {

		let plMod = await NinjaFile.API.load("plbnj.bml");
		let plTex = await NinjaFile.API.load("plbtex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[176],
			texList[76],
			texList[77],
			texList[75],
			texList[78],
			texList[163],
			texList[172],
			texList[0],
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plBbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[121],
			texList[125]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plBhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[142],
			texList[143]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plBhai00.nj"]);
		let hair = haiLoader.getModel();

		let meshList = [head, hair];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Hucast" : async function() {

		let plMod = await NinjaFile.API.load("plcnj.bml");
		let plTex = await NinjaFile.API.load("plctex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[101],
			texList[0],
			texList[1],
			texList[2],
			texList[90]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plCbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[3],
			texList[4]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plChed00.nj"]);
		let head = hedLoader.getModel();

		let meshList = [head];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Ramar" : async function() {

		let plMod = await NinjaFile.API.load("pldnj.bml");
		let plTex = await NinjaFile.API.load("pldtex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[123],
			texList[4],
			texList[5],
			texList[6],
			texList[111]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plDbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[63],
			texList[64]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plDhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[103],
			texList[104],
			texList[105]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plDhai00.nj"]);
		let hair = haiLoader.getModel();

		console.log("--- Load Cap ---");

		let capTex = [
			texList[2],
			texList[3]
		];

		let capLoader = new NinjaModel(this + "_cap", haiTex);
		capLoader.parse(bml["plDcap00.nj"]);
		let cap = capLoader.getModel();

		let meshList = [head, hair, cap];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);
		body.skeleton.bones[59].add(cap);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Racast" : async function() {

		let plMod = await NinjaFile.API.load("plenj.bml");
		let plTex = await NinjaFile.API.load("pletex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[113],
			texList[1],
			texList[2],
			texList[3],
			texList[99],
			texList[0]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plEbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[4]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plEhed00.nj"]);
		let head = hedLoader.getModel();

		let meshList = [head];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Racaseal" : async function() {

		let plMod = await NinjaFile.API.load("plfnj.bml");
		let plTex = await NinjaFile.API.load("plftex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[141],
			texList[0],
			texList[1],
			texList[2],
			texList[126]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plFbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[3],
			texList[4]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plFhed00.nj"]);
		let head = hedLoader.getModel();

		let meshList = [head];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Fomarl" : async function() {

		let plMod = await NinjaFile.API.load("plgnj.bml");
		let plTex = await NinjaFile.API.load("plgtex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[185], // blueful
			texList[1], // dress (front)
			texList[2], // dress
			texList[176]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plGbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[144]
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plGhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[164]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plGhai00.nj"]);
		let hair = haiLoader.getModel();

		console.log("--- Load Cap ---");

		let capTex = [
			texList[3],
			texList[4],
			texList[5]
		];

		let capLoader = new NinjaModel(this + "_cap", capTex);
		capLoader.parse(bml["plGcap00.nj"]);
		let cap = capLoader.getModel();

		let meshList = [head, hair, cap];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);
		body.skeleton.bones[59].add(cap);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);

	},

	"Fonewm" : async function() {

		let plMod = await NinjaFile.API.load("plhnj.bml");
		let plTex = await NinjaFile.API.load("plhtex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[178], 
			texList[4],
			texList[5],
			texList[166],
			texList[0]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plHbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[135],
			texList[139],
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plHhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[160]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plHhai00.nj"]);
		let hair = haiLoader.getModel();

		console.log("--- Load Cap ---");

		let capTex = [
			texList[159],
			texList[7],
			texList[6]
		];

		let capLoader = new NinjaModel(this + "_cap", capTex);
		capLoader.parse(bml["plHcap00.nj"]);
		let cap = capLoader.getModel();

		let meshList = [head, hair, cap];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);
		body.skeleton.bones[59].add(cap);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);


	},

	"Fonewearl" : async function() {

		let plMod = await NinjaFile.API.load("plinj.bml");
		let plTex = await NinjaFile.API.load("plitex.afs");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");

		let bml = NinjaFile.API.bml(plMod);
		let afs = NinjaFile.API.afs(plTex, true);
		let mot = NinjaFile.API.rlc(plMot);

		console.log("--- Parse All Textures ---");

		let texList = new Array(afs.length);
		for(let i = 0; i < afs.length; i++) {
			texList[i] = NinjaTexture.API.parse(afs[i]);
		}

		console.log("--- Load Body ---");

		console.log(afs[0]);

		let bdyTex = [
			texList[279], 
			texList[1],
			texList[2],
			texList[267],
			texList[0]
		];
		
		let bdyLoader = new NinjaModel(this + "_body", bdyTex);
		bdyLoader.parse(bml["plIbdy00.nj"]);
		bdyLoader.psobbMotion(mot);
		let body = bdyLoader.getModel();

		console.log("--- Load Head ---");

		let hedTex = [
			texList[238],
			texList[234],
		];

		let hedLoader = new NinjaModel(this + "_head", hedTex);
		hedLoader.parse(bml["plIhed00.nj"]);
		let head = hedLoader.getModel();

		console.log("--- Load Hair ---");

		let haiTex = [
			texList[160]
		];

		let haiLoader = new NinjaModel(this + "_hair", haiTex);
		haiLoader.parse(bml["plIhai00.nj"]);
		let hair = haiLoader.getModel();

		console.log("--- Load Cap ---");

		let capTex = [
			texList[12],
			null,
			texList[13]
		];

		let capLoader = new NinjaModel(this + "_cap", capTex);
		capLoader.parse(bml["plIcap00.nj"]);
		let cap = capLoader.getModel();

		let meshList = [head, hair, cap];

		for(let key in bml) {
			
			bml[key].seekSet(0);
			let ldr = new NinjaModel(key.replace(".nj", ""));
			ldr.parse(bml[key]);
			meshList.push(ldr.getModel());

		}

		console.log("--- Display Model ---");

		body.skeleton.bones[59].add(head);
		body.skeleton.bones[59].add(hair);
		body.skeleton.bones[59].add(cap);

		NinjaPlugin.API.setPlayer(this, body, meshList, texList);


	},

	"Red Ring Rico" : async function() {

		let ark = await NinjaFile.API.load("gsl_forest02.gsl");
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl["rico_body.bml"]);
		let tex = NinjaTexture.API.parse(bml["rico_body.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["rico_body.nj"]);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);
		

	},

	"bm_n_ebw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_ebw_i_body.bml']);

		console.log(bml);
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(bml["n_ebw_i_body.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["n_ebw_i_body.nj"]);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_ecw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_ecw_i_body.bml']);

		console.log(bml);
		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(bml["n_ecw_i_body.pvm"]);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(bml["n_ecw_i_body.nj"]);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_efsw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_efsw_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_efw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_efw_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_emw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_emw_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_eow_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_eow_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_etw_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_etw_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_ebm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_ebm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_ecm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_efsw_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_efsm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_efsm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_efm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_efm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_emm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_emm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_eom_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_eom_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_etm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_etm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteib_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteib_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteib2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteib2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteif_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteif_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteif2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteif2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteifs_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteifs_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteifs2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteifs2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteio_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteio_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteio2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteio2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteit_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteit_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_kanteit2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_kanteit2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_gunb2_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_gunb2_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_gunm_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_gunm_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_soutoku_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_soutoku_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_trunk_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_trunk_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_hakase_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_hakase_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_nurse_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_nurse_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"bm_n_hisyo_i_body" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);
		let bml = NinjaFile.API.bml(gsl['bm_n_hisyo_i_body.bml']);

		let queue = {};

		for(let key in bml) {
			let ext = key.split(".").pop();
			queue[ext] = bml[key];
		}

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);
		let tex = NinjaTexture.API.parse(queue.pvm);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.parse(queue.nj);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_a00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_a00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_a00_w_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_b00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_b00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_b00_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_d00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_d00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_d00_w_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_e00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_e00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_e00_w_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_f00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_f00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_f00_w_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_g00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_g00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_g00_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_h00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_h00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_h00_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_i00_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_i00_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_i00_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_b01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_b01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_b01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_c01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_c01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_c01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_d01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_d01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_d01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_g01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_g01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_g01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_h01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_h01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_h01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	},

	"npc_i01_data" : async function() {

		let ark = await NinjaFile.API.load("gsl_city.gsl");
		let gsl = NinjaFile.API.gsl(ark);

		let bs = gsl['npc_i01_data.rel'];
		let tex = NinjaTexture.API.parse(gsl['n_i01_e_body.pvm']);

		bs.seekEnd(-16);
		let tablePtr = bs.readUInt();

		bs.seekSet(tablePtr);
		//let njtlPtr = bs.readUInt();
		let njPtr = bs.readUInt();
		bs.seekSet(njPtr);

		let plMot = await NinjaFile.API.load("plymotiondata.rlc");
		let mot = NinjaFile.API.rlc(plMot);

		let modelLoader = new NinjaModel(this, tex);
		modelLoader.rel("NJCM", bs);
		modelLoader.psobbMotion(mot);
		let mdl = modelLoader.getModel();
		NinjaPlugin.API.setPlayer(this, mdl, [], tex);

	}


};
