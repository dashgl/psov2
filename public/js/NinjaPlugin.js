/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


const NinjaPlugin = (function() {

	"use strict";
	
	this.MEM = {
		label : "default",
		mdlList : [],
		texList : [],
		cache : [],
		mixers : [],
		clock : new THREE.Clock(),
		scene : new THREE.Scene(),
		camera : new THREE.PerspectiveCamera(),
		renderer : new THREE.WebGLRenderer(),
	}

	this.DOM = {
		nav : {
			stage : document.getElementById('NinjaPlugin.nav.stage'),
			rooms : document.getElementById('NinjaPlugin.nav.rooms'),
			player : document.getElementById('NinjaPlugin.nav.player'),
			enemies : document.getElementById('NinjaPlugin.nav.enemies'),
			weapons : document.getElementById('NinjaPlugin.nav.weapons'),
			objects : document.getElementById('NinjaPlugin.nav.objects')
		},
		assets : {
			tray : document.getElementById('NinjaPlugin.assets.tray'),
			viewport : document.getElementById('NinjaPlugin.assets.viewport'),
			download : document.getElementById('NinjaPlugin.assets.download'),
			signed : document.getElementById('NinjaPlugin.assets.signed'),
			textures : document.getElementById('NinjaPlugin.assets.Textures'),
			label : document.getElementById('NinjaPlugin.assets.label')
		},
		animations : {
			select : document.getElementById('NinjaPlugin.animation.select')
		}
	}


	this.EVT = {
		handleNavClick : evt_handleNavClick.bind(this),
		handleWindowResize : evt_handleWindowResize.bind(this),
		handleDownloadClick : evt_handleDownloadClick.bind(this),
		handleSignedClick : evt_handleSignedClick.bind(this),
		handleOptionChange : evt_handleOptionChange.bind(this),
		handleTextureClick : evt_handleTextureClick.bind(this),
		handleTrayClick : evt_handleTrayClick.bind(this)
	}

	this.API = {
		animate : api_animate.bind(this),
		resetScene : api_resetScene.bind(this),
		setPlayer : api_setPlayer.bind(this),
		setItem : api_setItem.bind(this),
		setModel : api_setModel.bind(this),
		setWeapon : api_setWeapon.bind(this),
		setStage : api_setStage.bind(this),
		updateViewport : api_updateViewport.bind(this),
		setActiveNav : api_setActiveNav.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		this.DOM.nav.stage.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.nav.rooms.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.nav.player.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.nav.enemies.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.nav.weapons.addEventListener('click', this.EVT.handleNavClick);
		this.DOM.nav.objects.addEventListener('click', this.EVT.handleNavClick);

		this.DOM.assets.download.addEventListener('click', this.EVT.handleDownloadClick);
		this.DOM.assets.signed.addEventListener('click', this.EVT.handleSignedClick);
		this.DOM.assets.textures.addEventListener('click', this.EVT.handleTextureClick);
		this.DOM.animations.select.addEventListener('change', this.EVT.handleOptionChange);
		this.DOM.assets.tray.addEventListener('click', this.EVT.handleTrayClick);

		// Set Camera

		this.MEM.camera.position.z = 55;
		this.MEM.camera.position.y = 19;
		this.MEM.camera.position.x = 3;
		this.MEM.camera.lookAt(new THREE.Vector3(0, 0, 0));

		// Init Threejs

		this.MEM.renderer.domElement.style.margin = "0";
		this.MEM.renderer.domElement.style.padding = "0";
		this.MEM.renderer.setClearColor(0x263238, 1);
		this.MEM.renderer.sortObjects = false;

		this.DOM.assets.viewport.appendChild(this.MEM.renderer.domElement);

		this.MEM.controls = THREE.OrbitControls(this.MEM.camera, this.DOM.assets.viewport);
		window.addEventListener("resize", this.EVT.handleWindowResize);

		// Set Grid and Color

		let grid = new THREE.GridHelper(100, 10);
		this.MEM.scene.add(grid);

		let light = new THREE.AmbientLight(0xffffff);
		this.MEM.scene.add(light);
		
		this.API.updateViewport();
		this.API.animate();

		const leaf = localStorage.getItem('leaf') || 'stage';
		this.API.setActiveNav(leaf);

	}

	function evt_handleNavClick(evt) {

		let element = evt.target;
		let id = element.getAttribute('id');
		let leaf = id.split('.').pop();
		this.API.setActiveNav(leaf);

	}

	function api_setActiveNav (leaf) {
	
		if(!this.DOM.nav[leaf]) {
			return;
		}

		for(let key in this.DOM.nav) {
			this.DOM.nav[key].classList.remove('active');
		}

		this.DOM.nav[leaf].classList.add('active');
		localStorage.setItem('leaf', leaf);
		
		const table = {
			'stage' : AssetStage,
			'rooms' : AssetRooms,
			'player' : AssetPlayer,
			'enemies' : AssetEnemies,
			'weapons' : AssetWeapons,
			'objects' : AssetObjects
		};

		this.DOM.assets.tray.innerHTML = "";
		
		let num = 0;
		for(let key in table[leaf]) {
			
			let str = num.toString();
			while(str.length < 3) {
				str = "0" + str;
			}
			num++;

			let li = document.createElement('li');
			li.textContent = str + " " + key;
			li.addEventListener('click', table[leaf][key].bind(key));
			this.DOM.assets.tray.appendChild(li);

		}

	}

	function evt_handleTrayClick(evt) {

		if(evt.target.tagName !== "LI") {
			return;
		}

		if(this.MEM.activeLi) {
			this.MEM.activeLi.classList.remove("active");
		}

		this.MEM.activeLi = evt.target;
		this.MEM.activeLi.classList.add("active");

	}

	function evt_handleWindowResize() {

		this.API.updateViewport();

	}

	function evt_handleSignedClick() {

		if(!this.MEM.label) {
			return;
		}
		
		const [ mesh ] = this.MEM.mdlList;

		if(!mesh) {
			return;
		}
		
		console.log(mesh);
		const cert = new CertLoader();
		cert.export();

	}

	async function evt_handleDownloadClick() {

		if(!this.MEM.label) {
			return;
		}

		let label = this.MEM.label.toLowerCase().replace(/ /g,"_");
		let zip_name = this.MEM.prefix + label + ".zip";
		let zip = new JSZip();
		
		let positions = {};

		// First Add DMF Files

		let dmfFolder = zip.folder("dmf");
		this.MEM.mdlList.forEach(mesh => {
				
			let expt = new THREE.DashExporter();
			let blob = expt.parse(mesh);
			dmfFolder.file(mesh.name + ".dmf", blob);

			positions[mesh.name] = {
				pos : mesh.position,
				rot : mesh.rotation
			}

		});

		let txt = JSON.stringify(positions, null, 4);
		zip.file("scene.json", txt);

		// Then add Texture Files

		let texFolder = zip.folder("png");
		this.MEM.texList.forEach(tex => {

			let canvas = tex.image;
			let savable = canvas.toDataURL();
			let index = savable.indexOf(',') + 1;
			let base = savable.substr(index);
			texFolder.file(tex.name + ".png", base, { base64 : true });

		});

		let glbFolder = zip.folder("glb");
		
		async.eachSeries(this.MEM.mdlList, (mesh, nextMesh) => {

			let gltfExp = new THREE.GLTFExporter();
			let opts = {
				binary : true,
				animations : mesh.geometry.animations || []
			};
		
			gltfExp.parse(mesh, async function(gltf) {
				glbFolder.file(mesh.name + ".glb", gltf);
				nextMesh();
			}, opts);

		}, async () => {
			
			let blob = await zip.generateAsync({ type: "blob" });
			saveAs(blob, zip_name);
			console.log("EXPORT COMPLETE!!!");

		});

	}

	async function evt_handleTextureClick() {

		if(!this.MEM.label) {
			return;
		}

		let label = this.MEM.label.toLowerCase().replace(/ /g,"_");
		let zip_name = this.MEM.prefix + label + ".zip";
		let zip = new JSZip();
		
		// Then add Texture Files

		this.MEM.texList.forEach(tex => {

			let canvas = tex.image;
			let savable = canvas.toDataURL();
			let index = savable.indexOf(',') + 1;
			let base = savable.substr(index);
			zip.file(tex.name + ".png", base, { base64 : true });

		});
		
		let blob = await zip.generateAsync({ type: "blob" });
		saveAs(blob, zip_name);

	}

	function evt_handleOptionChange() {

		let index = parseInt(this.DOM.animations.select.value);

		if(this.MEM.action) {
			this.MEM.action.stop();
		}

		if(index === -1) {
			return;
		}

		let clip = this.MEM.anims[index];
		this.MEM.action = this.MEM.mixers[0].clipAction(clip, this.MEM.active);
		this.MEM.action.timeScale = 1.0;
		this.MEM.action.play();

	}

	function api_updateViewport() {

		let width = this.DOM.assets.viewport.offsetWidth;
		let height = this.DOM.assets.viewport.offsetHeight;

		this.MEM.renderer.setSize(width, height);
		this.MEM.camera.aspect = width / height;
		this.MEM.camera.updateProjectionMatrix();

	}

	function api_animate() {
		
		requestAnimationFrame(this.API.animate);
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);

		let delta = this.MEM.clock.getDelta();
		this.MEM.mixers.forEach(mixer => {
			mixer.update(delta);
		});

	}

	function api_setPlayer(label, model, meshes, texList) {
		
		this.DOM.assets.label.textContent = label;
		this.API.resetScene();

		this.MEM.prefix = "ply_";
		this.MEM.label = label;
		this.MEM.texList = texList;


		let helper = new THREE.SkeletonHelper( model );
		this.MEM.scene.add(model);
		this.MEM.scene.add(helper);

		this.MEM.mdlList.push(model);
		meshes.forEach(mesh => {
			this.MEM.mdlList.push(mesh);
		});

		if(!model.geometry.animations) {
			return;
		}

		let mixer = new THREE.AnimationMixer(model);
		this.MEM.mixers.push(mixer);
		this.MEM.anims = model.geometry.animations;

		for(let i = 0; i < model.geometry.animations.length; i++) {
			
			let option = document.createElement("option");
			option.textContent = model.geometry.animations[i].name;
			option.setAttribute("value", i.toString());
			this.DOM.animations.select.appendChild(option);

		}

	}

	function api_setItem(label, model, meshes, texList) {

		this.DOM.assets.label.textContent = label;
		this.API.resetScene();
		
		this.MEM.prefix = "obj_";
		this.MEM.label = label;
		this.MEM.texList = texList;
		
		let helper = new THREE.SkeletonHelper( model );
		this.MEM.scene.add(model);
		this.MEM.scene.add(helper);

		let dx = 20;
		this.MEM.mdlList.push(model);
		meshes.forEach(mesh => {
			this.MEM.mdlList.push(mesh);
			mesh.position.x = dx;
			dx += 20;
			this.MEM.scene.add(mesh);
		});

		if(!model.geometry.animations) {
			return;
		}

		let mixer = new THREE.AnimationMixer(model);
		this.MEM.mixers.push(mixer);
		this.MEM.anims = model.geometry.animations;

		for(let i = 0; i < model.geometry.animations.length; i++) {
			
			let option = document.createElement("option");
			option.textContent = model.geometry.animations[i].name;
			option.setAttribute("value", i.toString());
			this.DOM.animations.select.appendChild(option);

		}

	}

	function api_setModel(label, model, meshes, texList) {

		this.DOM.assets.label.textContent = label;
		this.API.resetScene();
		
		this.MEM.prefix = "ene_";
		this.MEM.label = label;
		this.MEM.texList = texList;
		
		let helper = new THREE.SkeletonHelper( model );
		this.MEM.scene.add(model);
		this.MEM.scene.add(helper);

		let dx = 20;
		this.MEM.mdlList.push(model);
		meshes.forEach(mesh => {
			this.MEM.mdlList.push(mesh);
			mesh.position.x = dx;
			dx += 20;
			this.MEM.scene.add(mesh);
		});

		if(!model.geometry.animations) {
			return;
		}

		let mixer = new THREE.AnimationMixer(model);
		this.MEM.mixers.push(mixer);
		this.MEM.anims = model.geometry.animations;

		for(let i = 0; i < model.geometry.animations.length; i++) {
			
			let option = document.createElement("option");
			option.textContent = model.geometry.animations[i].name;
			option.setAttribute("value", i.toString());
			this.DOM.animations.select.appendChild(option);

		}

	}

	function api_setWeapon(label, model, meshes, texList) {

		this.DOM.assets.label.textContent = label;
		this.API.resetScene();
		
		this.MEM.prefix = "wpn_";
		this.MEM.label = label;
		this.MEM.texList = texList;
		
		this.MEM.scene.add(model);

		let pos = 10;
		this.MEM.mdlList.push(model);
		meshes.forEach(mesh => {
			this.MEM.mdlList.push(mesh);
			mesh.position.x = pos;
			pos += 10;
			this.MEM.scene.add(mesh);
		});

		if(!model.geometry.animations) {
			return;
		}

		let mixer = new THREE.AnimationMixer(model);
		this.MEM.mixers.push(mixer);
		this.MEM.anims = model.geometry.animations;

		for(let i = 0; i < model.geometry.animations.length; i++) {
			
			let option = document.createElement("option");
			option.textContent = model.geometry.animations[i].name;
			option.setAttribute("value", i.toString());
			this.DOM.animations.select.appendChild(option);

		}

	}

	function api_setStage(label, meshes, models, texList) {

		this.DOM.assets.label.textContent = label;
		this.API.resetScene();
		
		this.MEM.prefix = "stg_";
		this.MEM.label = label;
		this.MEM.texList = texList;

		meshes.forEach(mesh => {
			
			this.MEM.scene.add(mesh);
			this.MEM.mdlList.push(mesh);

		});

		models.forEach(mesh => {
			
			this.MEM.scene.add(mesh);
			let mixer = new THREE.AnimationMixer(mesh);
			let clip = mesh.geometry.animations[0];
			if(clip) {
				let action = mixer.clipAction(clip, mesh);
				action.play();
				this.MEM.mixers.push(mixer);
				this.MEM.mdlList.push(mesh);
			}

		});

	}

	function api_resetScene() {
		
		this.MEM.label = "default";
		this.MEM.mdlList = [];
		this.MEM.texList = [];
		this.MEM.mixers = [];
		
		this.MEM.cache.forEach(mesh => {
			this.MEM.scene.remove(mesh);
			mesh.geometry.dispose();
			mesh.material.forEach(mat => {
				if(mat.map) {
					mat.map.dispose();
				}
				mat.dispose();
			});
			mesh.material.dispose();
			mesh.dispose();
		});

		this.MEM.anims = [];
		this.MEM.cache = [];

		this.DOM.animations.select.innerHTML = "";
		let option = document.createElement("option");
		option.textContent = "Select Animation";
		option.setAttribute("value", "-1");
		this.DOM.animations.select.appendChild(option);
		
		//this.MEM.scene.dispose();
		this.MEM.scene = new THREE.Scene();

		let grid = new THREE.GridHelper(100, 10);
		this.MEM.scene.add(grid);

		let light = new THREE.AmbientLight(0xffffff);
		this.MEM.scene.add(light);

	}


}).apply({});
