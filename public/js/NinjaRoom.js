/**
  
  Ninja Plugin
  Copyright (C) 2019, DashGL Project
  By Kion (kion@dashgl.com)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
**/


class NinjaRoom {

	constructor(texList, matList) {
		
		this.texList = texList;
		this.matList = matList;

		this.sections = {
			"section_abs" : []
		};
		
		this.white = this.texList.length;
		this.animMesh = [];
		this.staticMesh = [];

	}

	prepare(bs) {

		bs.seekEnd(-16);
		let ptr = bs.readUInt();
		bs.seekSet(ptr);

		let nbSection = bs.readUInt();
		bs.seekCur(4);
		let sectionOfs = bs.readUInt();
		let textureOfs = bs.readUInt();

		bs.seekSet(sectionOfs);
		let sections = new Array(nbSection);

		for (let i = 0; i < nbSection; i++) {

			let id = bs.readInt();
			let pos = bs.readVec3();
			let rot = bs.readRot3();
			let radius = bs.readFloat();
			let staticModelOfs = bs.readUInt();
			let attributeOfs = bs.readUInt();
			let animatedModelOfs = bs.readUInt();
			let nbStaticModel = bs.readUInt();
			let nbAttribute = bs.readUInt();
			let nbAnimatedModel = bs.readUInt();
			let sectionEnd = bs.readUInt();
			
			let key = "section_abs";
			let saveOfs = bs.tell();
			let section;

			if(id >= 0) {

				id = id.toString();
				while(id.length < 3) {
					id = "0" + id;
				}

				key = "section_" + id;

				if(!this.sections[key]) {
					this.sections[key] = {
						pos : pos,
						rot : rot,
						statModel : [],
						animModel : []
					}
				}

				section = this.sections[key];

			} else {

				let index = this.sections[key].length;
				this.sections[key].push({
					pos : pos,
					rot : rot,
					statModel : [],
					animModel : []
				})

				section = this.sections[key][index];

			}

			let bone = new THREE.Bone();
			var yRotMatrix = new THREE.Matrix4();
			yRotMatrix.makeRotationY(rot.y);
			bone.applyMatrix(yRotMatrix);

			bone.position.x = pos.x;
			bone.position.y = pos.y;
			bone.position.z = pos.z;

			bone.updateMatrix();
			bone.updateMatrixWorld();
			section.bone = bone;

			bs.seekSet(staticModelOfs);
			for (let k = 0; k < nbStaticModel; k++) {
				let meshOfs = bs.readUInt();
				section.statModel.push({
					bs : bs,
					ofs : meshOfs
				});
				bs.seekCur(0x2c);
			}

			bs.seekSet(animatedModelOfs);
			for (let k = 0; k < nbAnimatedModel; k++) {
				let meshOfs = bs.readUInt();
				let animOfs = bs.readUInt();
				section.animModel.push({
					bs : bs,
					ofs : meshOfs,
					animOfs : animOfs
				});
				bs.seekCur(0x34);
			}

			bs.seekSet(saveOfs);

		}

	}

	makeWhiteTexture() {

		if(this.matList.length > this.texList.length) {
			return;
		}

		console.log("making white texture");

		let canvas = document.createElement("canvas");
		let ctx = canvas.getContext("2d");
		canvas.width = 32;
		canvas.height = 32;
		ctx.fillStyle = "#fff";
		ctx.fillRect(0,0, canvas.width, canvas.height);
		let texture = new THREE.Texture(canvas);
		texture.name = "white_texture";
		texture.needsUpdate = true;

		let num = this.white.toString();
		while(num.length < 3) {
			num = "0" + num;
		}

		let mat = new THREE.MeshBasicMaterial({
			name : "material_" + num,
			vertexColors : THREE.VertexColors,
			transparent : true
		});

		this.matList.push(mat);

	}

	reset() {
			
		// Prepare Attributes
		
		this.vertexStack = [];
		this.colorStack = [];

		this.vertices = [];
		this.matIndex = [];
		this.colors = [];
		this.vcolors = [];
		this.uv = [];

	}

	createMesh() {

		// Create Stage Mesh

		let geometry = new THREE.BufferGeometry();
		let vertices = new Float32Array(this.vertices);
		let colors = new Float32Array(this.colors);
		let vcolors = new Float32Array(this.vcolors);
		let uv = new Float32Array(this.uv);

		geometry.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
		geometry.addAttribute( 'color', new THREE.BufferAttribute( vcolors, 4 ) );
		geometry.addAttribute( 'vcolor', new THREE.BufferAttribute( vcolors, 4 ) );
		geometry.addAttribute( 'uv', new THREE.BufferAttribute( uv, 2 ) );
		geometry.computeVertexNormals();

		let stack = [];

		for(let i = 0; i < this.matIndex.length; i++) {
			
			let last = stack[stack.length - 1] || {};

			if(this.matIndex[i] === -1) {
				this.matIndex[i] = this.white;
				this.makeWhiteTexture();
			}

			if(this.matIndex[i] === last.materialIndex) {
				last.count += 3;
				continue;
			}

			stack.push({
				start : i*3,
				count : 3,
				materialIndex : this.matIndex[i]
			});

		}
			
		stack.forEach(m => {
			geometry.addGroup(m.start, m.count, m.materialIndex);
		});

		let mesh = new THREE.Mesh(geometry, this.matList);
		return mesh;

	}

	execute(name, skipAnim) {
		
		// Read Static Stage Mesh
		

		for(let key in this.sections) {

			if(key === "section_abs") {
				continue;
			}
		
			this.reset();
			

			this.sections[key].statModel.forEach(mesh => {
				
				this.bs = mesh.bs;
				this.bs.seekSet(mesh.ofs);
				this.readBone();

			});
			
			let mesh = this.createMesh();
			// this.sections[key].bone
			mesh.name = name + "_" + key;
			this.staticMesh.push(mesh);

		}

		// Read Absolute Mesh
		
			
		let i = 0;
		this.sections["section_abs"].forEach(section => {
				
			this.reset();

			section.statModel.forEach(mesh => {
				
				this.bs = mesh.bs;
				this.bs.seekSet(mesh.ofs);
				this.readBone();

			});
			
			if(!this.vertices.length) {
				return;
			}

			let mesh = this.createMesh();
			mesh.name = name + "_abs" + i;
			this.staticMesh.push(mesh);
			i++;

		});

		
	}

	getMeshList() {

		return this.staticMesh;

	}

	getModelList() {

		return this.animMesh;

	}

	readBone(parentBone) {

		let bone = {
			flag: this.bs.readUInt(),
			chunkOfs: this.bs.readUInt(),
			pos: this.bs.readVec3(),
			rot: this.bs.readRot3(),
			scl: this.bs.readVec3(),
			childOfs: this.bs.readUInt(),
			siblingOfs: this.bs.readUInt()
		}

		this.bone = new THREE.Bone();

		if (!BitStream.bitflag(bone.flag, 2)) {
			this.bone.scale.x = bone.scl.x;
			this.bone.scale.y = bone.scl.y;
			this.bone.scale.z = bone.scl.z;
			this.bone.updateMatrix();
		}

		if (!BitStream.bitflag(bone.flag, 1)) {

			var xRotMatrix = new THREE.Matrix4();
			xRotMatrix.makeRotationX(bone.rot.x);
			this.bone.applyMatrix(xRotMatrix);

			var yRotMatrix = new THREE.Matrix4();
			yRotMatrix.makeRotationY(bone.rot.y);
			this.bone.applyMatrix(yRotMatrix);

			var zRotMatrix = new THREE.Matrix4();
			zRotMatrix.makeRotationZ(bone.rot.z);
			this.bone.applyMatrix(zRotMatrix);

		}

		if (!BitStream.bitflag(bone.flag, 0)) {
			this.bone.position.x = bone.pos.x;
			this.bone.position.y = bone.pos.y;
			this.bone.position.z = bone.pos.z;
		}

		this.bone.updateMatrix();
		this.bone.updateMatrixWorld();

		if (parentBone) {
			parentBone.add(this.bone);
			this.bone.updateMatrix();
			this.bone.updateMatrixWorld();
		}

		if (bone.chunkOfs) {
			this.bs.seekSet(bone.chunkOfs);
			let vertexOfs = this.bs.readUInt();
			let stripOfs = this.bs.readUInt();

			if (vertexOfs) {
				this.bs.seekSet(vertexOfs);
				this.readChunk();
			}

			if (stripOfs) {
				this.bs.seekSet(stripOfs);
				this.readChunk();
			}

		}

		if (bone.childOfs) {
			this.bs.seekSet(bone.childOfs);
			this.readBone(this.bone);
		}

		if (bone.siblingOfs) {
			this.bs.seekSet(bone.siblingOfs);
			this.readBone(parentBone);
		}

	}

	readChunk () {

		const NJD_NULLOFF = 0x00;
		const NJD_BITSOFF = 0x01;
		const NJD_TINYOFF = 0x08;
		const NJD_MATOFF = 0x10;
		const NJD_VERTOFF = 0x20;
		const NJD_VOLOFF = 0x38;
		const NJD_STRIPOFF = 0x40;
		const NJD_ENDOFF = 0xFF;

		this.matId = -1;

		let chunk = {};

		do {

			chunk.head = this.bs.readByte();
			chunk.flag = this.bs.readByte();

			if (chunk.head > NJD_STRIPOFF + 11) {
				continue;
			}

			if (chunk.head >= NJD_STRIPOFF) {
				this.readStripChunk(chunk);
				continue;
			}

			if (chunk.head >= NJD_VOLOFF) {
				throw new Error("Volume chunk detected");
			}

			if (chunk.head >= NJD_VERTOFF) {
				this.readVertexChunk(chunk);
				continue;
			}

			if (chunk.head >= NJD_MATOFF) {
				this.readMaterialChunk(chunk);
				continue;
			}

			if (chunk.head >= NJD_TINYOFF) {
				this.readTinyChunk(chunk);
				continue;
			}

			if (chunk.head >= NJD_BITSOFF) {
				this.readBitsChunk(chunk);
				continue;
			}

		} while (chunk.head !== NJD_ENDOFF);

	}

	readBitsChunk(chunk) {

		switch (chunk.head) {
		case 1:
			let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
			let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);
			break;
		case 2:
			let mipmapDepth = chunk.flag & 0x0f;
			break;
		case 3:
			let specularCoef = chunk.flag & 0x1f;
			break;
		case 4:
			this.bs.storeOfs(chunk.flag);
			chunk.head = 0xff;
			break;
		case 5:
			this.bs.restoreOfs(chunk.flag);
			break;
		}

	}

	readTinyChunk(chunk) {

		let tinyChunk = this.bs.readUShort();
		this.matId = tinyChunk & 0x1FFF;

		let superSample = BitStream.bitflag(tinyChunk, 13);
		let filterMode = BitStream.bitmask(tinyChunk, [14, 15]);

		let clampU = BitStream.bitflag(chunk.flag, 4);
		let clampV = BitStream.bitflag(chunk.flag, 5);
		let flipU = BitStream.bitflag(chunk.flag, 6);
		let flipV = BitStream.bitflag(chunk.flag, 7);
		
		/*
		let tex = this.texList[this.matId];
		if(tex.clampU || tex.clampV) {
			tex.wrapS = THREE.RepeatWrapping;
			tex.wrapT = THREE.RepeatWrapping;
		}
		*/

	}

   readMaterialChunk(chunk) {

		chunk.length = this.bs.readUShort();

		// Alpha Blending Instructions

		let dstAlpha = BitStream.bitmask(chunk.flag, [0, 1, 2]);
		let srcAlpha = BitStream.bitmask(chunk.flag, [3, 4, 5]);

		// Diffuse

		if (BitStream.bitflag(chunk.head, 0)) {
			this.bs.readColor();
		}

		// Specular

		if (BitStream.bitflag(chunk.head, 1)) {
			this.bs.readColor();
		}

		// Ambient

		if (BitStream.bitflag(chunk.head, 2)) {
			this.bs.readColor();
		}

	}

	readVertexChunk(chunk) {

		chunk.length = this.bs.readUShort();

		// Read the index offset and the number of index

		let indexOfs = this.bs.readUShort();
		let nbIndex = this.bs.readUShort();

		// Read the vertex list

		for(let i = 0; i < nbIndex; i++) {

			let stackOfs = indexOfs + i;
			let vertex = new THREE.Vector3();

			// Read the position

			let pos = this.bs.readVec3();
			vertex.x = pos.x;
			vertex.y = pos.y;
			vertex.z = pos.z;
			vertex.applyMatrix4(this.bone.matrixWorld);
			this.vertexStack[stackOfs] = vertex;

			// Read vertex normals

			if (chunk.head > 0x28 && chunk.head < 0x30) {
				let normal = this.bs.readVec3();
			}

			// Read vertex color

			if(chunk.head === 0x23 || chunk.head === 0x2a) {
				let color = this.bs.readColor();
				this.colorStack[stackOfs] = color;
			}

		}

	}

	readStripChunk(chunk) {

		chunk.length = this.bs.readUShort();

		// Read the number of strips and user offset

		let stripChunk = this.bs.readUShort();
		let nbStrips = stripChunk & 0x3FFF;
		let userOffset = BitStream.bitmask(stripChunk, [14, 15]);
		let doubleSide = BitStream.bitflag(chunk.flag, 4);

		// Read the list of strips

		for(let i = 0; i < nbStrips; i++) {

			// Read the length and direction

			let strip_length = this.bs.readShort();
			let clockwise = strip_length < 0 ? true : false;
			let length = Math.abs(strip_length);
			let strip = new Array(length);

			// Read the strip

			for (let k = 0; k < strip.length; k++) {

				// Read stack position

				let stackOfs = this.bs.readUShort();

				strip[k] = {
					index : stackOfs
				};

				// Read face uv values

				switch (chunk.head) {
				case 0x41:
					this.face_texture = true;
					strip[k].uv = new THREE.Vector2(
						this.bs.readShort() / 255,
						this.bs.readShort() / 255
					);
					break;
				case 0x42:
					this.face_texture = true;
					strip[k].uv = new THREE.Vector2(
						this.bs.readShort() / 1023,
						this.bs.readShort() / 1023
					);
					break;
				default:
					strip[k].uv = new THREE.Vector2();
					break;
				}

				// Seek passed user offset

				if (userOffset && k > 1) {
					this.bs.seekCur(userOffset * 2);
				}

			}

			// Convert strips into faces

			for (let k = 0; k < strip.length - 2; k++) {

				let a, b, c;
				let aPos, bPos, cPos;
				let aClr, bClr, cClr;
				let aUv, bUv, cUv;

				if ((clockwise && !(k % 2)) || (!clockwise && k % 2)) {
					a = strip[k + 0];
					b = strip[k + 2];
					c = strip[k + 1];
				} else {
					a = strip[k + 0];
					b = strip[k + 1];
					c = strip[k + 2];
				}

				this.matIndex.push(this.matId);
				
				// Positions

				aPos = this.vertexStack[a.index];
				bPos = this.vertexStack[b.index];
				cPos = this.vertexStack[c.index];
	
				this.vertices.push(aPos.x, aPos.y, aPos.z);
				this.vertices.push(bPos.x, bPos.y, bPos.z);
				this.vertices.push(cPos.x, cPos.y, cPos.z);

				// Colors

				aClr = this.colorStack[a.index] || { r:1, g:1, b:1, a:1 };
				bClr = this.colorStack[b.index] || { r:1, g:1, b:1, a:1 };
				cClr = this.colorStack[c.index] || { r:1, g:1, b:1, a:1 };

				this.colors.push(aClr.r, aClr.g, aClr.b);
				this.colors.push(bClr.r, bClr.g, bClr.b);
				this.colors.push(cClr.r, cClr.g, cClr.b);

				this.vcolors.push(aClr.r, aClr.g, aClr.b, aClr.a);
				this.vcolors.push(bClr.r, bClr.g, bClr.b, bClr.a);
				this.vcolors.push(cClr.r, cClr.g, cClr.b, cClr.a);

				// UV Values
				
				this.uv.push(a.uv.x, 1-a.uv.y);
				this.uv.push(b.uv.x, 1-b.uv.y);
				this.uv.push(c.uv.x, 1-c.uv.y);

				// Push to geometry faces

				if(!doubleSide) {
					continue;
				}

				this.matIndex.push(this.matId);
				
				// Positions

				aPos = this.vertexStack[b.index];
				bPos = this.vertexStack[a.index];
				cPos = this.vertexStack[c.index];
				
				this.vertices.push(aPos.x, aPos.y, aPos.z);
				this.vertices.push(bPos.x, bPos.y, bPos.z);
				this.vertices.push(cPos.x, cPos.y, cPos.z);

				// Colors

				aClr = this.colorStack[a.index] || { r:1, g:1, b:1, a:1 };
				bClr = this.colorStack[b.index] || { r:1, g:1, b:1, a:1 };
				cClr = this.colorStack[c.index] || { r:1, g:1, b:1, a:1 };

				this.colors.push(aClr.r, aClr.g, aClr.b);
				this.colors.push(bClr.r, bClr.g, bClr.b);
				this.colors.push(cClr.r, cClr.g, cClr.b);

				this.vcolors.push(aClr.r, aClr.g, aClr.b, aClr.a);
				this.vcolors.push(bClr.r, bClr.g, bClr.b, bClr.a);
				this.vcolors.push(cClr.r, cClr.g, cClr.b, cClr.a);

				// UV Values
				
				this.uv.push(b.uv.x, 1-b.uv.y);
				this.uv.push(a.uv.x, 1-a.uv.y);
				this.uv.push(c.uv.x, 1-c.uv.y);


			}

		}

	}


}
